/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License).  You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://glassfish.dev.java.net/public/CDDLv1.0.html or
 * glassfish/bootstrap/legal/CDDLv1.0.txt.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at glassfish/bootstrap/legal/CDDLv1.0.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * you own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Copyright 2006 Sun Microsystems, Inc. All rights reserved.
 */


package com.sun.enterprise.security.auth.login;

import com.sun.enterprise.deployment.Group;
import com.sun.enterprise.security.auth.realm.InvalidOperationException;
import com.sun.enterprise.security.auth.realm.NoSuchRealmException;
import com.sun.enterprise.security.auth.realm.NoSuchUserException;
import com.sun.logging.LogDomains;
import java.util.Enumeration;
import java.util.Map;
import java.util.logging.Logger;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import com.sun.enterprise.util.i18n.StringManager;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import com.sun.web.security.PrincipalGroupFactory;
import com.sun.enterprise.deployment.PrincipalImpl;
import com.sun.enterprise.deployment.Group;
import com.sun.enterprise.security.auth.realm.Realm;
import java.util.Enumeration;

/**
 *
 * @author K.Venugopal@sun.com
 */
public class AssertedLoginModule implements LoginModule {

    private Subject subject = null;
    private CallbackHandler handler = null;
    private Map<String, ?> sharedState = null;
    private Map<String, ?> options = null;
    protected Logger _logger = LogDomains.getLogger(LogDomains.SECURITY_LOGGER);
    protected static final StringManager sm = StringManager.getManager("com.sun.enterprise.security.auth.login");
    protected boolean _succeeded = true;
    protected boolean _commitSucceeded = false;
    protected PrincipalImpl _userPrincipal;
    private AssertedCredentials asrtCredentials = null;
    private String username = null;
    private String realmName = null;
    private Realm _realm;

    public AssertedLoginModule() {
    }

    public final void initialize(Subject subject, CallbackHandler handler, Map<String, ?> sharedState, Map<String, ?> options) {
        this.subject = subject;
        this.handler = handler;
        this.sharedState = sharedState;
        this.options = options;
        if (_logger.isLoggable(Level.FINE)) {
            _logger.log(Level.FINE, "Login module initialized: " + this.getClass().toString());
        }
    }

    public final boolean login() throws LoginException {
        Set<Object> creds = this.subject.getPrivateCredentials();
        Iterator<Object> itr = creds.iterator();
       
        while (itr.hasNext()) {
            Object obj = itr.next();
            if (obj instanceof AssertedCredentials) {
                asrtCredentials = (AssertedCredentials) obj;
                break;
            }
        }
        realmName = asrtCredentials.getRealmName();
        try {
            _realm = Realm.getInstance(realmName);
        } catch (NoSuchRealmException ex) {
            _logger.log(Level.FINE, "", ex);
            _logger.log(Level.SEVERE, "no.realm", asrtCredentials.getRealmName());
            throw new LoginException(ex.getMessage());            
        }
        return _succeeded;
    }

    @SuppressWarnings(value = "unchecked")
    public final boolean commit() throws LoginException {
        try {

            if (!_succeeded) {
                _commitSucceeded = false;
                return false;
            }


            _userPrincipal = com.sun.web.security.PrincipalGroupFactory.getPrincipalInstance(asrtCredentials.getUserName(), asrtCredentials.getRealmName());
            java.util.Set principalSet = this.subject.getPrincipals();
            if (!principalSet.contains(_userPrincipal)) {
                principalSet.add(_userPrincipal);
                
            }
            java.util.Enumeration groupsList = getGroups(asrtCredentials.getUserName());
            while (groupsList.hasMoreElements()) {
                java.lang.String value = (java.lang.String) groupsList.nextElement();
                Group g = PrincipalGroupFactory.getGroupInstance(value, asrtCredentials.getRealmName());
                if (!principalSet.contains(g)) {
                    principalSet.add(g);
                }
                // cleaning the slate
            }            
            if(asrtCredentials.getInfo() != null){
                this.subject.getPrivateCredentials().add(asrtCredentials.getInfo());
            }
            return true;
        } catch (NoSuchUserException ex) {
            Logger.getLogger(AssertedLoginModule.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidOperationException ex) {
            Logger.getLogger(AssertedLoginModule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public final boolean abort() throws LoginException {
        if (_logger.isLoggable(Level.FINE)) {
            _logger.log(Level.FINE, "JAAS authentication aborted.");
        }

        if (_succeeded == false) {
            return false;
        } else if (_succeeded == true && _commitSucceeded == false) {
            // login succeeded but overall authentication failed
            _succeeded = false;
        } else {
            // overall authentication succeeded and commit succeeded,
            // but someone else's commit failed
            logout();
        }
        return true;
    }

    public final boolean logout() throws LoginException {
        subject.getPrincipals().clear();
        subject.getPublicCredentials().clear();
        subject.getPrivateCredentials().clear();

        _succeeded = false;
        _commitSucceeded = false;

        return true;
    }

    protected Realm getRealm() {
        return _realm;
    }

    protected Enumeration getGroups(String username) throws NoSuchUserException, InvalidOperationException {
        return _realm.getGroupNames(username);
    }
}
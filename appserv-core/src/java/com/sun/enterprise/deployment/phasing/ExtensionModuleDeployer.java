/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.phasing;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.enterprise.deploy.shared.ModuleType;

import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveDeployer;
import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveDescriptor;
import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveLoader;
import com.sun.enterprise.deployment.util.XModuleType;
import com.sun.enterprise.deployment.backend.WebArchiveDeployer;
import com.sun.enterprise.deployment.backend.DeploymentRequest;
import com.sun.enterprise.deployment.backend.DeploymentUtils;
import com.sun.enterprise.deployment.backend.DeployableObjectType;
import com.sun.enterprise.server.ApplicationServer;
import com.sun.enterprise.deployment.backend.DeploymentLogger;
import com.sun.enterprise.config.ConfigException;
import com.sun.enterprise.instance.InstanceEnvironment;
import com.sun.enterprise.instance.ExtensionModuleConfigManager;
import com.sun.enterprise.util.i18n.StringManager;

public class ExtensionModuleDeployer {

    private ArchiveDeployer archiveDeployer = null;
    private ArchiveLoader archiveLoader = null;
    private ExtensionModuleConfigManager configManager = null;
    private ModuleType moduleType = null;
    private InstanceEnvironment ienv = null;
    private static StringManager localStrings =
                StringManager.getManager( ExtensionModuleDeployer.class );

    protected final static Logger logger = DeploymentLogger.get();

    public ExtensionModuleDeployer(ArchiveDeployer aDeployer) {
        archiveDeployer = aDeployer;
        // use the fully qualified class name of archive deployer 
        // as the unique module type
        String mType = archiveDeployer.getClass().getName();
        moduleType = XModuleType.getModuleType(mType);

        // XXX: A deployableObjectType is obtained here for this module type. 
        // There is no immediate use for this object here, but it adds a new
        // DeployableObjectType corresponding to this module type. This is used
        // in the undeploy action.
        DeployableObjectType depObjType = 
                DeployableObjectType.getDeployableObjectType(moduleType);
        ienv = ApplicationServer.getServerContext().getInstanceEnvironment();
        
        try {
            configManager = new ExtensionModuleConfigManager(ienv, moduleType);
            /* archiveDeployer.registerCustomManager(configManager, 
                                              getParentClassLoader()); */
        } catch (ConfigException ce) {
            String msg = localStrings.getString("enterprise.deployment.backend.configException");
            logger.log(Level.SEVERE, msg, ce.toString());   
        }  
    }

    public void deployToDomain(DeploymentRequest request) throws Exception {

        String moduleName = request.getName();
        ClassLoader parentClassLoader = getParentClassLoader();
        File archivePath = request.getFileSource().getFile();
        File moduleRootDirectory = getModuleRootDirectory(moduleName);
        
        // set the isJ2eeApp flag to false
        archiveDeployer.setJ2eeApplication(false);
        
        // set the request in the WebArchiveDeployer.This is needed to
        // make use WebDeployer functionality
        if(archiveDeployer instanceof WebArchiveDeployer) {
            ((WebArchiveDeployer)archiveDeployer).setDeploymentRequest(request);
            ((WebArchiveDeployer)archiveDeployer).setManager(configManager);
        } else {
            // do nothing;
        }
        // Check if directory deploy 
        if (archivePath.isDirectory()) {
            moduleRootDirectory = archivePath;
        } else {
            archiveDeployer.expand(archivePath, moduleRootDirectory);
        }
        request.setDeployedDirectory(moduleRootDirectory);
        if(request.getDescription() == null) {
            request.setDescription(archiveDeployer.getModuleDescription());
        }
        // get the descriptor processed by the archiveDeployer
        ArchiveDescriptor descriptor = archiveDeployer.prepare(
            moduleRootDirectory, getModuleScratchDirectory(moduleName),  
            parentClassLoader, false); 
       
        logger.log(Level.FINE, "Descriptor ="+descriptor.toString());         
        // register the descriptor with the ModulesManager 
        configManager.registerExtensionDescriptor(request.getName(), 
            descriptor);
    }

    public void redeployToDomain(DeploymentRequest request) throws Exception {
        undeployFromDomain(request);
        deployToDomain(request);
    }

    public ArchiveDeployer getDeployer(){
        return archiveDeployer;
    }

    
    public void undeployFromDomain(DeploymentRequest request) 
        throws Exception {
        String appName = request.getName();
        // set the request in the WebArchiveDeployer
        if(archiveDeployer instanceof WebArchiveDeployer) {
            ((WebArchiveDeployer)archiveDeployer).setDeploymentRequest(request);
            ((WebArchiveDeployer)archiveDeployer).setManager(configManager);
        } else {
            // do nothing;
        }   
        //String location = configManager.getLocation(appName);
        // unregister application element from domain.xml
        configManager.unregisterExtensionDescriptor(appName);
        // Invoke the cleanup method in the archive deployer
        //archiveDeployer.cleanup(new File(configManager.getLocation(appName)));
        archiveDeployer.cleanup(getModuleRootDirectory(appName));
    }

    /**
     * Returns the module type
     * @return the module type of the module being deployed
     */
    public ModuleType getModuleType() {
        return moduleType;
    }

    /**
     * Returns the an instance of the ExtensionModulesConfigManager for the
     * module being deployed
     * @return The ExtensionModulesConfigManager
     */
    public ExtensionModuleConfigManager getConfigManager() {
        return configManager;
    }
        
    public void setConfigManager(ExtensionModuleConfigManager configManager) {
        this.configManager = configManager;
    }
   
    /**
     * Returns the ArchiveLoader implementation if it has been registered
     * @return the ArchiveLoader implementation
     */ 
    public ArchiveLoader getExtensionLoader() {
        return archiveLoader;
    }

    /** 
     * Utility method for registering the implementation of the ArchiveLoader
     * @param archiveLoader instance of the ArchiveLoader implementation
     */
    public void setExtensionLoader(ArchiveLoader archiveLoader) {
        this.archiveLoader = archiveLoader;
    }
    
   // XXX return a classloader with application libraries
    private static ClassLoader getParentClassLoader() {
        return ApplicationServer.getServerContext().getSharedClassLoader();
    }

   /**
    * Returns the directory where the module is exploded
    * @param moduleName the name of the module being deployed
    * @return a File object that represents the directory
    */
   private File getModuleRootDirectory(String moduleName) {
       File moduleRootDir = new File(ienv.getModuleRepositoryPath(),
                                          moduleName);
       if (!moduleRootDir.exists()) {
           moduleRootDir.mkdirs();
       }
       return moduleRootDir;
   }

   /**
    * Returns the directory where the generated artifacts from this module
    * are stored
    * @param moduleName the name of the module being deployed
    * @return a File object that represents the directory
    */
   private File getModuleScratchDirectory(String moduleName) {
       File moduleScratchDir = new File(ienv.getModuleGeneratedXMLPath(), 
           moduleName);
       if (!moduleScratchDir.exists()) {
           moduleScratchDir.mkdirs();
       }
       return moduleScratchDir;
   }
}

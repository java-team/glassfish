/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.enterprise.server;

/**
 * A class that implements this interface can be packaged in an ear file.
 * The manifest of the ear file should contain an entry called 
 * EAR-Loader-Listener, which specifies the class name. 
 * 
 * Eg: EAR-Loader-Listener: org.foo.test.listener
 * @author Binod
 */
public interface ApplicationLoadEventListener {
    /**
     * Handle loading of the application in the instance. This method will be
     * executed just after the completion of the app loading.
     */
    public void handleLoad();
    
    /**
     * Handle load failure of the application in the instance. This method will 
     * be executed if the application fails to load.
     */
    public void handleLoadFailure();
    
    /**
     * Handle load failure of the application in the cluster. This method will 
     * be executed when the application fails to load in a cluster.
     * 
     * This method will be executed in only one instance of the cluster. 
     * 
     * @param timedout This value will be false, if result of load operation is 
     * failure in all the instances and the aggrgation of results in the 
     * instance elected for invoking this method is successful. For any reason, 
     * if the elected instance could not aggregate results (for example, the 
     * loading failed in one instance), then the elected instance will wait for
     * the result for a particular period of time and then pass the 
     * <code>timedout</code> as true.
     */
    public void handleLoadFailureInCluster(boolean timedout);
    
    /**
     * Handle unloading of the application in the instance. This method will be
     * executed just before the completion of the app unloading.
     */
    public void handleUnLoad();
       
    /**
     * Handle loading of the application in the cluster. This method will be
     * executed just before the completion of the app loading.
     * 
     * This method will be executed only one instance of the cluster and will
     * wait for aggregation of load results from all instances of the cluster.
     * 
     * @param timedout This value will be false, if result of load operation is 
     * successful in all the instances and the aggrgation of results in the 
     * instance elected for invoking this method is successful. For any reason, 
     * if the elected instance could not aggregate results (for example, the 
     * loading failed in one instance), then the elected instance will wait for
     * the result for a particular period of time and then pass the 
     * <code>timedout</code> as true.
     */
    public void handleLoadInCluster(boolean timedout);
    
    /**
     * Handle unloading of the application in the cluster. 
     * 
     * This method will be executed only one instance of the cluster. This will
     * not wait for aggregation of unload results in the cluster. 
     */
    public void handleUnLoadInCluster();
}

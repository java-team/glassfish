/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.server;

import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Iterator;

import javax.enterprise.deploy.shared.ModuleType;
import javax.management.MBeanException;

import com.sun.enterprise.config.ConfigException;
import com.sun.enterprise.config.serverbeans.ElementProperty;
import com.sun.enterprise.config.serverbeans.ExtensionModule;
import com.sun.enterprise.instance.ExtensionModuleConfigManager;
import com.sun.logging.LogDomains;
import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveDescriptor;
import com.sun.enterprise.deployment.phasing.ExtensionModuleDeployer;
import com.sun.enterprise.deployment.pluggable.PluggableDeploymentInfo;
import com.sun.enterprise.deployment.Descriptor;
import com.sun.enterprise.deployment.BundleDescriptor;
import com.sun.enterprise.deployment.Application;
import com.sun.enterprise.Switch;
import com.sun.enterprise.server.event.ApplicationEvent;
import com.sun.enterprise.loader.EJBClassLoader;
import com.sun.enterprise.server.WebArchiveLoader;
import com.sun.enterprise.ManagementObjectManager;

/**
 * Extension loader loads and unloads extension module.
 *
 */
class ExtensionModuleLoader extends AbstractLoader {
 
    static Logger _logger=LogDomains.getLogger(LogDomains.LOADER_LOGGER);
    private ArchiveDescriptor descriptor = null;
    private ExtensionModuleDeployer extModuleDeployer = null; 
    private ModuleType moduleType = null;
    private boolean isEnableDisable = false;
    

    /**
     * ExtensionModuleLoaderer loads one module.
     *
     * @param modID              the name of the extension module
     * @param parentClassLoader  the parent class loader
     * @param extensionConfigManager  the extension module mgr for this VS
     */
    ExtensionModuleLoader(String modID, ClassLoader parentClassLoader,
                           ExtensionModuleConfigManager extensionConfigManager, 
                                       ModuleType mType) {

        super(modID, parentClassLoader, extensionConfigManager);

        moduleType = mType;

        extModuleDeployer =  
            PluggableDeploymentInfo.getExtensionModuleDeployer(moduleType);

        try {
            initializeLoader(new String[0],
                extensionConfigManager.getLocation(modID), moduleType);

            descriptor = 
                extensionConfigManager.getRegisteredExtensionDescriptor(modID);

            //application object would be null if this is during server
            //startup or deployment to remote instance
            if (descriptor == null) {
                descriptor =  extensionConfigManager.getExtensionDescriptor(
                                    modID, ejbClassLoader);
           }
            application = extensionConfigManager.getExtensionDescriptor(modID,
                                     extensionConfigManager.getLocation(modID));

        } catch (Exception confEx) {
            //@@ i18n
            _logger.log(Level.SEVERE, "ERROR while loading descriptor " + modID);
            _logger.log(Level.SEVERE,"loader.error_while_loading_app_desc",
                        confEx);
        }
    }
    
    /**
     * Overriden method to make this a no-op for WebContainer 
     * Loads all the beans in this stand alone extension module.
     * This routine creates the Extension and MDB container.
     *
     * @param    jsr77    create jsr77 mBeans if true
     * @return   true     if all extensions loaded properly
     */
    boolean load(boolean jsr77) {
        return super.load(jsr77);
    }
    /**
     * Loads all the beans in this stand alone extension module.
     * This routine creates the Extension and MDB container.
     *
     * @param    jsr77    create jsr77 mBeans if true
     * @return   true     if all extensions loaded properly
     */
    boolean doLoad(boolean jsr77) {
	notifyAppEvent(ApplicationEvent.BEFORE_APPLICATION_LOAD);
        boolean status =false; 
        try {
            if (extModuleDeployer.getExtensionLoader() != null ) {
                if(extModuleDeployer.getExtensionLoader() instanceof
                         WebArchiveLoader) {
                    ((WebArchiveLoader)extModuleDeployer.getExtensionLoader()).
                                setModulesManager(
                                  (ExtensionModuleConfigManager)configManager);
                }
                Properties props = getProperties();
                if (props != null) {
                	status = extModuleDeployer.getExtensionLoader().
                	load(descriptor,
                			getIsEnableDisable(), props);
                } else {
                status = extModuleDeployer.getExtensionLoader().
                                                 load(descriptor,
                                                     getIsEnableDisable()); 
                }
            } else {
                status = false;
            }
        } catch (Exception e) {
            status = false;
            // XXX log exception here
        }
	if (status == true) {
	    notifyAppEvent(ApplicationEvent.AFTER_APPLICATION_LOAD);
	} 

	return status;
    }
        
    private Properties getProperties() {
    	Properties result = null;
    	try {
    		ExtensionModule  em = ((ExtensionModuleConfigManager)configManager).getExtensionModule(id);
    		ElementProperty[] eProp = em.getElementProperty();
    		if (eProp.length != 0) {
    			// return a non-null result
    			result = new Properties();
    			for(int j=0; j<eProp.length; j++) {
    				ElementProperty eProp1 = eProp[j];
    				result.put(eProp1.getName(), eProp1.getValue());
    			}
    		}
    	} catch (Throwable e) {
    		_logger.log(Level.WARNING, "could not determine properties for AR module",e);
    		result = null;
    	}
    	if (_logger.isLoggable(Level.FINEST)) {
    		_logger.log(Level.FINEST, "module {0} has properties {1}.", new Object[] {id, result});
    	}
    	return result;
    }
        
    /**
     * Unloads the beans in this stand alone extension module.
     *
     * @param    jsr77    delete jsr77 mBeans if true
     * @return   true     if removed successful
     */
    boolean unload(boolean jsr77) {
        // undeploy the extension modules

	//Note: Application.isVirtual will be true for stand-alone module
	notifyAppEvent(ApplicationEvent.BEFORE_APPLICATION_UNLOAD);

        boolean result = false; 
        try {
            if (extModuleDeployer.getExtensionLoader() != null ) {
                if(extModuleDeployer.getExtensionLoader() instanceof
                         WebArchiveLoader) {
                    ((WebArchiveLoader)extModuleDeployer.getExtensionLoader()).
                                setModulesManager(
                                   (ExtensionModuleConfigManager)configManager);
                }
                result = extModuleDeployer.getExtensionLoader().
                                                    unload(descriptor, 
                                                        getIsEnableDisable()); 
            } else {
                result = false;
            }
        } catch (Exception e) {
            result = false;
            // XXX log exception here
        }

        ((ExtensionModuleConfigManager)configManager).unregisterExtensionDescriptor(id);

	notifyAppEvent(ApplicationEvent.AFTER_APPLICATION_UNLOAD);

        // helps garbage collector
        // Commenting this out since we do use the objects from this class
        // after this stage. The done() method would be called from the 
        // ExtensionModuleManager instead.
        //done();
        return result;
    }

   /**
     * Override the super class done method
     * Helps garbage collector by assigning member variables to null.
     * This is called from unload.
     *
     * @see #unload
     */
    protected void done() {
        // releases resources (file handles, etc) in the class loader
        if (this.ejbClassLoader != null && 
            this.ejbClassLoader instanceof EJBClassLoader) {
            ((EJBClassLoader) ejbClassLoader).done();
        }
                                                                                
        this.id                 = null;
        this.parentClassLoader  = null;
        this.application        = null;
        this.descriptor         = null;
        this.ejbClassLoader     = null;
        this.registry           = null;
        this.configManager      = null;
        this.isEnableDisable    = false;
    }

    /**
     * @return the module type this class is managing
     */
    public ModuleType getModuleType() {
        return moduleType;
    }

    void createRootMBean () throws MBeanException {

        java.util.Set extBundles = this.application.getExtnBundleDescriptors();

        for(Iterator it=extBundles.iterator(); it.hasNext(); ) {
            BundleDescriptor bd = (BundleDescriptor)it.next();
	    try {
            	Switch.getSwitch().getManagementObjectManager().createExtModuleMBean(bd,
		    this.configManager.getInstanceEnvironment().getName(),
		    this.configManager.getLocation(this.id),
                    moduleType.toString());
	    } catch (Exception e) {
	        throw new MBeanException(e);
	    }
        }
    }
                                                                                
    void deleteRootMBean () throws MBeanException{

        /*
        java.util.Set extBundles = this.application.getExtnBundleDescriptors();

        for(Iterator it=extBundles.iterator(); it.hasNext(); ) {
            BundleDescriptor bd = (BundleDescriptor)it.next();
            Switch.getSwitch().getManagementObjectManager().deleteExtModuleMBean(bd,
                    this.configManager.getInstanceEnvironment().getName(),
                    moduleType.toString());
        }
        */
    }
                                                                                
    void createLeafMBeans () throws MBeanException{
    }
                                                                                
    void deleteLeafMBeans () throws MBeanException {
    }
                                                                                
    void createLeafMBean (Descriptor descriptor) throws MBeanException {
    }
                                                                                
    void deleteLeafMBean (Descriptor descriptor) throws MBeanException {
    }
                                                                                
    void deleteLeafAndRootMBeans () throws MBeanException {
    }
                                                                                
    void setState(int state) throws MBeanException {

        java.util.Set extBundles = this.application.getExtnBundleDescriptors();

        for(Iterator it=extBundles.iterator(); it.hasNext(); ) {
            BundleDescriptor bd = (BundleDescriptor)it.next();
            Switch.getSwitch().getManagementObjectManager().setExtModuleState(state, bd,
                this.configManager.getInstanceEnvironment().getName(),
                    moduleType.toString());
        }
    }
    
    public void setIsEnableDisable(boolean isEnableDisable) {
        this.isEnableDisable = isEnableDisable;
    }
    
    public boolean getIsEnableDisable() {
        return isEnableDisable;
    }
}

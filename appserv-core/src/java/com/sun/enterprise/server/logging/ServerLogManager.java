
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.server.logging;



import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.Handler;
import java.util.logging.Filter;
import java.util.logging.ConsoleHandler;
import java.util.logging.ErrorManager;

import java.util.List;
import java.util.Iterator;

import com.sun.enterprise.server.ServerContext;
import com.sun.enterprise.server.ApplicationServer;
import com.sun.enterprise.config.serverbeans.ServerBeansFactory;
import com.sun.enterprise.config.serverbeans.Server;
import com.sun.enterprise.config.serverbeans.Config;
import com.sun.enterprise.config.serverbeans.ElementProperty;
import com.sun.enterprise.config.serverbeans.ModuleLogLevels;
import com.sun.enterprise.config.serverbeans.LogService;

import com.sun.logging.LogDomains;


/**
 * Class ServerLogManager is a subclass of LogManager for use within the 
 * Application Server. The FileandSyslogHandler is installed
 * on each logger created by this log manager. 
 *
 * _REVISIT_: Need to make sure that the LogLevels in domain.xml is parsed
 * first to set the log levels correctly. We may miss setting some of the 
 * loglevels, because the values in domain.xml is not available when some
 * loggers are created 
 * <p><b>NOT THREAD SAFE: mutable instance variables: verboseMode, customFilterError</b>
 */
public class ServerLogManager extends BaseLogManager {

    // We will use a single instance of Handler for all the Loggers 
    // Note: these handlers extend StreamHandler whose methods are synchronized
    private static FileandSyslogHandler handlerSingleton; 
    private static ConsoleHandler consoleHandler; 
    private static SystemLogHandler syslogHandler;
    private static DeploymentAuditLogHandler deploymentAuditHandler;
    
    // If we are running on Windows, RI or some other platform where we
    // cannot do syslog and the user has turned on use-system-logging is true
    // then we will set this flag to avoid wasting cycles to reload the library
    private static boolean syslogLibraryLoadError = false;

    // If there is any CustomHandler and/or CustomFilter we will plug that.
    private static Handler customHandler = null;
    private static Filter customFilter = null;


    private static ServerLogManager thisInstance;

    private static List listOfUnInitializedLoggers = new java.util.ArrayList();

    // caches value of System property com.sun.aas.verboseMode
    private static Boolean verboseMode = null;

    private static boolean customFilterError = false;

    private static boolean customHandlerError = false;

    private static final String SUN_OS = "SunOS";

    private static final String LINUX_OS = "Linux";

    private static final String OS_NAME_PROPERTY = "os.name";

    private Object lockObj = new Object();

    private static final String ORG_APACHE_CATALINA = "org.apache.catalina.";
    private static final String ORG_APACHE_COYOTE = "org.apache.coyote.";
    private static final String ORG_APACHE_JASPER = "org.apache.jasper.";
    private static final String SYNCHRONIZATION = 
                        "javax.ee.enterprise.system.tools.synchronization";

    public ServerLogManager() {
        super();
    }
    
        public static synchronized ServerLogManager
    getInstance() {
        if ( thisInstance == null ) {
            thisInstance    = new ServerLogManager();
        }
        return thisInstance;
    }

    /**
     * When a new logger is created in the system, this method will be invoked
     * to intialize the log level appropriately and also to set the required
     * LogHandlers.
     */
    protected void initializeLogger(Logger logger) {
	synchronized(lockObj) {
            internalInitializeLogger( logger );
        
            if( getLogService() == null ) {
                listOfUnInitializedLoggers.add( logger );
            }
        }
    }

    /**
     *  Internal Method to initialize a list of unitialized loggers.
     */
    private void internalInitializeLogger( final Logger logger ) {
        java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction() {
                public Object run() {
                    // Explicitly remove all handlers.
                    Handler[] h = logger.getHandlers();
                    for (int i = 0; i < h.length; i++) {
                        logger.removeHandler(h[i]);
                    }

                    if (logger.getName().equals(LogDomains.DPLAUDIT_LOGGER)) {
                        // redirect to deployment audit log
                        logger.addHandler(getDeploymentAuditHandler());
                        logger.setUseParentHandlers(false);
                    }


                    if( logger.getName().intern() == "".intern() ) {

                        if (!AsyncMemoryHandler.isEnabled()) {
                            logger.addHandler(getFileandSyslogHandler() );
                        }
                        else {
                            logger.addHandler(AsyncMemoryHandler.getInstance(getFileandSyslogHandler(),Level.WARNING));
                        }
                        if ( logToStderr() ) {
                            logger.addHandler(getConsoleHandler());
                        }
                        if( logToSyslog() ) { 
                           Handler syslogHandler = getSyslogHandler();
                           if( syslogHandler != null ) {
                               logger.addHandler( syslogHandler );
                           }
                        }
                        logger.setUseParentHandlers( false );

                    }

                    Level logLevel = getConfiguredLogLevel(logger.getName());
                    if( logLevel != null ) {
                        logger.setLevel( logLevel );
                    }
                    postInitializeLogger( logger );
                    return null;
                }
            }
        );
    }

    /**
     *  This is where we plug in any custom Log Handler and Log Filter.
     */
    private void postInitializeLogger( final Logger logger ) {
        final Handler customHandler = getCustomHandler( );
        final Filter customFilter = getCustomFilter( );
        if( ( customHandler == null)
          &&( customFilter == null ) ) {
            return;
        }
        java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction() {
                public Object run() {
                    if( customHandler != null ) {
                         logger.addHandler( customHandler );
                    }
                    if( customFilter != null ) {
                         logger.setFilter( customFilter );
                    }
                    return null;
                }
            }
        );
    } 


    /**
     *  _REVISIT_: Talk to JDO team to see if we can nuke this method and
     *  rely on the generic framework for the Logger initialization..
     */
    public static void initializeServerLogger( Logger logger ) {
    }


    /** 
     * This is a Singleton factory method to get and instance of 
     * FileandSyslogHandler.
     */
    private static synchronized Handler getFileandSyslogHandler ( ) {
        if( handlerSingleton == null ) {
            try {
                handlerSingleton = FileandSyslogHandler.getInstance();
                handlerSingleton.setLevel( Level.ALL );
            } catch( Exception e ) {
                new ErrorManager().error( "Exception caught in getHandler ",
                     e, ErrorManager.GENERIC_FAILURE );
            }
        }
        return handlerSingleton;
    } 

    
    /** 
     * This is a Singleton factory method to get an instance of 
     * DeploymentLogHandler.  
     */
    private static synchronized Handler getDeploymentAuditHandler( ) {
        if( deploymentAuditHandler == null ) {
            try {
                deploymentAuditHandler = DeploymentAuditLogHandler.getInstance();
                deploymentAuditHandler.setLevel(DeploymentAuditLogHandler.getConfiguredLevel());
            } catch( Exception e ) {
                new ErrorManager().error( "Exception caught in getHandler ",
                     e, ErrorManager.GENERIC_FAILURE );
            }
        }
        return deploymentAuditHandler;
    } 


    /** 
     * This is a Singleton factory method to get an instance of ConsoleHandler.
     */
    private static synchronized Handler getConsoleHandler() {
        if( consoleHandler == null ) {
            try {
                consoleHandler = new ConsoleHandler();
                consoleHandler.setLevel(Level.ALL);
                if (!AsyncMemoryHandler.isEnabled()) {
                    consoleHandler.setFormatter(new UniformLogFormatter());
                } else {
                    consoleHandler.setFormatter(new CustomLogFormatter());
                }

            } catch( Exception e ) {
                new ErrorManager().error( 
                     "Exception caught in getConsoleHandler ",
                     e, ErrorManager.GENERIC_FAILURE );
            }
        }
        return consoleHandler;
    } 


     private static synchronized Handler getSyslogHandler() {
         if( syslogLibraryLoadError ) return null;
         if( syslogHandler == null ) {
             try {
                 syslogHandler = new SystemLogHandler();
                 syslogHandler.setLevel(Level.ALL);
                if (!AsyncMemoryHandler.isEnabled()) {
                    syslogHandler.setFormatter(new UniformLogFormatter());
                } else {
                    syslogHandler.setFormatter(new CustomLogFormatter());
                }
             } catch( Exception e ) {
                 syslogLibraryLoadError = true; 
                 // WE WILL EAT AWAY THE EXCEPTION, IF SOME ONE TURNS ON SYSLOG
                 // ON WINDOWS or LINUX, They will not see any error.
             }
         }
         return syslogHandler;
     }

    /**
     *  If there is any custom handler we will use that.
     */
    private static synchronized Handler getCustomHandler( ) {
        if( (customHandler != null ) 
          ||(customHandlerError) ) 
        {
            return customHandler;
        }
        LogService logService = getLogService( );
        if( logService == null ) {
            return null;
        }
        String customHandlerClassName = null;
        try {
            customHandlerClassName = logService.getLogHandler( );

            customHandler = (Handler) getInstance( customHandlerClassName );
            // We will plug in our UniformLogFormatter to the custom handler
            // to provide consistent results
            if( customHandler != null ) {
                if (!AsyncMemoryHandler.isEnabled()) {
                    customHandler.setFormatter( new UniformLogFormatter( ) );
                } else {
                    customHandler.setFormatter( new CustomLogFormatter( ) );
                }
            }
        } catch( Exception e ) {
            customHandlerError = true; 
            new ErrorManager().error( "Error In Initializing Custom Handler " +
                customHandlerClassName, e, ErrorManager.GENERIC_FAILURE );
        }
        return customHandler;
    }

    /**
     *  If there is any Custom Filter we will use that.
     */
    private static Filter getCustomFilter( ) {
        if(( customFilter != null ) 
          ||( customFilterError ) )
        {
            return customFilter;
        }
        LogService logService = getLogService( );
        if( logService == null ) {
            return null;
        }
        String customFilterClassName = null;
        try {
            customFilterClassName = logService.getLogFilter( );
            customFilter = (Filter) getInstance( customFilterClassName );
        } catch( Exception e ) {
            customFilterError = true;
            new ErrorManager().error( "Error In Instantiating Custom Filter " +
                customFilterClassName, e, ErrorManager.GENERIC_FAILURE );
        }
        return customFilter;
    }


    /**
     *  A Utility method to get the LogService Configuration element.
     */
    static LogService getLogService( ) {
        try {
            ServerContext sc = ApplicationServer.getServerContext();
            if( sc == null ) {
                return null;
            }
            return ServerBeansFactory.getConfigBean(
                sc.getConfigContext()).getLogService( );
        } catch( Exception e ) {
            new ErrorManager().error( "Error In getLogService  ", e,
                ErrorManager.GENERIC_FAILURE );
        }
        return null;
    }


    /**
     *  A Utility method to instantiate Custom Log Handler and Log Filter.
     */
    private static Object getInstance( final String className ) {
        if( className == null ) return null;
        return  java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction() {
                public Object run() {
                    try {
                        ClassLoader cl =
                            Thread.currentThread().getContextClassLoader();
                        if (cl == null)
                            cl = ClassLoader.getSystemClassLoader();
                        return Class.forName( className, true,cl).newInstance();
                    } catch( Exception e ) {
                        new ErrorManager().error(
                            "Error In Instantiating Class " + className, e,
                            ErrorManager.GENERIC_FAILURE );
                    }
                    return null;
               }
           }
       );
    }

    
    // return true if log msgs have to be printed on to Syslog
    private static boolean logToSyslog() {
         boolean sunOS = 
             System.getProperty( OS_NAME_PROPERTY ).equals( SUN_OS );
         boolean linuxOS = 
             System.getProperty( OS_NAME_PROPERTY ).equals( LINUX_OS );

         // If it is not SUN OS, then logToSyslog should return false
         if( !sunOS && !linuxOS ) { 
             return false; 
         }
         if( syslogLibraryLoadError ) 
             return false;
         LogService logService = getLogService( );
         if( logService != null ) {
             return logService.isUseSystemLogging( );
         }
         return false;
     }


    // return true if log msgs have to be printed on stderr
    private static boolean logToStderr() {
        try {
	    // Check if server was started with "asadmin start-domain --verbose"
	    // This system property is set in PELaunchFilter.
	    if ( verboseMode == null ) {
		verboseMode = Boolean.FALSE;
		String verbose = System.getProperty("com.sun.aas.verboseMode");
		if ( verbose != null && verbose.equals("true") ) {
		    verboseMode = Boolean.TRUE;
		}
	    }
	    if ( verboseMode.booleanValue() == true ) {
		return true;
	    }

	    // check if log-console / echo-log-messages-to-stderr is true
	    ServerContext sc = ApplicationServer.getServerContext();
	    if (sc == null) {
		// This can happen before ApplicationServer.onInitialization()
		// has been called.
		return false;
	    }

            Config cfg = 
                ServerBeansFactory.getConfigBean(sc.getConfigContext());
            return cfg.getLogService().isLogToConsole();

        } catch ( Exception ex ) {
            new ErrorManager().error( 
                "Error while geting echo-log-messages-to-stderr attribute of " +
                " log-service ", ex, ErrorManager.GENERIC_FAILURE );
	    return false;
        }
    }


    /**
     * Given a logger name, this method returns its level as defined
     * in domain.xml (the app server config file).
     * _REVISIT_: 
     * 1. Replace multiple if (loggerName) checks with a Hashmap
     */
    public static Level getConfiguredLogLevel(String loggerName) {
        ServerContext sc = ApplicationServer.getServerContext();
        if (sc == null) {
            if (loggerName.startsWith(SYNCHRONIZATION)) { 
                try {
                    String level = System.getProperty(SYNCHRONIZATION, "INFO");
                    return Level.parse(level);
                } catch (Exception e) {
                    return Level.INFO;
                }
            } else {
                return Level.INFO;
            }
        }

        if (loggerName.equals(LogDomains.DPLAUDIT_LOGGER)) {
            try {
                Level level = DeploymentAuditLogHandler.getConfiguredLevel();
                return level;
            } catch (Throwable thr) {
                return Level.OFF;
            }
        }
        
        Level logLevel = null;
        try {
            Config cfg = 
                ServerBeansFactory.getConfigBean(sc.getConfigContext());
            ModuleLogLevels allModulesLogLevels = 
                cfg.getLogService().getModuleLogLevels( );
            // _REVISIT_: Right now ModuleLogLevels element in Log-Service
            // is optional. If the user doesn't specify any module log levels
            // then we will use 'INFO' as the default. For 8.1 this should
            // be a required element.
            if( allModulesLogLevels == null ) { return Level.INFO; }
            if( allModulesLogLevels.getRoot( ).equals( "OFF" ) ) {
                return Level.OFF;
            }

            // _REVISIT_: This is a bad way of searching for a loggername match
            // clean this up after Technology Preview
            ElementProperty[] elementProperties = 
                cfg.getLogService().getModuleLogLevels().getElementProperty( );

            if( elementProperties != null ) {
                for( int i = 0; i < elementProperties.length; i++ ) {
                    if( elementProperties[i].getName().equals(loggerName) ) {
                        return Level.parse( elementProperties[i].getValue());
                    }
                }
            }

            String logModName = ModuleToLoggerNameMapper.getModuleName(loggerName);
            if (logModName!=null) {
                try {
                    String val = allModulesLogLevels.getAttributeValue(logModName);
                    logLevel = Level.parse(val);
                } catch (Exception noSuch) {  //no such module name,such as "core", in <module-log-levels>
                }
	    }
        } catch ( Exception e ) {
            new ErrorManager().error( "Error In Setting Initial Loglevel", e,
                ErrorManager.GENERIC_FAILURE );
        } 
        return logLevel;
    }


    /**
     *  Here's an oppurtunity to reInitialize the Loggers, if they are
     *  initialized before the admin service started.
     */
    public static void reInitializeServerLoggers() { 
        try {
            FileandSyslogHandler handler= 
                (FileandSyslogHandler)getFileandSyslogHandler();
            // If there is a file name specified by the administrator for
            // Log File, then here's a first oppurtunity to change the
            // file name because the Log Handles may have been initialized 
            // before the configuration data is completely read.
            handler.changeFileName( getLogService().getFile() );        
            Long rotationTimeLimitValue = new Long( 
                getLogService().getLogRotationTimelimitInMinutes().trim() );
            if( rotationTimeLimitValue.longValue( ) != 0 ) {
                // If there is a value specified for the rotation based on
                // time we set that first, if not then we will fall back to
                // size based rotation
                LogRotationTimer.getInstance().startTimer( 
                    new LogRotationTimerTask( 
                        rotationTimeLimitValue.longValue( ) ) ); 
                // Disable the Size Based Rotation if the Time Based
                // Rotation is set.
                handler.setLimitForRotation( 0 );
            } else {
                Integer rotationLimitAttrValue = new Integer(
                    getLogService().getLogRotationLimitInBytes().trim() );
                // We set the LogRotation limit here. The rotation limit is the
                // Threshold for the number of bytes in the log file after which
                // it will be rotated.
                handler.setLimitForRotation( 
                    rotationLimitAttrValue.intValue() );
            }
  
            AsyncMemoryHandler.init();
            if( listOfUnInitializedLoggers.size() == 0 ) {
                return;
            }
            Iterator listIterator = listOfUnInitializedLoggers.iterator( );
            final ServerLogManager mgr = getInstance();
            while( listIterator.hasNext( ) ) {
                mgr.initializeLogger( (Logger) listIterator.next() );
            }
        } catch( Exception e ) {
            new ErrorManager().error( 
                "Exception caught in reInitializeServerLoggers ",
                 e, ErrorManager.GENERIC_FAILURE );
        }
    }
}

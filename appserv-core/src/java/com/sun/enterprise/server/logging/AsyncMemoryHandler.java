/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.server.logging;

import com.sun.enterprise.config.serverbeans.ElementProperty;
import com.sun.enterprise.config.ConfigContext;
import com.sun.enterprise.config.serverbeans.ServerBeansFactory;
import com.sun.enterprise.server.ApplicationServer;

import java.io.UnsupportedEncodingException;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.*;


public class AsyncMemoryHandler extends Handler implements Runnable {
    private static AsyncMemoryHandler _instance;
    private static AtomicInteger msgSize = new AtomicInteger(0);
    private static final Object lock = new Object();
    private static long pushInterval = 3 * 60 * 1000L;
    private static int bufferSize = 8 * 1024;
    private static MemoryHandler mh;
    private static MemoryHandler backupMh;
    private static AtomicReference<MemoryHandler> activeHandler;
    private static AtomicReference<MemoryHandler> passiveHandler;
    private static String format = "DEFAULT";
    private static boolean async = Boolean.getBoolean(
            "org.glassfish.logging.async");

    private AsyncMemoryHandler(Handler h, Level pushLevel) {
        super();
        mh = new MemoryHandler(h, bufferSize, pushLevel);
        activeHandler = new AtomicReference(mh);
        passiveHandler = new AtomicReference(mh);
        backupMh = new MemoryHandler(h, bufferSize, pushLevel);

        Thread t = new Thread(this);
        t.setDaemon(true);
        t.start();
    }

    static void init() {
        try {
            ConfigContext ctx = 
                ApplicationServer.getServerContext().getConfigContext();
            ctx.refresh();
            ElementProperty[] eprops = 
                ServerBeansFactory.getConfigBean(ctx).getLogService().getElementProperty();

            if (eprops != null) {
                for (int index = 0; index < eprops.length; index++) {
                    if ("max-buffered-messages".equals(eprops[index].getName())) {
                        String value = eprops[index].getValue();
                        bufferSize = Integer.parseInt(value);
                    } else if ("push-interval-in-seconds".equals(
                                eprops[index].getName())) {
                        String value = eprops[index].getValue();
                        pushInterval = Integer.parseInt(value) * 1000;
                    } else if ("format".equals(eprops[index].getName())) {
                        format = eprops[index].getValue();
                    }
                }
            }
        } catch (Exception ex) {
            long pushInterval = 3 * 60 * 1000L;
            bufferSize = 8 * 1024;
            format = "DEFAULT";
        }

        CustomLogFormatter.parseFormat(format);
    }

    public static synchronized AsyncMemoryHandler getInstance(Handler h,
        Level pushLevel) {
        if (_instance == null) {
            _instance = new AsyncMemoryHandler(h, pushLevel);
        }

        return _instance;
    }

    static boolean isEnabled() {
        return async;
    }

    public void publish(LogRecord record) {
        int count;

        while (true) {
            if ((count = msgSize.incrementAndGet()) < bufferSize) {
                if (record.getLevel().intValue() <= Level.FINE.intValue()) {
                    record.setSourceClassName(record.getSourceClassName());
                    record.setSourceMethodName(record.getSourceMethodName());
                }

                activeHandler.get().publish(record);

                return;
            } else {
                synchronized (lock) {
                    if (count == bufferSize) {
                        if (activeHandler.compareAndSet(mh, backupMh)) {
                            passiveHandler.set(mh);
                        } else {
                            activeHandler.compareAndSet(backupMh, mh);
                            passiveHandler.set(backupMh);
                        }

                        lock.notifyAll();
                        msgSize.set(0);
                    } else {
                        try {
                            lock.wait();
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }
    }

    public void run() {
        while (true) {
            synchronized (lock) {
                try {
                    lock.wait(pushInterval);
                } catch (Exception ex) {
                }
            }

            passiveHandler.get().push();
            passiveHandler.get().flush();
            msgSize.set(0);
        }
    }

    public static void shutdown() {
        synchronized (lock) {
            lock.notifyAll();
        }
    }

    // Handler Methods
    public void flush() {
        mh.flush();
        backupMh.flush();
    }

    public void close() throws SecurityException {
        mh.close();
        backupMh.close();
    }

    public void setFormatter(Formatter newFormatter) throws SecurityException {
        mh.setFormatter(newFormatter);
        backupMh.setFormatter(newFormatter);
    }

    public Formatter getFormatter() {
        return activeHandler.get().getFormatter();
    }

    public void setEncoding(String encoding)
        throws SecurityException, UnsupportedEncodingException {
        mh.setEncoding(encoding);
        backupMh.setEncoding(encoding);
    }

    public String getEncoding() {
        return activeHandler.get().getEncoding();
    }

    public void setFilter(Filter newFilter) throws SecurityException {
        mh.setFilter(newFilter);
        backupMh.setFilter(newFilter);
    }

    public Filter getFilter() {
        return activeHandler.get().getFilter();
    }

    public void setErrorManager(ErrorManager em) {
        mh.setErrorManager(em);
        backupMh.setErrorManager(em);
    }

    public ErrorManager getErrorManager() {
        return activeHandler.get().getErrorManager();
    }

    public void setLevel(Level newLevel) throws SecurityException {
        mh.setLevel(newLevel);
        backupMh.setLevel(newLevel);
    }

    public Level getLevel() {
        return activeHandler.get().getLevel();
    }

    public boolean isLoggable(LogRecord record) {
        return activeHandler.get().isLoggable(record);
    }
}

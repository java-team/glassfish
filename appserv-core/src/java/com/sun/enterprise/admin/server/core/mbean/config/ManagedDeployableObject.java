/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.admin.server.core.mbean.config;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.enterprise.instance.InstanceEnvironment;
import com.sun.enterprise.admin.event.AdminEvent;
import com.sun.enterprise.admin.event.AdminEventCache;
import com.sun.enterprise.admin.event.AdminEventResult;
import com.sun.enterprise.admin.event.ApplicationDeployEvent;
import com.sun.enterprise.admin.event.ModuleDeployEvent;
import com.sun.enterprise.admin.event.BaseDeployEvent;
import com.sun.enterprise.admin.event.AdminEventMulticaster;
import com.sun.enterprise.admin.server.core.channel.RMIClient;
import com.sun.enterprise.admin.server.core.channel.AdminChannel;
import com.sun.enterprise.admin.common.Status;
import com.sun.enterprise.config.serverbeans.ApplicationHelper;
import com.sun.enterprise.config.serverbeans.ApplicationRef;
import com.sun.enterprise.config.serverbeans.ServerBeansFactory;
import com.sun.enterprise.config.serverbeans.Server;
import com.sun.enterprise.config.ConfigBean;
import com.sun.enterprise.config.ConfigContext;


/**
 * The base class that represents a Managed JavaEE application, standalone 
 * module and deployable extension module.
 */

public class ManagedDeployableObject extends ConfigMBeanBase
{

    /**
     * Multicasts the admin event so that the application gets loaded
     * dynamically without the need for reconfig. 
     */
    protected void multicastAdminEvent(String entityName, String actionCode, 
        String eventModuleType) {
        String instanceName = super.getServerInstanceName();
        AdminEvent event; 
        if (eventModuleType.equals(BaseDeployEvent.APPLICATION)) {
            event = new ApplicationDeployEvent(instanceName, entityName, 
                actionCode);
        } else {
            event = new ModuleDeployEvent(instanceName, entityName, 
                eventModuleType, actionCode);
        }

        // When the action is to start the application, set the enable 
        // attributes of the application to true locally so the application
        // will always be loaded regardless of its original enable attributes.
        // The event will be set to transient so this config change will 
        // not be persisted. 
        if (actionCode.equals(BaseDeployEvent.ENABLE) ) {
            try {
                ConfigContext ctx = (ConfigContext)getConfigContext().clone();
                ConfigBean appBean =
                    ApplicationHelper.findApplication(ctx, entityName);
                appBean.setEnabled(true); 
                Server server = ServerBeansFactory.getServerBean(ctx);
                ApplicationRef appRef = 
                    server.getApplicationRefByRef(entityName);
                appRef.setEnabled(true);
                if (ctx.getConfigChangeList() != null && 
                    ctx.getConfigChangeList().size() > 0) {
                    event.addConfigChange(ctx.getConfigChangeList());
                    event.setTransient(true);
                }
            } catch (Exception e) {
                sLogger.log(Level.WARNING, e.getMessage(), e);
            }
        }
        RMIClient serverInstancePinger = AdminChannel.getRMIClient(instanceName);
        if (serverInstancePinger.getInstanceStatusCode() != 
            Status.kInstanceRunningCode) {
            return;
        }
        AdminEventResult multicastResult = AdminEventMulticaster.multicastEvent(
            event);
        if (!AdminEventResult.SUCCESS.equals(multicastResult.getResultCode())) {
                AdminEventCache cache = AdminEventCache.getInstance(instanceName);
                    cache.setRestartNeeded(true);
        }
    }
}

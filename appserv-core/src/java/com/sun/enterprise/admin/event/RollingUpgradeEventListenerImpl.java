/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/**
 * PROPRIETARY/CONFIDENTIAL.  Use of this product is subject to license terms.
 *
 * Copyright 2001-2002 by iPlanet/Sun Microsystems, Inc.,
 * 901 San Antonio Road, Palo Alto, California, 94303, U.S.A.
 * All rights reserved.
 */
package com.sun.enterprise.admin.event;

import com.sun.enterprise.admin.common.constant.AdminConstants;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
//import com.sun.enterprise.ee.web.sessmgmt.RollingUpgradeHandler;
//import com.sun.enterprise.ee.web.sessmgmt.RollingUpgradeException;

/**
 * Listener implementation to handle rolling upgrade event.
 */
public class RollingUpgradeEventListenerImpl implements RollingUpgradeEventListener {

    /**
     * A reference to logger object
     */
    static Logger logger = Logger.getLogger(AdminConstants.kLoggerName);

    public static final String BACKUP_EVENT     = "backupSession";
    public static final String RESTORE_EVENT    = "restoreSession";
    public static final String RECONCILE_EVENT  = "reconcileSession";

    private Class c;
    private final String ROLLING_UPGRADE_CLASSNAME = "com.sun.enterprise.ee.web.sessmgmt.RollingUpgradeHandler";

    /**
     * process the event
     * @throws AdminEventListenerException when the listener is unable to
     *         process the event.
     */
    public void processEvent(RollingUpgradeEvent event)
            throws AdminEventListenerException {
        String operation = event.getOperation();
        long waitTime = event.getWaitTime();
        Method[] allMethods = null;
        try {
            Class c = Class.forName(ROLLING_UPGRADE_CLASSNAME);
            allMethods = c.getDeclaredMethods();
        } catch (Exception ex) {
            throw new AdminEventListenerException(ex.getMessage());
        }
        if (operation.equals(BACKUP_EVENT)) {
            long drainTime = event.getDrainTime();
            backupSessionStore(allMethods, waitTime, drainTime);
        } else if (operation.equals(RESTORE_EVENT)) {
            restoreSessionStore(allMethods, waitTime);
        } else if (operation.equals(RECONCILE_EVENT)) {
            reconcileSessionStore(allMethods, waitTime);
        }
    }

    /**
     * backup the session store
     * @throws AdminEventListenerException when the listener is unable to
     *         process the event.
     */
    public void backupSessionStore(Method[] allMethods, long waitTime, long drainTime) throws AdminEventListenerException {
        logger.log(Level.FINE, "Backing up the session store");
        try {
            Method m = getMethod(allMethods, "backupSessionState");
            if ( m != null)
                m.invoke(c, waitTime, drainTime);
        } catch (Exception rue) {
            throw new AdminEventListenerException(rue.getMessage());
        }
    }

    /**
     * restore the session store
     * @throws AdminEventListenerException when the listener is unable to
     *         process the event.
     */
    public void restoreSessionStore(Method[] allMethods, long waitTime) throws AdminEventListenerException {
        logger.log(Level.FINE, "Restoring the session store");
        try {
            Method m = getMethod(allMethods, "restoreSessionState");
            if ( m != null)
                m.invoke(c, waitTime);
        } catch (Exception rue) {
            throw new AdminEventListenerException(rue.getMessage());
        }
    }

    /**
     * reconcile the session store
     * @throws AdminEventListenerException when the listener is unable to
     *         process the event.
     */
    public void reconcileSessionStore(Method[] allMethods, long waitTime) throws AdminEventListenerException {
        logger.log(Level.FINE, "Reconcile the session store");
        try {
            Method m = getMethod(allMethods, "reconcileSessionState");
            if ( m != null)
                m.invoke(c, waitTime);
        } catch (Exception rue) {
            throw new AdminEventListenerException(rue.getMessage());
        }
    }

    public Method getMethod(Method[] allMethods, String methodName) throws Exception {
        Method method = null;
        for (Method m : allMethods) {
            String mname = m.getName();
            if (mname.startsWith(methodName))
                method = m;
        }
        return method;
    }
}
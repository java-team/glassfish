/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package com.sun.enterprise.instance;

import com.sun.enterprise.deployment.archivist.ApplicationArchivist;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

import javax.enterprise.deploy.shared.ModuleType;

import com.sun.logging.LogDomains;
import com.sun.enterprise.config.ConfigBean;
import com.sun.enterprise.config.ConfigContext;
import com.sun.enterprise.config.ConfigException;
import com.sun.enterprise.config.serverbeans.*;
import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveDescriptor;
import com.sun.enterprise.deployment.archivist.ExtensionModuleArchivist;
import com.sun.enterprise.deployment.backend.DeployableObjectType;
import com.sun.enterprise.deployment.pluggable.PluggableDeploymentInfo;
import com.sun.enterprise.deployment.util.ModuleDescriptor;
import com.sun.enterprise.deployment.util.ModuleContentLinker;
import com.sun.enterprise.deployment.Application;
import com.sun.enterprise.deployment.BundleDescriptor;
import com.sun.enterprise.server.ApplicationServer;
import com.sun.enterprise.deployment.deploy.shared.FileArchive;
import com.sun.enterprise.deployment.util.ModuleContentLinker;
//The RelativePathResolver is used to translate relative paths containing 
//embedded system properties (e.g. ${com.sun.aas.instanceRoot}/descriptors) 
//into absolute paths
import com.sun.enterprise.deployment.util.XModuleType;
import com.sun.enterprise.util.RelativePathResolver;
import com.sun.enterprise.util.SystemPropertyConstants;
import com.sun.enterprise.util.io.FileUtils;

public class ExtensionModuleConfigManager extends ModulesManager {
    
    private Hashtable<String, ArchiveDescriptor> extensionDescriptors 
        = new Hashtable<String, ArchiveDescriptor>();
    private ModuleType moduleType = null;
    private ExtensionModuleArchivist extensionModuleArchivist = null;

    protected static final Logger _logger=
                            LogDomains.getLogger(LogDomains.CORE_LOGGER);
  
    public ExtensionModuleConfigManager(InstanceEnvironment env, 
        ModuleType mType) throws ConfigException {
        super(env, false);
        moduleType = mType;
        extensionModuleArchivist = 
            PluggableDeploymentInfo.getExtensionModuleArchivist(moduleType);
    }

    /**
     * @return the module type this class is managing
     */
    public ModuleType getModuleType() {
        return moduleType;
    }    

    public void registerExtensionDescriptor(String appName, 
        ArchiveDescriptor descriptor) {
        extensionDescriptors.put(appName, descriptor);
    }

    public void unregisterExtensionDescriptor(String appName) {
        extensionDescriptors.remove(appName);
    }

    public ArchiveDescriptor getRegisteredExtensionDescriptor(String 
        appName) {
        return extensionDescriptors.get(appName);
    }

    /**
     * Returns an array of all extension modules wth this module type 
     * that are deployed with the server.
     */
    public ExtensionModule[] listExtensionModules() {
        ExtensionModule[] mods = ((Applications)this.configBean
            ).getExtensionModule();
        if(mods == null) return new ExtensionModule[0];

        ArrayList list = new ArrayList();
        for (int i=0; i<mods.length; i++) {
            // add the modules to the list if the module type matches and 
            // it is referenced by this server 
            if ( mods[i].getModuleType().equals((XModuleType)moduleType) && 
                isReferenced(mods[i].getName())  ) {
                list.add(mods[i]);
            }            
        }
        // returns an array of modules referenced by this server
        ExtensionModule[] refList = new ExtensionModule[list.size()];
        return ( (ExtensionModule[]) list.toArray(refList) );
    }

   /**
     * Returns an array of all extension modules deployed with the server.
     */
    public ExtensionModule[] listAllExtensionModules() {
        ExtensionModule[] mods = ((Applications)this.configBean
            ).getExtensionModule();
        if(mods == null) return new ExtensionModule[0];
                                                                                
        ArrayList list = new ArrayList();
        for (int i=0; i<mods.length; i++) {
            // add the modules to the list if
            // it is referenced by this server
            if (isReferenced(mods[i].getName())  ) {
                list.add(mods[i]);
            }
        }
        // returns an array of modules referenced by this server
        ExtensionModule[] refList = new ExtensionModule[list.size()];
        return ( (ExtensionModule[]) list.toArray(refList) );
    }

   /**
     * Returns an array of all extension module types deployed with the server.
     */
    public String[] listAllExtensionModuleTypes() {
        ExtensionModule[] mods = ((Applications)this.configBean
            ).getExtensionModule();
        if(mods == null) return new String[0];
                                                                                
        Set<String> moduleTypes = new HashSet<String>();
        for (int i=0; i<mods.length; i++) {
            // add the module type to the list if
            // the module is referenced by this server
            if (isReferenced(mods[i].getName())) {
                moduleTypes.add(mods[i].getModuleType());
            }
        }

        String[] types = new String[moduleTypes.size()];
        return (String[]) moduleTypes.toArray(types);
    }



   /**
     * Returns a list of all extension module ids with this module type 
     * that are deployed with the server.
     */
    public List listIds() {
        ArrayList arr = new ArrayList();
        ExtensionModule[] mods = ((Applications)this.configBean
            ).getExtensionModule();
        if(mods == null) return arr;

        for (int i=0;i<mods.length;i++) {
            String name = mods[i].getName();
            // adds the extension module to the list if 
            // it is referenced by this server             
            if ( mods[i].getModuleType().equals(moduleType.toString()) && 
                isReferenced(name) ) {
                arr.add(name);
            }            
        }
        return arr;
    }
    
    /**
     * Returns a list of Virtual Servers where extensionModules with this ID 
     * are deployed
     */
    public String getVirtualServers(String modId) {
        try {
            return ServerBeansFactory.getVirtualServersByAppName(configContext,
                                                                         modId);      
        } catch (ConfigException ce) {
            //ce.printStackTrace();
            //XXX: log the exception
            return null;
        }                                                                                                                        
    }
    
    // Method used to lookup/remove/ lookup properties of the extension module
    // from the extension module bean
 
    public ExtensionModule getExtensionModule(String modId) throws ConfigException {
        ExtensionModule mod = (ExtensionModule)
            ((Applications)this.configBean
                ).getExtensionModuleByName(modId);
        
        return mod;
    }
    
    public void remove(String modID) throws ConfigException {
        removeExtensionModule(modID);
    }
    
     private void removeExtensionModule(String modId) throws ConfigException {
        ExtensionModule backEm = (ExtensionModule)
            ((Applications)configBean
            ).getExtensionModuleByName(modId);
        ((Applications)configBean).removeExtensionModule(
            backEm);
    }
    
    protected boolean isRegistered(String appId, ConfigBean bean) {
        ConfigBean cb = null;
        try {
            cb = ((Applications)bean).getExtensionModuleByName(
                appId);
        } catch(Exception cn) {
        }
        
        if(cb != null) return true;
        return false;
    }
    
    public boolean isShared(String modId)  throws ConfigException{
        return false; 
    }
    
    public boolean isEnabled(String modId)  throws ConfigException{
        return getExtensionModule(modId).isEnabled();
    }

    /**     
     * Checks whether this module is a systemmodule 
     * ResourceType in domain.xml should start with "system-"
     * @return true if resourceType starts with "system-"
     */     
    public boolean isSystem(String modId)  throws ConfigException{
        return false;
    }

    /**
     * Checks whether this module is a system admin module
     * ResourceType in domain.xml should start with "system-admin"
     * @return true if resourceType starts with "system-admin"
     */
    public boolean isSystemAdmin(String modId)  throws ConfigException{
        return false;
    }
    
    /**
     * Checks whether this module is a pre-deployed system module
     * ResourceType in domain.xml should start with "system"
     * Also it should be directory deployed
     * @return true if its a predeployed system module
     */
    public boolean isSystemPredeployed (String modId)  throws ConfigException{
        return false;
    }

    public void setShared(String modId, boolean shared)  throws ConfigException{
    }
    
    public void setEnable(String modId, boolean enable)  throws ConfigException{
        getExtensionModule(modId).setEnabled(enable);
    }
    
	/**
     * Set the optional attributes for an module
     *
     * @param modId unique idenitifier for the module
     * @param optionalAttributes - pairs tag/value to set
     */
    public void setOptionalAttributes(String modId, 
        Properties optionalAttributes) throws ConfigException {
        if(optionalAttributes!=null) {
            ExtensionModule em = getExtensionModule(modId);
            Enumeration tags = optionalAttributes.keys();
            while(tags.hasMoreElements())
            {
                String tag = (String)tags.nextElement();
                String value = optionalAttributes.getProperty(tag);
                em.setAttributeValue(tag, value);
            }
        }
    }
    
    public String getLocation(String name) throws ConfigException {
        ExtensionModule extensionModule = 
                ((Applications) this.configBean).getExtensionModuleByName(name);
        String location = extensionModule.getLocation();
        return resolvePath(location);
    }

    /**
     * Set the location for an extension Module
     *
     * @param modId unique idenitifier for the extension module
     * @param location full path String
     */
    
    public void setLocation(String modId, String location)  
        throws ConfigException{
        getExtensionModule(modId).setLocation(location);
    }
   
   /**
    * Returns the description of the module
    * @return the description of the module
    */ 
    public String getDescription(String modId) throws ConfigException {
        return getExtensionModule(modId).getDescription();
    }
   
    /**
     * Sets the description for the module in the config bean
     * @param modId the module ID
     * @param desc the description
     * @throws ConfigException
     */ 
    public void setDescription(String modId, String desc) 
            throws ConfigException {
        getExtensionModule(modId).setDescription(desc);
    }

    /**
     * retrives the descriptor for the extension module. This is an overloaded
     * method that retrieves the descriptor based on module id and location
     * of the exploded module 
     * @param modId the Module ID
     * @param location the location on the disk of the exploded application
     * @throws ConfigException
     */
    public Application getExtensionDescriptor(String modId, String location)
                                                throws ConfigException {
        return getExtensionDescriptor(modId, location, false);
    }
                                                 
    
    /** 
     * Returns the deployment descriptor object for this extension module. This
     * method gets called when deployment backend is running verification 
     * during deployment.Takes in an additional parameter to determine if this
     * is being called at deployment time or at startup time.
     *
     * @param    modId        extension module id
     * @param    parentClassLoader parentClassLoader
     * @param    boolean the flag to determine if this is a startup call.
     * @return   the deployment descriptor object for this extension module
     *
     * @throws   ConfigException  if unable to load the deployment descriptor
     */
    public Application getExtensionDescriptor(String modId, 
                           String location, boolean isStartup)
                                 throws ConfigException {
        
        // Try and get it from the map of registered descriptors
        ArchiveDescriptor descriptor =
            getRegisteredExtensionDescriptor(modId);
        if (descriptor!=null) {
            //wrap it as an Application object
            return getApplication(modId, descriptor);
         }
          
         // Next try and retrieve it from the exploded archive
         try {
            //String loadingPath = getLocation(modId);
            File moduleScratchDir = new File(
                instanceEnvironment.getModuleGeneratedXMLPath(), modId);
           
            ClassLoader parentClassLoader = 
                    ApplicationServer.getServerContext().getSharedClassLoader();
	    descriptor =
                    PluggableDeploymentInfo.getExtensionModuleArchivist(
                        moduleType).openArchive(
                            location, moduleScratchDir, 
                                parentClassLoader, isStartup); 
            
            // set it in all the bundles as well,
            registerExtensionDescriptor(modId, descriptor);
            
            return getApplication(modId, descriptor);
         } catch (ConfigException ce) {
            throw ce;
        } catch (Throwable t) {
            throw new ConfigException(Localizer.getValue(
                        ExceptionType.FAIL_DD_LOAD, modId), t);
        }
    }
   
    /** 
     * Returns the deployment descriptor object for this extension module. This
     * method gets called when deployment backend is running verification 
     * during deployment.
     *
     * @param    modId        extension module id
     * @param    parentClassLoader parentClassLoader
     *
     * @return   the deployment descriptor object for this extension module
     *
     * @throws   ConfigException  if unable to load the deployment descriptor
     */
    public ArchiveDescriptor getExtensionDescriptor(String modId, 
        ClassLoader parentClassLoader) throws ConfigException {
        ArchiveDescriptor descriptor =
            getRegisteredExtensionDescriptor(modId);

        if (descriptor!=null) {
            return descriptor;
         }
       
        try {
            String loadingPath = getLocation(modId);
            File moduleScratchDir = new File(
                instanceEnvironment.getModuleGeneratedXMLPath(), modId);

	    descriptor = 
                 PluggableDeploymentInfo.getExtensionModuleArchivist(
                                           moduleType).openArchive(
                                               loadingPath, moduleScratchDir, 
                                                    parentClassLoader, true); 
            
            // set it in all the bundles as well,
            registerExtensionDescriptor(modId, descriptor);
            
            return  descriptor;
        } catch (ConfigException ce) {
            throw ce;
        } catch (Throwable t) {
            throw new ConfigException(Localizer.getValue(
                        ExceptionType.FAIL_DD_LOAD, modId), t);
        }
    }
       
    /**
     * Return the location where stubs are generated
     * @param name Name of the module 
     * @return location of the generated stubs 
     */
    public String getStubLocation(String name) {
        return instanceEnvironment.getModuleStubPath();
    }
   
    /**
     * Returns the location where the modified descriptors are stored
     * @param name the name of the module
     * @return the location where the modified descriptors are stored
     */ 
    public String getGeneratedXMLLocation(String name){
        return instanceEnvironment.getModuleGeneratedXMLPath() +
                File.separator + name;
    } 

    public Application getDescriptor(String modId, ClassLoader cl, 
                       String loc, boolean validateXML) 
                             throws ConfigException {
        return null;
    }
    
    /**
     * This method wraps the descriptor object as an Application object
     * @param modId the module ID
     * @param desc the Archive descriptor that needs to be wrapped
     * @return Application  the Java EE Application Descriptor object
     */
    public Application getApplication(String modId, ArchiveDescriptor desc) {        
        ModuleDescriptor modDesc = new ModuleDescriptor();
        modDesc.setDescriptor((BundleDescriptor)desc);
        return Application.createApplication(modId, modDesc);
    }
        
}

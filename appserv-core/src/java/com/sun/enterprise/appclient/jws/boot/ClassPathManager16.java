/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.appclient.jws.boot;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.jar.JarFile;
import java.util.zip.ZipFile;

/**
 * Class Path manager for Java Web Start-aware ACC running under Java runtime 1.6.
 *
 * @author tjquinn
 */
public class ClassPathManager16 extends ClassPathManager {
    
    /** Class object for the JNLPClassLoader class */
    private Class jnlpClassLoaderClass;

//    /** Method object for the getJarFile method on JNLPClassLoader - only in 1.6 and later */
//    private Method getJarFileMethod;

    /** Field object for the name field on ZipFile - only needed for 6 update 7 and later */
    private Field nameField;
    /**
     *Returns a new instance of the class path manager for use under Java 1.6
     *@param loader the Java Web Start-provided class loader
     */
    protected ClassPathManager16(ClassLoader loader, boolean keepJWSClassLoader) {
        super(loader, keepJWSClassLoader);
        try {
            prepareIntrospectionInfo();
        } catch (Throwable thr) {
            throw new RuntimeException(thr);
        }
    }

    /**
     *Prepares the reflection-related private variables for later use in 
     *locating classes in JARs.
     *@throws ClassNotFoundException if the JNLPClassLoader class cannot be found
     *@throws NoSuchMethodException if the getJarFile method cannot be found
     */
    private void prepareIntrospectionInfo() throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException {
        jnlpClassLoaderClass = getJNLPClassLoader().loadClass("com.sun.jnlp.JNLPClassLoader");
//        getJarFileMethod = jnlpClassLoaderClass.getDeclaredMethod("getJarFile", URL.class);
//        getJarFileMethod.setAccessible(true);

        nameField = ZipFile.class.getDeclaredField("name");
        nameField.setAccessible(true);

    }

    public ClassLoader getParentClassLoader() {
        return (keepJWSClassLoader() ? getJnlpClassLoader() : getJNLPClassLoader().getParent());
    }

    public File findContainingJar(URL resourceURL) throws IllegalArgumentException, URISyntaxException, MalformedURLException, IllegalAccessException, InvocationTargetException, IOException {
        File result = null;
        if (resourceURL != null) {
            URLConnection urlConnection = resourceURL.openConnection();
            String jarFileName = null;
            if (urlConnection instanceof JarURLConnection) {
                JarFile jarFile = ((JarURLConnection) urlConnection).getJarFile();
                if (jarFile == null) {
                    throw new IllegalArgumentException("urlConnection.getJarFile == null: " + resourceURL.toExternalForm());
                }
                jarFileName = jarFile.getName();
                /*
                 * Handle releases of Java SE that do not expose the jar's name.
                 */
                if (jarFileName == null || jarFileName.length() == 0) {
                   jarFileName = getNameFromZip(jarFile);
                }
            }

            if (jarFileName == null) {
                throw new IllegalArgumentException(resourceURL.toExternalForm());
            }
            result = new File(jarFileName);
        }
        return result;
    }

    private String getNameFromZip(JarFile jarFile) throws IllegalArgumentException, IllegalAccessException {
        String name = (String) nameField.get(jarFile);
        return name;

    }
}

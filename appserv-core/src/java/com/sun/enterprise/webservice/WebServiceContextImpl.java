/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.webservice;

import java.security.Principal;
import java.util.Set;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.EndpointReference;
import com.sun.web.security.WebPrincipal;
import com.sun.enterprise.Switch;
import com.sun.enterprise.InvocationManager;
import com.sun.ejb.containers.StatelessSessionContainer;
import com.sun.enterprise.deployment.WebComponentDescriptor;
import com.sun.enterprise.security.AppservAccessController;
import com.sun.enterprise.security.SecurityContext;
import com.sun.enterprise.web.WebModule;
import com.sun.web.security.RealmAdapter;
import com.sun.xml.ws.api.server.WSWebServiceContext;
import com.sun.xml.ws.api.message.Packet;
import java.security.PrivilegedAction;
import java.util.Iterator;
import javax.security.auth.Subject;
import org.apache.catalina.Realm;

/**
 * <p><b>NOT THREAD SAFE: mutable instance variables</b>
 */
public final class WebServiceContextImpl implements WSWebServiceContext {
    
    public static final ThreadLocal msgContext = new ThreadLocal();
    
    public static final ThreadLocal principal = new ThreadLocal();

    private WSWebServiceContext jaxwsContextDelegate;
    
    //needed to implement isUserInRole()
    private String servletName;
    private final String JAXWS_SERVLET = "com.sun.enterprise.webservice.JAXWSServlet";
    
    public void setContextDelegate(WSWebServiceContext wsc) {
        this.jaxwsContextDelegate = wsc;
    }
    
    public MessageContext getMessageContext() {
        return this.jaxwsContextDelegate.getMessageContext();
    }

    public void setMessageContext(MessageContext ctxt) {
        msgContext.set(ctxt);
    }

    /*
     * this may still be required for EJB endpoints
     *
     */
    public void setUserPrincipal(final WebPrincipal p) {
        /*
        if (p == null) {
            SecurityContext.setCurrent(null);
            return;
        }
        final Subject subject = new Subject();
        AppservAccessController.doPrivileged(new PrivilegedAction() {
            public java.lang.Object run() {
                subject.getPrincipals().add(p);
                return null;
            }
        });
        SecurityContext ctx = new SecurityContext(subject);
        SecurityContext.setCurrent(ctx);*/
        principal.set(p);
    }

    public Principal getUserPrincipal() {
        // This could be an EJB endpoint; check the threadlocal variable
        Switch sw = Switch.getSwitch();
        InvocationManager mgr = sw.getInvocationManager();
        Object o = mgr.getCurrentInvocation().getContainerContext();
        if (o instanceof StatelessSessionContainer) {
            WebPrincipal p = (WebPrincipal) principal.get();
            if (p != null) {
                return p;
            }
        }
        //This is a servlet endpoint
        SecurityContext ctx = SecurityContext.getCurrent();
        if (ctx == null) {
            return null;
        }
        if (ctx.didServerGenerateCredentials()) {
            if (o instanceof WebModule) {
                return null;
            }
        }
        return ctx.getCallerPrincipal();
    } 

     public boolean isUserInRole(String role) {
        Switch sw = Switch.getSwitch();
        InvocationManager mgr = sw.getInvocationManager();
        Object o = mgr.getCurrentInvocation().getContainerContext();
        if(o instanceof StatelessSessionContainer) {
            StatelessSessionContainer cont = (StatelessSessionContainer) o;
            boolean res = cont.getSecurityManager().isCallerInRole(role);
            return res;
        } else if (o instanceof WebModule) {
            WebModule webModule = (WebModule)o;
            Realm realm = webModule.getRealm();
            if (realm instanceof RealmAdapter) {
                RealmAdapter realmAdapter = (RealmAdapter)realm;
                Principal princ = getUserPrincipal();
                return realmAdapter.hasRole(servletName, princ, role);
            }
        }
        // This is a servlet endpoint
        return this.jaxwsContextDelegate.isUserInRole(role);
    }
   
    
    public EndpointReference getEndpointReference(Class clazz, org.w3c.dom.Element... params) {
        return this.jaxwsContextDelegate.getEndpointReference(clazz, params);
    }
    
    public EndpointReference getEndpointReference(org.w3c.dom.Element... params) {
        return this.jaxwsContextDelegate.getEndpointReference(params);
    }
    
    public Packet getRequestPacket() {
        return this.jaxwsContextDelegate.getRequestPacket();
    }
    
    void setServletName(Set webComponentDescriptors) {
        Iterator it = webComponentDescriptors.iterator();
        String endpointName = null;
        while (it.hasNext()) {
            WebComponentDescriptor desc = (WebComponentDescriptor)it.next();
            String name = desc.getCanonicalName();
            if (JAXWS_SERVLET.equals(desc.getWebComponentImplementation())) {
                endpointName = name;
            }
            if (desc.getSecurityRoleReferences().hasMoreElements()) {
                servletName = name;
                break;
            }
        }
        if (servletName == null) {
            servletName = endpointName;
        }
    }
}

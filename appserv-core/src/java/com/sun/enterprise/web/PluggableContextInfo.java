/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * PluggableContextInfo.java
 *
 * Created on March 21, 2007, 6:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sun.enterprise.web;

import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.ResourceBundle;

import javax.enterprise.deploy.shared.ModuleType;

import com.sun.logging.LogDomains;

/**
 * The PluggableContextInfo class provides a registry for mapping
 * the ContextConfig object to be used for each Module Type
 * The class becomes more relevant when the WebContainer is extended
 * and ModuleType other than WAR are deployed on to this extended
 * Web Container
 * @author ps125818
 */
public class PluggableContextInfo {
    
    private static HashMap contextConfigRegistry = null;
    private static HashMap contextRegistry = null;
    private static HashMap  customRealms= null;
    private static final String WEBMODULE_CONTEXT_CONFIG =
                               "com.sun.enterprise.web.WebModuleContextConfig";
    private static final String WEBMODULE = 
                                   "com.sun.enterprise.web.WebModule"; 
    protected static final Logger _logger = LogDomains.getLogger(
                                                     LogDomains.WEB_LOGGER);
    protected static final ResourceBundle _rb = _logger.getResourceBundle();
  

    static {
        contextConfigRegistry = new HashMap<String, String>();
        contextConfigRegistry.put((ModuleType.WAR).toString(),
                                           WEBMODULE_CONTEXT_CONFIG);
        contextRegistry = new HashMap<String, String>();
        customRealms = new HashMap<String, String>();
        contextRegistry.put((ModuleType.WAR).toString(),
                                           WEBMODULE); 
    }
   
    /**
     * Registers a custom ContextConfig object for a particular module type
     * @param moduleType the module type for which the ContextConfig is 
     *                   being resgistered
     * @param contextConfig the FQ class name of the ContextConfig
     */ 
    public static void registerContextConfig(String moduleType, 
                                                 String contextConfig) {
        contextConfigRegistry.put(moduleType, contextConfig);
        // String msg = _rb.getString("enterprise.web.registerContextConfig");
        if (_logger.isLoggable(Level.FINE )) {
            _logger.log(Level.FINE, "Registering custom ContextConfig",
                contextConfig);
        }
    }
   
    /**
     * Returns the ContextConfig class name registered for this module type
     * @param moduleType the module type for which the ContextConfig is being 
     *                   retrieved.
     * @return the class name of the ContextConfig
     */ 
    public static String getContextConfig(String moduleType) {
        return (String)contextConfigRegistry.get(moduleType);
    }
    /**
     * Registers a custom Context object for a particular module type
     * @param moduleType the module type for which the Context is
     *                   being resgistered
     * @param contextConfig the FQ class name of the Context
     */
    public static void registerContext(String moduleType,
                                                 String context) {
        contextRegistry.put(moduleType, context);
        // String msg = _rb.getString("enterprise.web.registerContextConfig");
        if(_logger.isLoggable(Level.FINE)) {
            _logger.log(Level.FINE, "Registering Custom Context", context);
        }
    }

    
    public static void registerCustomRealmAdapter(String moduleType, String realmClass ){
        customRealms.put(moduleType, realmClass);        
    }
    
    public static String getCustomRealmAdapter(String moduleType){
        return (String) customRealms.get(moduleType);
    }
    /**
     * Returns the Context class name registered for this module type
     * @param moduleType the module type for which the ContextConfig is being
     *                   retrieved.
     * @return the class name of the Context
     */
    public static String getContext(String moduleType) {
        return (String)contextRegistry.get(moduleType);
    }
}

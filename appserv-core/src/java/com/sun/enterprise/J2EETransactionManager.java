/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise;

import com.sun.ejb.*;
import com.sun.enterprise.resource.ResourceHandle;
import javax.transaction.*;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;
import java.rmi.RemoteException;
import javax.resource.spi.XATerminator;
import javax.resource.spi.work.WorkException;

// START IASRI 4705808 TTT002 -- use List instead of Vector
import java.util.List;
// END IASRI 4705808 TTT002

/**
 *
 * Manages transacations, acting as a gateway to the TM state machine.
 *
 * @author Tony Ng
 */
public interface J2EETransactionManager extends TransactionManager {

    /**
     * register a synchronization object with the transaction
     * associated with the current thread
     *
     * @param sync the synchronization object
     *
     * @exception IllegalStateException Thrown if the transaction in the
     *    target object is in prepared state or the transaction is inactive.
     *
     * @exception SystemException Thrown if the transaction manager
     *    encounters an unexpected error condition
     *
     */
    public void registerSynchronization(Synchronization sync)
        throws RollbackException, IllegalStateException, SystemException;


    /**
     * Enlist the resource specified with the transaction
     *
     *
     * @param tran The transaction object
     *
     * @param h The resource handle object
     *
     * @return <i>true</i> if the resource was enlisted successfully; otherwise     *    false.
     *
     * @exception RollbackException Thrown to indicate that
     *    the transaction has been marked for rollback only.
     *
     * @exception IllegalStateException Thrown if the transaction in the
     *    target object is in prepared state or the transaction is inactive.
     *
     * @exception SystemException Thrown if the transaction manager
     *    encounters an unexpected error condition
     *
     */
    public boolean enlistResource(Transaction tran,
                                  ResourceHandle h)
        throws RollbackException,
               IllegalStateException, SystemException;

    /**
     * Delist the resource specified from the transaction
     *
     * @param tran The transaction object
     *
     * @param h The resource handle object
     *
     * @param flag One of the values of TMSUCCESS, TMSUSPEND, or TMFAIL.
     *
     * @exception IllegalStateException Thrown if the transaction in the
     *    target object is inactive.
     *
     * @exception SystemException Thrown if the transaction manager
     *    encounters an unexpected error condition
     *
     */
    public boolean delistResource(Transaction tran,
                                  ResourceHandle h,
                                  int flag)
        throws IllegalStateException, SystemException;

    /**
     * This is called by the Container to ask the Transaction
     * Manager to enlist all resources held by a component and
     * to associate the current Transaction with the current
     * Invocation
     * The TM finds the component through the InvocationManager
     */
    public void enlistComponentResources() throws RemoteException;

    /**
     * This is called by the Container to ask the Transaction
     * Manager to delist all resources held by a component
     *
     * The TM finds the component through the InvocationManager
     *
     * @param suspend true if the resources should be delisted
     * with TMSUSPEND flag; false otherwise
     *
     */
    public void delistComponentResources(boolean suspend)
        throws RemoteException;

    /**
     * This is called by Container to indicate that a component
     * is being destroyed. All resources registered in the context
     * should be released
     *
     * @param instance The component instance
     */
    public void componentDestroyed(Object instance);

    public void ejbDestroyed(ComponentContext context);

    /**
     * Called by InvocationManager
     */

    public void preInvoke(ComponentInvocation prev)
	throws InvocationException;

    /**
     * Called by InvocationManager
     */

    public void postInvoke(ComponentInvocation curr, ComponentInvocation prev)
	throws InvocationException;

    public void setDefaultTransactionTimeout(int seconds);
    public void cleanTxnTimeout(); // clean up thread specific timeout
    /**
     * Returns a list of resource handles held by the component
     */

    public List getExistingResourceList(Object instance, ComponentInvocation inv);

    public void registerComponentResource(ResourceHandle h);

    public void unregisterComponentResource(ResourceHandle h);

    public void recover(XAResource[] resourceList);

    public void begin(int timeout)
        throws NotSupportedException, SystemException;

    /**
     * Return true if a "null transaction context" was received
     * from the client or if the server's transaction.interoperability
     * flag is false.
     * A null tx context indicates that the client had an active
     * tx but the client container did not support tx interop.
     * See EJB2.0 spec section 18.5.2.1.
     */
    public boolean isNullTransaction();

    /**
     * Perform checks during export of a transaction on a remote call.
     */
    public void checkTransactionExport(boolean isLocal);

    /**
     * Perform checks during import of a transaction on a remote call.
     * This is called from the reply interceptors after a remote call completes.
     */
    public void checkTransactionImport();


    /**
     * Utility for the ejb container to check if the transaction is marked for
     * rollback because of timeout. This is applicable only for local transactions
     * as jts transaction will rollback instead of setting the txn for rollback
     */
    public boolean isTimedOut();

    // START IASRI 4662745

    /*
     * Returns the list of ActiveTransactions. Called by Admin framework
     *  The ArrayList contains TransactionAdminBean
     */
    public java.util.ArrayList getActiveTransactions();

    /*
     * Called by Admin Framework. Forces the given transaction to be rolled back
     */
    public void forceRollback(Transaction tran) throws IllegalStateException, SystemException;

    /*
     * Called by Admin Framework. Returnes number of transactions commited till now.
     */
    public int getNumberOfTransactionsCommitted();

    /*
     * Called by Admin Framework. Returnes number of transactions rolledback till now.
     */
    public int getNumberOfTransactionsRolledBack();

    /*
     * Called by Admin Framework. Returnes number of Active transactions.
     */
    public int getNumberOfActiveTransactions();

    /*
     * Called by Admin Framework.
     */
    public void setMonitoringEnabled(boolean enabled);

    /*
     * Called by Admin Framework.
     */
    public void freeze();
    /*
     * Called by Admin Framework.
     */
    public void unfreeze();

    /*
     * Called by Admin Framework
     */
    public boolean isFrozen();

    // END IASRI 4662745


   /**
     * recreate a transaction based on the Xid. This call causes the calling
     * thread to be associated with the specified transaction. <p>
     * This is used by importing transactions via the Connector contract.
     *
     * @param xid the Xid object representing a transaction.
     */
    public void recreate(Xid xid, long timeout) throws WorkException ;

    /**
     * Release a transaction. This call causes the calling thread to be
     * dissociated from the specified transaction. <p>
     * This is used by importing transactions via the Connector contract.
     *
     * @param xid the Xid object representing a transaction.
     */
    public void release(Xid xid) throws WorkException ;

    /**
     * Provides a handle to a <code>XATerminator</code> instance. The
     * <code>XATerminator</code> instance could be used by a resource adapter
     * to flow-in transaction completion and crash recovery calls from an EIS.
     * <p>
     * This is used by importing transactions via the Connector contract.
     *
     * @return a <code>XATerminator</code> instance.
     */
    public XATerminator getXATerminator() ;


}

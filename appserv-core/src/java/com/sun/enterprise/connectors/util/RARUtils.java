/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.connectors.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.sun.enterprise.connectors.ConnectorRuntimeException;
import com.sun.enterprise.deployment.ConnectorDescriptor;
import com.sun.enterprise.deployment.EjbMessageBeanDescriptor;
import com.sun.enterprise.deployment.EnvironmentProperty;
import com.sun.logging.LogDomains;
import com.sun.enterprise.util.i18n.StringManager;
import com.sun.enterprise.server.ApplicationServer;

/**
 * This is a utility class to obtain the properties of a
 * RA JavaBean housed in a RAR module/deployment dir, without exploding the RAR
 * contents. This method would be used by the admin-gui to configure
 * RA properties during RA deployment to a cluster.
 *
 * @author Sivakumar Thyagarajan
 */
public class RARUtils {
    static Logger _logger = LogDomains.getLogger(LogDomains.RSR_LOGGER);
    private static StringManager localStrings =
            StringManager.getManager(RARUtils.class);

    /**
     * given a .rar name, specified bean class will be loaded and returned
     * @param rarName resource-adapter name
     * @param beanClassName class that has to be loaded
     * @return loaded class
     * @throws ConnectorRuntimeException when unable to load the class
     */
    public static Class loadClassFromRar(String rarName, String beanClassName) throws ConnectorRuntimeException{
        String rarLocation = getRarLocation(rarName);
        return loadClass(rarLocation, beanClassName);
    }

    /**
     * given the location of .rar (archive / exploded dir), the specified class will be loaded
     * @param pathToDeployableUnit location of .rar (archive / exploded dir)
     * @param beanClassName class that has to be loaded
     * @return loaded class
     * @throws ConnectorRuntimeException when unable to load the class
     */
    private static Class loadClass(String pathToDeployableUnit, String beanClassName) throws ConnectorRuntimeException {
        Class cls = null;

        ClassLoader cl = getClassLoader(pathToDeployableUnit);

        try {
            //Only if RA is a 1.5 RAR, we need to get RA JavaBean properties, else
            //return an empty map.

            if (beanClassName != null && beanClassName.trim().length() != 0) {
                cls = cl.loadClass(beanClassName);
            }
            return cls;
        } catch (ClassNotFoundException e) {
            _logger.info(e.getMessage());
            _logger.log(Level.FINE, "Unable to find class while trying to read connector" +
                    "descriptor to get resource-adapter properties", e);
            ConnectorRuntimeException cre = new ConnectorRuntimeException("unable to find class : " + beanClassName);
            cre.setStackTrace(e.getStackTrace());
            throw cre;
        }
    }

    /**
     * based on the provided file type (dir or archive) appropriate class-loader will be selected
     * @param file file (dir/ archive)
     * @return classloader that is capable of loading the .rar
     * @throws ConnectorRuntimeException when unable to load the .rar
     */
    private static ClassLoader getClassLoader(String file) throws ConnectorRuntimeException {
        ClassLoader cl = null;
        File f = new File(file);
        validateRARLocation(f);
        try {
            if (f.isDirectory()) {
                cl = new URLClassLoader(new URL[]{f.toURI().toURL()},
                        ApplicationServer.getServerContext().getCommonClassLoader());
            } else {
                cl =
                        (new ConnectorRARClassLoader(file,
                                ApplicationServer.getServerContext().getCommonClassLoader()));
            }
            return cl;
        } catch (IOException ioe) {
            _logger.info(ioe.getMessage());
            _logger.log(Level.FINE, "IO Error while trying to read connector" +
                    "descriptor to get resource-adapter properties", ioe);
            ConnectorRuntimeException cre = new ConnectorRuntimeException("unable to read connector descriptor from : " + file);
            cre.setStackTrace(ioe.getStackTrace());
            throw cre;
        }
    }

/*    private static Map getBeanProperties(String className, String rarName) throws ConnectorRuntimeException{
        String pathToDeployableUnit = getRarLocation(rarName);
        Class cls = loadClass(pathToDeployableUnit, className);
        return getJavaBeanProperties(cls);
    } */

    /**
     * given the rar name, location of rar will be returned
     * @param rarName resource-adapter name
     * @return location of resource-adapter
     */
    private static String getRarLocation(String rarName) {
        ResourcesUtil dasUtil = DASResourcesUtil.createInstance();
        return dasUtil.getLocation(rarName);
    }

    /**
     * Finds the properties of a RA JavaBean bundled in a RAR
     * without exploding the RAR
     *
     * @param pathToDeployableUnit a physical,accessible location of the connector module.
     *                             [either a RAR for RAR-based deployments or a directory for Directory based deployments]
     * @return A Map that is of <String RAJavaBeanPropertyName, String defaultPropertyValue>
     *         An empty map is returned in the case of a 1.0 RAR
     */
    public static Map getRABeanProperties(String pathToDeployableUnit) throws ConnectorRuntimeException {

        String beanClassName = getRAClassName(pathToDeployableUnit);
        Class cls = loadClass(pathToDeployableUnit, beanClassName);
        return getJavaBeanProperties(cls);
    }

    /**
     * given the location of .rar (archive / dir), get the ResourceAdapter class name
     * @param pathToDeployableUnit location of .rar (archive / dir)
     * @return RA Bean class name
     * @throws ConnectorRuntimeException when unable to access the .rar
     */
    private static String getRAClassName(String pathToDeployableUnit) throws ConnectorRuntimeException{
        File f = new File(pathToDeployableUnit);
        String raClassName = null;

        validateRARLocation(f);

        if(f.isDirectory()){
            raClassName = getRAClassNameFromDirectory(pathToDeployableUnit);
        }else{
            raClassName = getRAClassNameFromRAR(pathToDeployableUnit);
        }

        return raClassName;
    }

    /**
     * check whether the provided location is valid
     * @param f location where the .rar is present
     * @throws ConnectorRuntimeException
     */
    private static void validateRARLocation(File f) throws ConnectorRuntimeException {
        if (!f.exists()) {
            String i18nMsg = localStrings.getString(
                    "rar_archive_not_found", f);
            throw new ConnectorRuntimeException(i18nMsg);
        }
    }

    private static String getRAClassNameFromRAR(String rarLocation) {
        //TODO what is the diff between cddTU.getRAClassName and cdesc.getRAClass() ?
        String beanClassName = ConnectorDDTransformUtils.
                getResourceAdapterClassName(rarLocation);
        return beanClassName;
    }


    private static String getRAClassNameFromDirectory(String directoryLocation) throws ConnectorRuntimeException {
        //Use the deployment APIs to get the name of the resourceadapter
        //class through the connector descriptor
        //TODO what is the diff between cddTU.getRAClassName and cdesc.getRAClass() ?
        String raClassName = null;
        try {
            ConnectorDescriptor cd = ConnectorDDTransformUtils.
                    getConnectorDescriptor(directoryLocation);
            raClassName = cd.getResourceAdapterClass();
        } catch (ConnectorRuntimeException e) {
            _logger.info(e.getMessage());
            _logger.log(Level.FINE, "Error while trying to read connector" +
                    "descriptor to get resource-adapter properties", e);
            throw e;
        }
        return raClassName;
    }





    /**
     * Extracts RA Bean properties via reflection.
     *
     * @param raClassName The RA Bean class name.
     * @param ucl         the classloader to use to find the class.
     */
    /*private static Map extractRABeanProps(String raClassName, ClassLoader classLoader)
                                    throws ClassNotFoundException {
        Map hMap = new HashMap();
        //Only if RA is a 1.5 RAR, we need to get RA JavaBean properties, else
        //return an empty map.
        if(raClassName.trim().length() != 0) {
            Class c = classLoader.loadClass(raClassName);
            if(_logger.isLoggable(Level.FINER)) printClassDetails(c);
            hMap = getJavaBeanProperties(c);
        }
        return hMap;
    }*/
    public static void main(String[] args) throws ConnectorRuntimeException{
        if (!(args.length >= 1)) {
            System.out.println("<Usage> java RARUtils directory-path ");
            return;
        }

        Map hMap = RARUtils.getRABeanProperties(args[0]);
        System.out.println("RA JavaBean Properties");
        System.out.println(hMap);
    }

    private static Map getJavaBeanProperties(Class c) {
        Method[] m = c.getMethods();
        Map hMap = new HashMap();
        for (int i = 0; i < m.length; i++) {
            _logger.finer(m[i].getName());
            if (m[i].getName().startsWith("get")
                    && isValidRABeanConfigProperty(m[i].getReturnType())) {
                hMap.put(m[i].getName().substring(3), m[i].getReturnType());
            }
        }

        //remove Object's Class attribute.
        hMap.remove("Class");
        return hMap;
    }

    /**
     * A valid resource adapter java bean property should either be one of the
     * following
     * 1. A Java primitive or a primitve wrapper
     * 2. A String
     */
    public static boolean isValidRABeanConfigProperty(Class clz) {
        return (clz.isPrimitive() || clz.equals(String.class)
                || isPrimitiveWrapper(clz));
    }

    /**
     * Determines if a class is one of the eight java primitive wrapper classes
     */
    private static boolean isPrimitiveWrapper(Class clz) {
        return (clz.equals(Boolean.class) || clz.equals(Character.class)
                || clz.equals(Byte.class) || clz.equals(Short.class)
                || clz.equals(Integer.class) || clz.equals(Long.class)
                || clz.equals(Float.class) || clz.equals(Double.class));
    }


    private static void printClassDetails(Class c) {
        Method[] m = c.getMethods();
        _logger.finer("Methods in " + c.getName());
        for (int i = 0; i < m.length; i++) {
            _logger.finer(m[i].toString());
        }
    }

    /**
     * Prepares the name/value pairs for ActivationSpec. <p>
     * Rule: <p>
     * 1. The name/value pairs are the union of activation-config on
     * standard DD (message-driven) and runtime DD (mdb-resource-adapter)
     * 2. If there are duplicate property settings, the value in runtime
     * activation-config will overwrite the one in the standard
     * activation-config.
     */
    public static Set getMergedActivationConfigProperties(EjbMessageBeanDescriptor msgDesc) {

        Set mergedProps = new HashSet();
        Set runtimePropNames = new HashSet();

        Set runtimeProps = msgDesc.getRuntimeActivationConfigProperties();
        if (runtimeProps != null) {
            Iterator iter = runtimeProps.iterator();
            while (iter.hasNext()) {
                EnvironmentProperty entry = (EnvironmentProperty) iter.next();
                mergedProps.add(entry);
                String propName = (String) entry.getName();
                runtimePropNames.add(propName);
            }
        }

        Set standardProps = msgDesc.getActivationConfigProperties();
        if (standardProps != null) {
            Iterator iter = standardProps.iterator();
            while (iter.hasNext()) {
                EnvironmentProperty entry = (EnvironmentProperty) iter.next();
                String propName = (String) entry.getName();
                if (runtimePropNames.contains(propName))
                    continue;
                mergedProps.add(entry);
            }
        }

        return mergedProps;

    }

}

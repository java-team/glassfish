/*
 * The contents of this file are subject to the terms 
 * of the Common Development and Distribution License 
 * (the License).  You may not use this file except in
 * compliance with the License.
 * 
 * You can obtain a copy of the license at 
 * https://glassfish.dev.java.net/public/CDDLv1.0.html or
 * glassfish/bootstrap/legal/CDDLv1.0.txt.
 * See the License for the specific language governing 
 * permissions and limitations under the License.
 * 
 * When distributing Covered Code, include this CDDL 
 * Header Notice in each file and include the License file 
 * at glassfish/bootstrap/legal/CDDLv1.0.txt.  
 * If applicable, add the following below the CDDL Header, 
 * with the fields enclosed by brackets [] replaced by
 * you own identifying information: 
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Copyright 2006 Sun Microsystems, Inc. All rights reserved.
 */
package com.sun.enterprise.transaction;

import javax.transaction.SystemException;
import javax.transaction.xa.Xid;
import javax.resource.spi.XATerminator;

/**
 * Transaction Manager extensions to support transaction inflow w/o resource adapter.
 */
public interface TransactionImport {
  /**
     * Recreate a transaction based on the Xid. This call causes the calling
     * thread to be associated with the specified transaction. 
     * 
     * <p>
     * This method imports a transactional context controlled by an external transaction manager.
     *
     * @param xid the Xid object representing a transaction.
     */
    public void recreate(Xid xid, long timeout); 

    /**
     * Release a transaction. This call causes the calling thread to be
     * dissociated from the specified transaction. 
     * 
     * <p>
     * This call releases transactional context imported by recreate method.
     *
     * @param xid the Xid object representing a transaction.
     */
    public void release(Xid xid); 

    /**
     * Provides a handle to a <code>XATerminator</code> instance.
     *
     * <p> The XATerminator exports 2PC protocol control to an external root transaction coordinator.
     *
     * @return a <code>XATerminator</code> instance.
     */
    public XATerminator getXATerminator();
    
    /**
     * Return duration before current transaction would timeout.
     *
     * @return Returns the duration in seconds before current transaction would
     *         timeout.
     *         Returns zero if transaction has no timeout set and returns 
     *         negative value if transaction already timed out.
     *
     * @exception IllegalStateException Thrown if the current thread is
     *    not associated with a transaction.
     *
     * @exception SystemException Thrown if the transaction manager
     *    encounters an unexpected error condition.
     */
    public int getTransactionRemainingTimeout() throws SystemException;
}

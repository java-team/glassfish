<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
         xmlns:sch="http://www.ascc.net/xml/schematron"
         xmlns="http://relaxng.org/ns/structure/1.0"
         xmlns:x="http://www.w3.org/1999/xhtml"
         datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">

  <define name="log-service">
    <element name="log-service">
      <x:div class='dtd'>
        By default, logs would be kept in $INSTANCE-ROOT/logs. The
        following log files will be stored under the logs directory.
        <x:dl>
          <x:dt>access.log</x:dt>
          <x:dd>keeps default virtual server HTTP access messages.</x:dd>
          <x:dt>server.log</x:dt>
          <x:dd>keeps log messages from default virtual server.
          Messages from other configured virtual servers also go here,
          unless log-file is explicitly specified in the virtual-server
          element.</x:dd>
        </x:dl>
      </x:div>
      <ref name="log-service-attlist"/>
      <optional>
        <ref name="module-log-levels"/>
      </optional>
      <zeroOrMore>
        <ref name="property"/>
      </zeroOrMore>
    </element>
  </define>
  <define name="log-service-attlist" combine="interleave">
    <optional>
      <attribute name="file">
        <x:div class='dtd'>
          can be used to rename or relocate server.log using absolute
          path.
        </x:div>
	<ref name="file-type"/>
      </attribute>
    </optional>
    <optional>
      <attribute name="use-system-logging" a:defaultValue="false">
        <x:div class='dtd'>
          if true, will utilize Unix syslog service or Windows Event
          Logging to produce and manage logs.
        </x:div>
        <ref name="boolean"/>
      </attribute>
    </optional>
    <optional>
      <attribute name="log-handler">
        <x:div class='dtd'>
          Can plug in a custom log handler to add it to the chain of
          handlers to log into a different log destination than the
          default ones given by the system (which are Console, File
          and Syslog). It is a requirement that customers use the log
          formatter provided by the the system to maintain uniformity
          in log messages. The custom log handler will be added at the
          end of the handler chain after File + Syslog Handler,
          Console Handler and JMX Handler. User cannot replace the
          handler provided by the system, because of loosing precious
          log statements. The Server Initialization will take care of
          installing the custom handler with the system formatter
          initialized. The user need to use JSR 047 Log Handler
          Interface to implement the custom handler.
        </x:div>
      </attribute>
    </optional>
    <optional>
      <attribute name="log-filter">
        <x:div class='dtd'>
          Can plug in a log filter to do custom filtering of log
          records .  By default there is no log filter other than the
          log level filtering provided by JSR 047 log API.
        </x:div>
      </attribute>
    </optional>
    <optional>
      <attribute name="log-to-console" a:defaultValue="false">
        <x:div class='dtd'>
          logs will be sent to stderr when asadmin start-domain
          verbose is used
        </x:div>
        <ref name="boolean"/>
      </attribute>
    </optional>
    <optional>
      <attribute name="log-rotation-limit-in-bytes" a:defaultValue="500000">
        <x:div class='dtd'>
          Log Files will be rotated when the file size reaches the
          limit.
        </x:div>
        <data type="positiveInteger"/>
      </attribute>
    </optional>
    <optional>
      <attribute name="log-rotation-timelimit-in-minutes" a:defaultValue="0">
        <x:div class='dtd'>
          This is a new attribute to enable time based log
          rotation. The Log File will be rotated only if this value is
          non-zero and the valid range is 60 minutes (1 hour) to
          10*24*60 minutes (10 days).  If the value is zero then the
          files will be rotated based on size specified in
          log-rotation-limit-in-bytes.
        </x:div>
	<data type="nonNegativeInteger">
          <param name="maxInclusive">14400</param>
        </data>	
      </attribute>
    </optional>
    <optional>
      <attribute name="alarms" a:defaultValue="false">
        <x:div class='dtd'>
          if true, will turn on alarms for the logger. The SEVERE and
          WARNING messages can be routed through the JMX framework to
          raise SEVERE and WARNING alerts. Alarms are turned off by
          default.
        </x:div>
       <ref name="boolean"/>
      </attribute>
    </optional>
    <optional>
      <attribute name="retain-error-statistics-for-hours" a:defaultValue="5">
        <x:div class='dtd'>
          The number of hours since server start, for which error 
          statistics should be retained in memory. The default and 
          minimum value is 5 hours. The maximum value allowed is 
          500 hours. Note that larger values will incur additional 
          memory overhead.
        </x:div>
        <data type="positiveInteger">
          <param name="minInclusive">5</param>
          <param name="maxInclusive">500</param>
        </data>	
      </attribute>
    </optional>
  </define>
  
</grammar>

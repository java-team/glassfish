/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
 
package com.sun.enterprise.admin.pluggable;

import java.util.HashMap;
import java.util.Map;

/**
 * Base interface implemented by external modules.
 * All the extended AMX Support Features can be added here.
 * This interface is <b>not</b> for public consumption. 
 *
 * @author: Yamini K B
 */

public interface AMXSupportExtensionFeature {

    /**
     * Gets all external (new) AMX interfaces.
     * This will be consumed by TypesMapper during AMX initialization
     * to provide a mapping for the new XTypes 
     *
     * @return Class[] Containing the AMX interfaces for example 
     *                 NewElement1Config.class, NewElement2Config.class 
     *                 and so on..
     */ 
    public Class[] getExternalMbeanInterfaces();

    /**
     * Gets all the new extended types of type TypeData
     * TypeInfos will add these new types into the existing heirarchy.
     *
     * We are returning an Object array here though it will actually contain
     * TypeData objects only because of build dependency problem. TypeData
     * is withing admin/mbeanapi-impl and this class is going to compile
     * first.
     *
     * @return Object[] Containing the new config types pertaining
     *                  to the new configuration elements in domain.xml
     */
    public Object[] getNewTypeData();

    /**
     * Gets the new AMX type and old config type mappings.
     *
     * @return Map Containing the (new type, config) mapping.
     */
    public Map<String,String> getOldConfigTypes();

    /**
     * Gets the interface implementation classes.
     * This will be used to look up the implementation class
     * for a particular interface. 
     *
     * @return Map Containing the interface implementations.
     *             That is map the new interface to its 
     *             implementation class
     */
    public Map<String,String> getInterfaceImpls();

    /**
     * Gets the new AMX type and old monitor type mappings.
     *
     * @return Map Containing the (new type, monitor) mapping.
     *               
     */
    public Map<String,String> getOldMonitorTypes();

    /**
     * Get all ignorable mbean types
     *
     * @return String[] containing the mbean types that need to be ignored
     */
    public String[] getIgnoreTypes();
}

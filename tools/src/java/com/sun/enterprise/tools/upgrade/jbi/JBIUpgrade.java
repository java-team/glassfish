/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package com.sun.enterprise.tools.upgrade.jbi;

import java.util.logging.*;

import com.sun.enterprise.tools.upgrade.common.*;
import com.sun.enterprise.util.i18n.StringManager;
import com.sun.enterprise.tools.upgrade.logging.*;

import com.sun.jbi.upgrade.JBIUpgradeToolFactory;
import com.sun.jbi.upgrade.JBIUpgradeTool;
import com.sun.jbi.upgrade.JBIUpgradeException;

/**
 *  This class calls a class provided by the JCAPS team that updates
 *  their JBI config files.
 * 
 *  A copy of their jar file is kept locally in this module for compiling
 *  purposes only.
 * 
 * @author rebeccas
 */
public class JBIUpgrade implements com.sun.enterprise.tools.upgrade.common.BaseModule {
    
    private StringManager stringManager = StringManager.getManager(
            "com.sun.enterprise.tools.upgrade.jbi");
    private Logger logger = CommonInfoModel.getDefaultLogger();
    
    public JBIUpgrade(){
    }
    
    public boolean upgrade(CommonInfoModel commonInfo){
        boolean flag = false;
        
        //- Only process 9.1X source versions are supported.
        if (UpgradeConstants.isPreAS91Version(commonInfo.getSourceVersion())) {
            flag = true;
        } else {
            //- verify the needed classes are available
            try {
                Class cls = Class.forName("com.sun.jbi.upgrade.JBIUpgradeToolFactory");
            } catch (ClassNotFoundException ce) {
                logger.log(Level.SEVERE, stringManager.getString("upgrade.jbi.classNotFoundMessage", ce.getMessage()), ce);
                return false;
            }

            String sourceDomainPath = commonInfo.getSourceDomainPath();
            String targetDomainPath = commonInfo.getDestinationDomainPath();
            JBIUpgradeTool jbiTool = JBIUpgradeToolFactory.getJBIUpgradeTool(logger);

            try {
                logger.log(Level.INFO, stringManager.getString("upgrade.jbi.startMessage"));
                jbiTool.migrateJBIArtifacts(sourceDomainPath, targetDomainPath);
                logger.log(Level.INFO, stringManager.getString("upgrade.jbi.finishMessage"));
                flag = true;
            } catch (Exception e) {
                logger.log(Level.SEVERE, stringManager.getString("upgrade.jbi.jbiExceptionMessage", e.getMessage()), e);
            }
        }
        return flag;
    }
    
    public String getName(){
        return stringManager.getString("upgrade.jbi.moduleName");
    }
    
    public void recovery(CommonInfoModel commonInfo){}
}

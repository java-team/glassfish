/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.appserv.ha.uow;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sun.common.util.logging.LogDomains;

/**
 * This class represents a collection of entities (Unit-of-work, or UOW for
 * short) to be replicated.
 *
 * @author epetstr
 */
public class ReplicationUnitOfWork {

    private static final Logger logger =
        LogDomains.getLogger(LogDomains.CMN_LOGGER);

    protected static final ThreadLocal<ReplicationUnitOfWork> threadLocalReplicationUnits =
        new ThreadLocal<ReplicationUnitOfWork>();

    protected LinkedHashSet<ReplicableEntity> workSet;

    /**
     * Creates an empty UOW and stores it as a threadlocal.
     */
    public ReplicationUnitOfWork() {
        // Assign ourselves as the current UOW for this thread
        this(true);
    }

    /**
     * Creates an empty UOW.
     *
     * @param setToThread true if the new UOW is to be stored as a threadlocal,
     * false otherwise
     */
    public ReplicationUnitOfWork(boolean setToThread) {
        if (setToThread) {
            threadLocalReplicationUnits.set(this);
        }
    }

    public static ReplicationUnitOfWork getThreadLocalUnitOfWork() {
        return threadLocalReplicationUnits.get();
    }

    /**
     * Clears this UOW from the threadlocal.
     */
    public static void clearThreadLocal() {
        ReplicationUnitOfWork tl = threadLocalReplicationUnits.get();
        threadLocalReplicationUnits.set(null);
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Cleared thread local; was: " + tl);
        }
    }

    /**
     * Stores this UOW as a threadlocal.
     */
    public void setThreadLocal() {
        threadLocalReplicationUnits.set(this);
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Has set thread local: " + this);
        }
    }

    /**
     * Adds the specified entity to this UOW.
     *
     * @param entity the entity to be added
     */
    public synchronized void add(ReplicableEntity entity) {
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "ENTER: " + this + ", entity: " + entity);
        }

        if (workSet == null) {
            workSet = new LinkedHashSet<ReplicableEntity>();
        }
        if (workSet.add(entity) == true) {
            if (logger.isLoggable(Level.FINER)) {
                logger.log(Level.FINER, "Added modified entity " + entity);
            }
            entity.lockForegroundWithRetry();
            if (logger.isLoggable(Level.FINEST)) {
                logger.log(Level.FINEST, "FG locked modified entity " +
                           entity);
            }
        }

        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "EXIT: " + this);
        }
    }

    /**
     * Removes the given entity from this UOW, if present.
     *
     * @param entity the entity to be removed
     */
    public synchronized void remove(ReplicableEntity entity) {
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "ENTER: " + this + ", entity: " + entity);
        }
        
        if (workSet != null && workSet.remove(entity)) {
            if (logger.isLoggable(Level.FINER)) {
                logger.log(Level.FINER, "Removed modified entity " + entity);
            }

            entity.unlockForeground();
            if (logger.isLoggable(Level.FINEST)) {
                logger.log(Level.FINEST, "Unlocked modified entity " + entity);
            }
        } else if (workSet == null) {
            if (logger.isLoggable(Level.FINEST)) {
                logger.log(Level.FINEST,
                           "Tried to remove, but workSet was null");
            }
        }
        
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "EXIT: "+ this);
        }
    }

    /**
     * Saves the entities in this UOW.
     */
    public void save() {
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "ENTER: "+ this);
        }
        
        Collection<ReplicableEntity> localWorkSet = null;
        synchronized (this) {
            // Make sets local to shorten lock duration
            // and avoid possibility for deadlocks
            if (workSet != null) {
                localWorkSet = workSet;
                workSet = null;
            }            
        }

        if (localWorkSet != null) {
            for (ReplicableEntity entity : localWorkSet) {
                entity.save();
                entity.unlockForeground();
                if (logger.isLoggable(Level.FINER)) {
                    logger.log(Level.FINER, "Saved and unlocked entity: " + entity);
                }
            }
        }
        
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "EXIT: "+ this);
        }
    }

    @Override
    public String toString() {
        boolean isThisTheThreadLocal =
            threadLocalReplicationUnits.get() == this;
        return super.toString() + "{workset=" + workSet +
            "; this == threadLocal:" + isThisTheThreadLocal + "}";
    }
}

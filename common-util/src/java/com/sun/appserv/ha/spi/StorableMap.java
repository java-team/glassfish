package com.sun.appserv.ha.spi;

import java.util.Collection;

/**
 * C@author Mahesh Kannan
 *
 */
public interface StorableMap<K, V> {

    public Collection<K> getNewKeys();

    public Collection<K> getModifiedKeys();

    public Collection<K> getDeletedKeys();

    public V get(K k);
    
}

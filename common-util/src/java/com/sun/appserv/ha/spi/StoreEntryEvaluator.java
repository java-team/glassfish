package com.sun.appserv.ha.spi;

import java.io.Serializable;

public interface StoreEntryEvaluator<K, V>
    extends Serializable {

    //TODO: Need to go through ASARCH
    public Object eval(K key, V value);
    
}

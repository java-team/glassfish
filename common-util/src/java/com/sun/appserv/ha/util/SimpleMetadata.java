/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.appserv.ha.util;

import org.glassfish.enterprise.ha.store.spi.Storable;

import java.util.HashSet;
import java.util.Set;

/**
 * A class for storing meta information
 */
public class SimpleMetadata<E>
    extends Metadata<E>
    implements Storable {

    public transient static final int SAVE_ALL = 0;

    public transient static final int SAVE_EP = 1;

    public transient static final int SAVE_TIME_STAMP = 2;

    public transient int saveMode = SAVE_ALL;

    private transient Set<String> _dirtyAttributeNames = new HashSet<String>();

    private static Set<String> saveALL = new HashSet<String>();
    private static Set<String> saveEP = new HashSet<String>();

    static {
        saveALL.add(ReplicationAttributeNames.STATE);
        saveALL.add(ReplicationAttributeNames.EXTRA_PARAM);
        saveEP.add(ReplicationAttributeNames.EXTRA_PARAM);
    }
    /**
     * Convenience method for creating a SimpleMetadata
     *  specifically for updateTimeStamp()
     *
     * @param version
     * @param lastAccessTime
     */
    SimpleMetadata(long version, long lastAccessTime) {
        super(version, lastAccessTime);
        saveMode = SAVE_TIME_STAMP;
    }

    SimpleMetadata(long version, long lastAccesstime, E e) {
        super(version, lastAccesstime, e);
        saveMode = SAVE_EP;
    }


    SimpleMetadata(long version, long lastAccessTime,
                   long maxInactiveInterval, byte[] state) {
        super(version, lastAccessTime, maxInactiveInterval, state);
        saveMode = SAVE_ALL;
        _dirtyAttributeNames = saveALL;
    }

    SimpleMetadata(long version, long lastAccessTime,
                   long maxInactiveInterval, byte[] state, E e) {
        super(version, lastAccessTime, maxInactiveInterval, state, e);
        saveMode = SAVE_ALL;
        _dirtyAttributeNames = saveALL;
    }

    public Object _getAttributeValue(String attrName) {
        Object result = null;
        if (ReplicationAttributeNames.EXTRA_PARAM.equals(attrName)) {
            result = getExtraParam();
        } else if (ReplicationAttributeNames.STATE.equals(attrName)) {
            result = getState();
        }

        return result;
    }

    public Set<String> _getDirtyAttributeNames() {
        return _dirtyAttributeNames;
    }

    public String _getOwnerInstanceName() {
        return getOwningInstanceName();
    }

    public int getSaveMode() {
        return saveMode;
    }
}

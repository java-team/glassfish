/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.appserv.ha.util;

import org.glassfish.enterprise.ha.store.spi.Storable;

import java.util.Map;
import java.util.HashMap;

/**
 * A class for storing meta information
 */
public abstract class Metadata<E>
    implements Storable {

    private String beKey;

    private String owningInstanceName;

    private long version;

    private long lastAccessTime;

    private long maxInactiveInterval;

    private byte[] state;

    private E extraParam;

    protected Metadata(long version, long lastAccessTime) {
        this.version = version;
        this.lastAccessTime = lastAccessTime;
    }

    protected Metadata(long version, long lastAccesstime, E e) {
        this.version = version;
        this.lastAccessTime = lastAccesstime;
        this.extraParam = e;
    }

    protected Metadata(long version, long lastAccessTime, long maxInactiveInterval) {
        this(version, lastAccessTime, maxInactiveInterval, (byte[]) null, (E) null);
    }

    protected Metadata(long version, long lastAccessTime, long maxInactiveInterval, byte[] state) {
        this(version, lastAccessTime, maxInactiveInterval, state, (E) null);
    }

    /**
     * Construct a Metadata object
     *
     * @param version
     *            The version of the data. A freshly created state has a version ==
     *            0
     * @param lastAccesstime
     *            the last access time of the state. This must be used in
     *            conjunction with getMaxInactiveInterval to determine if the
     *            state is idle enough to be removed.
     * @param maxInactiveInterval
     *            the maximum time that this state can be idle in the store
     *            before it can be removed.
     */
    protected Metadata(long version, long lastAccesstime, long maxInactiveInterval, byte[] state, E e) {
        this.version = version;
        this.lastAccessTime = lastAccesstime;
        this.maxInactiveInterval = maxInactiveInterval;
        this.state = state;
        this.extraParam = e;
    }

    /**
     * Get the verion of the state. A freshly created state has a version == 0
     * 
     * @return the version.
     */
    public long getVersion() {
        return version;
    }

    /**
     * Get the last access time of the state. This must be used in conjunction
     * with getMaxInactiveInterval to determine if the state is idle enough to
     * be removed.
     * 
     * @return The time when the state was accessed last
     * @see {getMaxInactiveInterval}
     * @see {BackingStore.removeExpired}
     */
    public long getLastAccessTime() {
        return lastAccessTime;
    }

    /**
     * Get the maximum time that this state can be idle in the store before it
     * can be removed.
     * 
     * @return the maximum idle time. If zero or negative, then the component
     *         has no idle timeout limit
     */
    public long getMaxInactiveInterval() {
        return this.maxInactiveInterval;
    }

    public byte[] getState() {
        return state;
    }

    public void setState(byte[] state) {
        this.state = state;
    }

    public E getExtraParam() {
        return extraParam;
    }

    public void setExtraParam(E extraParam) {
        this.extraParam = extraParam;
    }

    public String getBeKey() {
        return beKey;
    }

    public String _getHashKey() {   //Storable method
        return beKey;
    }

    public void setBeKey(String beKey) {
        this.beKey = beKey;
    }

    public void _setBeKey(String beKey) {
        this.beKey = beKey;
    }

    public String getOwningInstanceName() {
        return owningInstanceName;
    }

    public String _getOwningInstanceName() { //Storable method
        return getOwningInstanceName();
    }

    public void setOwningInstanceName(String owningInstanceName) {
        this.owningInstanceName = owningInstanceName;
    }

    public void _setOwningInstanceName(String owningInstanceName) {
        this.owningInstanceName = owningInstanceName;
    }

    public void setLastAccessTime(long lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public void setMaxInactiveInterval(long maxInactiveInterval) {
        this.maxInactiveInterval = maxInactiveInterval;
    }

    public void _setMaxInactiveInterval(long maxInactiveInterval) {
        this.maxInactiveInterval = maxInactiveInterval;
    }
    
    public long _getVersion() {
        return getVersion();
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public void _setVersion(long version) {
        this.version = version;
    }
    

}

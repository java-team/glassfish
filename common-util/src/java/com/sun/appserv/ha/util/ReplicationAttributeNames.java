package com.sun.appserv.ha.util;


public interface ReplicationAttributeNames {

    public static final String EXTRA_PARAM = "extraParam";

    public static final String STATE = "state";

    public static final String VERSION = "version";

    public static final String BE_KEY = "beKey";

    public static final String OWNING_INSTANCE_NAME = "ownerId";

    public static final String MAX_INACTIVE_INTERVAL = "maxInactiveInterval";

    public static final String LAST_ACCESS_TIME = "lastAccessTime";

    public static final String STRING_EXTRA_PARAM = "stringExtraParam";

    public static final String SESSION_ATTRIBUTES = "sessionAttributes";

}

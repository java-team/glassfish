package org.glassfish.enterprise.ha;

import org.glassfish.enterprise.ha.store.annotations.Attribute;
import org.glassfish.enterprise.ha.store.annotations.StoreEntry;
import org.glassfish.enterprise.ha.store.annotations.Version;
import org.glassfish.enterprise.ha.store.annotations.HashKey;

import java.io.Serializable;

/**
 * An object that holds some state to be persisted in a HA BackingStore.
 *
 * @author Mahesh Kannan
 */
@StoreEntry
public class SimpleSessionState<E extends Serializable> {

    private long version;

    private String beKey;

    private long lastAccessTime;

    private long maxInactiveInterval;

    private byte[] state;

    private E extraParam;

    private String ownerInstanceName;

    /**
     * Get the verion of the state. A freshly created state has a version == 0
     *
     * @return the version.
     */
    @Version(name = "version")
    public long getVersion() {
        return version;
    }

    public void setVersion(long value) {
        this.version = value;
    }

    @HashKey(name="beKey")
    public String getBeKey() {
        return beKey;
    }

    public void setBeKey(String beKey) {
        this.beKey = beKey;
    }

    /**
     * Get the last access time of the state. This must be used in conjunction
     * with getMaxInactiveInterval to determine if the state is idle enough to
     * be removed.
     *
     * @return The time when the state was accessed last
     * @see {getMaxInactiveInterval}
     * @see {BackingStore.removeExpired}
     */
    @Attribute(name = "lastAccessTime")
    public long getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(long value) {
        this.lastAccessTime = value;
    }

    /**
     * Get the maximum time that this state can be idle in the store before it
     * can be removed.
     *
     * @return the maximum idle time. If zero or negative, then the component
     *         has no idle timeout limit
     */
    @Attribute(name = "maxInactiveInterval")
    public long getMaxInactiveInterval() {
        return this.maxInactiveInterval;
    }

    public void setMaxInactiveInterval(long value) {
        this.maxInactiveInterval = value;
    }

    @Attribute(name="state")
    public byte[] getState() {
        return state;
    }

    public void setState(byte[] state) {
        this.state = state;
    }

    @Attribute(name="extraParam")
    public E getExtraParam() {
        return extraParam;
    }

    public void setExtraParam(E extraParam) {
        this.extraParam = extraParam;
    }

    @Attribute(name="ownerInstanceName")
    public String getOwnerInstanceName() {
        return ownerInstanceName;
    }

    public void setOwnerInstanceName(String ownerInstanceName) {
        this.ownerInstanceName = ownerInstanceName;
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.enterprise.ha.store.util;

import org.glassfish.enterprise.ha.store.spi.AttributeMetadata;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * @author bhavanishankar@dev.java.net
 */
public class StoreEntryMetadataHelper<S> {

    private Class<S> clazz;
    
    private LinkedList<AttributeMetadataImpl> attributeMetadata =
            new LinkedList<AttributeMetadataImpl>();

    private HashMap<Method, AttributeMetadataImpl> attrSetterMethodMap =
            new HashMap<Method, AttributeMetadataImpl>();

    private HashMap<String, AttributeMetadataImpl> attrNameMethodMap =
            new HashMap<String, AttributeMetadataImpl>();

    private int serializedDataIndex = -1;

    StoreEntryMetadataHelper(Class<S> clazz) {
        this.clazz = clazz;
    }

    public int getSerializedDataIndex() {
        return serializedDataIndex;
    }

    public AttributeMetadata[] getAllAttributeMetadata() {
        return attributeMetadata.toArray(new AttributeMetadata[0]);
    }

    public AttributeMetadata getAttributMetadata(int index) {
        return attributeMetadata.get(index);
    }

    // TODO :: I added the following method(s), please check.
    public AttributeMetadata getAttributMetadata(String attrName) {
        return attrNameMethodMap.get(attrName);
    }

    public int getAttributeCount() {
        return attributeMetadata.size();
    }

    public int getAttributeIndex(Method setterMethod) {
        AttributeMetadataImpl attrMetadata = attrSetterMethodMap.get(setterMethod);
        return attrMetadata == null ? -1 : attrMetadata.getAttributeIndex();
    }

    public int getAttributeIndex(String attrName) {
        AttributeMetadataImpl attrMetadata = attrNameMethodMap.get(attrName);
        return attrMetadata == null ? -1 : attrMetadata.getAttributeIndex();
    }

    public AttributeMetadataImpl getAttributeMetaData(int index) {
        if (index < 0 || index >= attributeMetadata.size()) {
            return null;
        }
        return attributeMetadata.get(index);
    }

    public String getName() {
        return clazz != null ? clazz.getName() : null;
    }

    public Class getStoreEntryClass() {
        return clazz;
    }

    public void addAttributeMetadata(AttributeMetadataImpl metaData) {
        int index = attributeMetadata.size();
        metaData.setIndex(index);
        attributeMetadata.add(index, metaData);
        if ("SerializedData".equalsIgnoreCase(metaData.getName())) {
            serializedDataIndex = index;
        }
        attrSetterMethodMap.put(metaData.getSetterMethod(), metaData);
        attrNameMethodMap.put(metaData.getName(), metaData);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
//        sb.append("\n" + getClass().getName() + " :: ");
        sb.append("\nStoreEntryMetadata for " + getName() + " = ");
        sb.append("\n" + "Attributes = ");
        for (AttributeMetadata attrMetadata : getAllAttributeMetadata()) {
            sb.append("\n\t" + attrMetadata);
        }
        
        return sb.toString();
    }

}

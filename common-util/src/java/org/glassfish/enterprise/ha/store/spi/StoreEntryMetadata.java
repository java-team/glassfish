package org.glassfish.enterprise.ha.store.spi;

import java.util.Collection;


public interface StoreEntryMetadata<S> {

    public AttributeMetadata<S, ?> getAttributeMetadata(String name);

    public Collection<AttributeMetadata<S, ?>> getAllAttributeMetadata();
    
}

package org.glassfish.enterprise.ha.store.criteria.spi;

import org.glassfish.enterprise.ha.store.criteria.Expression;

/**
 * An abstract class that represents some operation.
 *
 * @param <T> The type of this expression
 *
 * @author Mahesh.Kannan@Sun.Com
 */
public abstract class ExpressionNode<T>
    implements Expression<T> {

    private Opcode opcode;

    protected Class<T> returnType;

    public ExpressionNode(Opcode opcode, Class<T> returnType) {
        this.opcode = opcode;
        this.returnType = returnType;
    }

    public Opcode getOpcode() {
        return opcode;
    }

    public Class<T> getReturnType() {
        return returnType;
    }
    
}

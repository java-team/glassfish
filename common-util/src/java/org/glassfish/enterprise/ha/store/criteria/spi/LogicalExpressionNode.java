package org.glassfish.enterprise.ha.store.criteria.spi;

import java.util.Collection;

/**
 * An ExpressionNode that denotes a logical operation. The type of the
 *  expression is same as the Attribute's type itself.
 *
 *
 * @author Mahesh.Kannan@Sun.Com
 */
public class LogicalExpressionNode
    extends BinaryExpressionNode<Boolean> {

    Collection entries;

    public LogicalExpressionNode(Opcode opcode, ExpressionNode left, ExpressionNode right) {
        super(opcode, Boolean.class, left, right);
    }

    public LogicalExpressionNode and(LogicalExpressionNode expr) {
        return new LogicalExpressionNode(Opcode.AND, this, expr);
    }

    public LogicalExpressionNode or(LogicalExpressionNode expr) {
        return new LogicalExpressionNode(Opcode.OR, this, expr);
    }

    public LogicalExpressionNode isTrue() {
        return new LogicalExpressionNode(Opcode.EQ, this, new LiteralNode(Boolean.class, true));
    }

    public LogicalExpressionNode eq(boolean value) {
        return new LogicalExpressionNode(Opcode.EQ, this, new LiteralNode(Boolean.class, value));
    }

    public LogicalExpressionNode isNotTrue() {
        return new LogicalExpressionNode(Opcode.EQ, this, new LiteralNode(Boolean.class, true));
    }

    public LogicalExpressionNode neq(boolean value) {
        return new LogicalExpressionNode(Opcode.NEQ, this, new LiteralNode(Boolean.class, value));
    }

    public Class<Boolean> getReturnType() {
        return Boolean.class;
    }

}
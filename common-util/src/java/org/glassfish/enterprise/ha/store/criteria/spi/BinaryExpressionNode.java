package org.glassfish.enterprise.ha.store.criteria.spi;

import org.glassfish.enterprise.ha.store.criteria.spi.Opcode;

/**
 * An abstract class that represents a binary operation.
 *
 * @param <T> The type of the expression
 *
 * @author Mahesh.Kannan@Sun.Com
 */
public class BinaryExpressionNode<T>
    extends ExpressionNode<T> {

    private ExpressionNode<T> left;

    private ExpressionNode<T> right;

    public BinaryExpressionNode(Opcode opcode, Class<T> returnType, ExpressionNode<T> left) {
        this(opcode, returnType, left, null);
    }

    public BinaryExpressionNode(Opcode opcode, Class<T> returnType,
                                ExpressionNode<T> left, ExpressionNode<T> right) {
        super(opcode, returnType);
        this.left= left;
        this.right = right;
    }

    public ExpressionNode<T> getLeft() {
        return left;
    }

    public ExpressionNode<T> getRight() {
        return right;
    }

}
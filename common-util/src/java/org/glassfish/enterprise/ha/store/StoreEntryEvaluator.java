package org.glassfish.enterprise.ha.store;

import java.io.Serializable;

public interface StoreEntryEvaluator<K, V>
    extends Serializable {

    public boolean eval(K key, V value);
    
}

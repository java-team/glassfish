package org.glassfish.enterprise.ha.store;

import org.glassfish.enterprise.ha.store.spi.AttributeMetadata;
import org.glassfish.enterprise.ha.store.util.AttributeMetadataImpl;

import javax.annotation.Generated;

@Generated("EjbState")
public class EjbState_Metadata_ {

   public static AttributeMetadata<EjbState, String> beanName;

   public static AttributeMetadata<EjbState, Long> containerId;

   public static AttributeMetadata<EjbState, Long> maxIdleTime;

   public static AttributeMetadata<EjbState, Integer> version;

   public static AttributeMetadata<EjbState, Short> contextState;

   public static AttributeMetadata<EjbState, byte[]> state;

   private static AttributeMetadata<EjbState, ?>[] _attrs;

   static {

       beanName =
               new AttributeMetadataImpl<EjbState, String>(0, "beanName", EjbState.class, String.class);
       containerId =
          new AttributeMetadataImpl<EjbState, Long>(1, "containerId", EjbState.class, Long.class);
       maxIdleTime =
           new AttributeMetadataImpl<EjbState, Long> (2, "maxIdleTime", EjbState.class, long.class);
       version =
           new AttributeMetadataImpl<EjbState, Integer> (3, "version", EjbState.class, int.class);
       contextState =
           new AttributeMetadataImpl<EjbState, Short> (4, "contextState", EjbState.class, short.class);
       state =
           new AttributeMetadataImpl<EjbState, byte[]> (5, "state", EjbState.class, byte[].class);


       ((AttributeMetadataImpl) version).markAsVersionAttribute();
       _attrs = new AttributeMetadata[] {
           beanName, containerId, maxIdleTime, version, contextState, state
       };
   }

   public static AttributeMetadata<EjbState, ?> getAttributeMetadata(String attrName)
       throws NoSuchFieldException, IllegalAccessException {
       Class<EjbState_Metadata_> clz = EjbState_Metadata_.class;
       return (AttributeMetadata<EjbState, ?>) clz.getField(attrName).get(null);
   }

   public static AttributeMetadata<EjbState, ?> getAttributeMetadata(int index) {
       return _attrs[index];
   }

}
package org.glassfish.enterprise.ha.store.criteria.spi;

/**
 * A node that represnts a Literal
 * @param <T> The type of the Literal
 *
 * @author Mahesh.Kannan@Sun.Com
 */
public class LiteralNode<T>
    extends ExpressionNode<T> {

    private T value;

    public LiteralNode(Class<T> clazz, T t) {
        super(Opcode.LITERAL, clazz);
        this.value = t;
    }

    public T getValue() {
        return value;
    }
}

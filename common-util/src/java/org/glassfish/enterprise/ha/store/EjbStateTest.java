package org.glassfish.enterprise.ha.store;

import org.glassfish.enterprise.ha.store.criteria.ExpressionBuilder;
import org.glassfish.enterprise.ha.store.util.StoreEntryMetadataCache;

import java.util.List;
import java.util.Arrays;


public class EjbStateTest {

    public static void main(String[] args)
            throws Exception {
        StoreEntryMetadataCache.getMetadata("app1", EjbState.class);
    }
    
    public void test1() {
        EjbState_Metadata_ es_;

        List<String> entries = Arrays.asList(new String[] {"A", "B", "C", "D", "E", "F", "G"});
        List<Long> longEntries = Arrays.asList(new Long[] {0L, 1L, 2L, 3L, 4L, 5L, 6L});

        ExpressionBuilder<EjbState> cb = new ExpressionBuilder<EjbState>(EjbState.class);

        cb.eq(EjbState_Metadata_.beanName, "abc")
                .and(cb.eq(EjbState_Metadata_.beanName, "abc"));

        cb.eq(EjbState_Metadata_.containerId, 6L)
                .and(cb.attr(EjbState_Metadata_.containerId).in(longEntries));

        cb.attr(EjbState_Metadata_.beanName).in(entries);

        //cb.attr(EjbState_Metadata_.beanName).in(byteEntries); Won't work!!
    }
}

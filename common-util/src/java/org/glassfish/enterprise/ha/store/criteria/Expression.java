package org.glassfish.enterprise.ha.store.criteria;

public interface Expression<T> {

    public Class<T> getReturnType();
    
    
}

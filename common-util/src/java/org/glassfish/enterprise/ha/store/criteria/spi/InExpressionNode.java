package org.glassfish.enterprise.ha.store.criteria.spi;

import java.util.Collection;

/**
 * A node that represnt the "in" operation
 *
 * @param <T>  The type of operands involed
 *
 * @author Mahesh.Kannan@Sun.Com
 */
public class InExpressionNode<T>
    extends LogicalExpressionNode {

    Collection<? extends T> entries;

    public InExpressionNode(ExpressionNode<T> value, Collection<? extends T> entries) {
        super(Opcode.IN, value, null);
        this.entries = entries;
    }

    public Collection<? extends T> getEntries() {
        return entries;
    }

}
package org.glassfish.enterprise.ha.store;

import org.glassfish.enterprise.ha.store.annotations.Attribute;
import org.glassfish.enterprise.ha.store.annotations.StoreEntry;

@StoreEntry
public class EjbState {

    private String beanName;

    private long containerId;

    private long maxIdleTime;

    private int version;

    private short contextState;

    private byte[] state;

    public String getBeanName() {
        return beanName;
    }

    @Attribute
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public long getContainerId() {
        return containerId;
    }

    @Attribute
    public void setContainerId(long containerId) {
        this.containerId = containerId;
    }

    public long getMaxIdleTime() {
        return maxIdleTime;
    }

    @Attribute
    public void setMaxIdleTime(long maxIdleTime) {
        this.maxIdleTime = maxIdleTime;
    }

    public byte[] getState() {
        return state;
    }

    @Attribute
    public void setState(byte[] state) {
        this.state = state;
    }

    public int getVersion() {
        return version;
    }

    @Attribute
    public void setVersion(int version) {
        this.version = version;
    }

    public short getContextState() {
        return contextState;
    }

    @Attribute
    public void setContextState(short contextState) {
        this.contextState = contextState;
    }
    
}

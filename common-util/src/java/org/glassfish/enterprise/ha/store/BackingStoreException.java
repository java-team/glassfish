package org.glassfish.enterprise.ha.store;


public class BackingStoreException
    extends Exception {

    public BackingStoreException(String msg) {
        super(msg);
    }

    public BackingStoreException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
}

package org.glassfish.enterprise.ha.store.criteria.spi;

/**
 * The set of operators used by ExpressionBuilder
 *
 * @author Mahesh.Kannan@Sun.Com
 */
public enum Opcode {
    AND, OR, EQ, NEQ, LT, GT, LTE, GTE, IN, ATTR, LITERAL
}

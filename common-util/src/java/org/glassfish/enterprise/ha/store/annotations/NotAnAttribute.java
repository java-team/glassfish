package org.glassfish.enterprise.ha.store.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

/**
 * Annotation to declare a non Attribute of a StoreEntry.
 *
 * @author Mahesh.Kannan@Sun.Com
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NotAnAttribute {
}
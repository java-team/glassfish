/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.enterprise.ha.store.util;

import org.glassfish.enterprise.ha.store.annotations.Attribute;
import org.glassfish.enterprise.ha.store.spi.AttributeMetadata;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bhavanishankar@dev.java.net
 * @author Mahesh.Kannan@Sun.Com
 */

public class StoreEntryMetadataCache {

    private static final ConcurrentHashMap<String, Class> class2Entries =
            new ConcurrentHashMap<String, Class>();

    public static <S> Class getMetadata(String storeName, Class<S> clazz)
            throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        String entryClassName = clazz.getName();
        String key = storeName + ":" + entryClassName;
        if (!entryClassName.endsWith("_Metadata_")) {
            entryClassName = entryClassName + "_Metadata_";
        }

        Class metaClazz = null;
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        metaClazz = cl.loadClass(entryClassName);

        Class metadata = class2Entries.get(metaClazz);
        if (metadata == null) {
            synchronized (class2Entries) {
                metadata = class2Entries.get(metaClazz);
                if (metadata == null) {
                    createMetadata(clazz, metaClazz);
                    class2Entries.put(key, metaClazz);
                    System.out.println("Updated all fields for key: " + key);
                }
            }
        }

        return metadata;
    }

    public static <S> void createMetadata(Class<S> clazz, Class metaClazz)
            throws IllegalAccessException, NoSuchMethodException, NoSuchFieldException {

        HashMap<String, AttributeInfo> attrInfoMap =
                new HashMap<String, AttributeInfo>();

        for (Method m : clazz.getDeclaredMethods()) {
            loadAttributeInfo(clazz, m, attrInfoMap);
        }

        for (AttributeInfo ainfo : attrInfoMap.values()) {
            if (ainfo.getterMethod == null || ainfo.setterMethod == null) {
                //Fixme: Log warning or throw exception
                String unsetMethodType = (ainfo.getterMethod == null)
                        ? "getter" : "setter";
                System.err.println(ainfo.attrName + " has no " + unsetMethodType);
            } else {
                String attrName = ainfo.attrName;
                Field f = metaClazz.getField(attrName);
                if (!Modifier.isStatic(f.getModifiers())) {
                    System.out.println("Field: " + f.getName() + " is not static");
                    continue;
                }
                //FIXME wrap with doPrivileged
                f.setAccessible(true);
                AttributeMetadata<S, ?> attr = (AttributeMetadata) f.get(null);
                if (attr != null) {
                    ((AttributeMetadataImpl) attr).setGetterMethod(ainfo.getterMethod);
                    ((AttributeMetadataImpl) attr).setSetterMethod(ainfo.getterMethod);
                } else {
                    //Fixme: Log error and throw Exception
                    throw new RuntimeException("No static field named " + attrName);
                }

                System.out.println("Processed " + ainfo);
            }
        }
    }

    private static <S> void loadAttributeInfo(Class<S> clazz, Method m, HashMap<String, AttributeInfo> attrInfoMap)
            throws NoSuchMethodException {

        String methodName = m.getName();
        Attribute attr = (Attribute) m.getAnnotation(Attribute.class);
        if (attr != null) {
            if (methodName.startsWith("set")) {
                String attrName = attr.name();
                if (attrName == null || attrName.length() == 0) {
                    if (methodName.length() < 4) {
                        throw new RuntimeException("Invalid method name" + methodName);
                    } else {
                        attrName = ("" + methodName.charAt(3)).toLowerCase() + methodName.substring(4);
                    }
                } else {
                    attrName = m.getName();
                }

                AttributeInfo attrInfo = attrInfoMap.get(attrName);
                if (attrInfo == null) {
                    attrInfo = new AttributeInfo(attrName);
                    attrInfoMap.put(attrName, attrInfo);
                    attrInfo.setterMethod = m;

                    String getterName = "g" + methodName.substring(1);
                    try {
                        attrInfo.getterMethod = clazz.getDeclaredMethod(getterName, (Class []) null);
                    } catch (NoSuchMethodException nsmEx) {
                        throw nsmEx;
                    }
                } else {

                }
            } else {
                throw new RuntimeException("Warning: @Attribute specified to a non-setter method (" + methodName + ")");
            }
        }
    }

    private static class AttributeInfo {
        String attrName;
        Method getterMethod;
        Method setterMethod;

        AttributeInfo(String name) {
            this.attrName = name;
        }

        public String toString() {
            return " [attr: " + attrName + "; getter: " + getterMethod.getName() + "; setter: " + setterMethod.getName() + "]";
        }
    }


}
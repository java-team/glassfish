package org.glassfish.enterprise.ha.util.apt.processor;

import com.sun.mirror.declaration.ParameterDeclaration;

/**
 * @author Mahesh Kannan
 *         Date: Jun 11, 2009
 */
public interface MethodVisitor {

    public void visit(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType);

}
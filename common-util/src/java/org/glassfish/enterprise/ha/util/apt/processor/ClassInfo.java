package org.glassfish.enterprise.ha.util.apt.processor;

import com.sun.mirror.declaration.ClassDeclaration;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Mahesh Kannan
 *         Date: Jun 11, 2009
 */
public class ClassInfo {

    private ClassDeclaration classDecl;

    private String javaDoc;

    private Collection<MethodInfo> methodInfos
           = new ArrayList<MethodInfo>();

    public ClassInfo(ClassDeclaration classDecl) {
        this.classDecl = classDecl;
    }

    public void setJavaDoc(String javaDoc) {
        this.javaDoc = javaDoc;
    }

    public ClassDeclaration getClassDeclaration() {
        return classDecl;
    }

    public String getJavaDoc() {
        return javaDoc;
    }

    public Collection<MethodInfo> getMethodInfos() {
        return methodInfos;
    }

    void addMethodInfo(MethodInfo info) {
        methodInfos.add(info);
    }
}

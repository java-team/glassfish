package org.glassfish.enterprise.ha.util.apt.processor;

import com.sun.mirror.declaration.ParameterDeclaration;

/**
 * @author Mahesh Kannan
 *         Date: Jun 11, 2009
 */
public interface ClassVisitor {

    public void visit(String packageName, String javaDoc, String className);

    public void visitSetter(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType);

    public void visitMappedSetter(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType);

    public void visitVersionMethod(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType);

    public void visitHashKeyMethod(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType);

    public void visitEnd();

}

package org.glassfish.enterprise.ha.util.apt.processor;

import com.sun.mirror.apt.AnnotationProcessor;
import com.sun.mirror.apt.AnnotationProcessorEnvironment;
import com.sun.mirror.apt.AnnotationProcessorFactory;
import com.sun.mirror.apt.AnnotationProcessors;
import com.sun.mirror.declaration.AnnotationTypeDeclaration;
import com.sun.mirror.declaration.TypeDeclaration;

import java.util.*;

/**
 * @author Mahesh Kannan
 *         Date: Jun 9, 2009
 */
public class StoreEntryAnnotationProcessorFactory
        implements AnnotationProcessorFactory {

    String[] anns = new String[]{"*"};

    List<String> options = new ArrayList<String>();

    public Collection<String> supportedOptions() {
        return Collections.emptyList();
    }

    public Collection<String> supportedAnnotationTypes() {
        return Collections.singletonList("org.glassfish.enterprise.ha.store.annotations.StoreEntry");
    }

    public AnnotationProcessor getProcessorFor(Set<AnnotationTypeDeclaration> decls, AnnotationProcessorEnvironment annEnv) {
        AnnotationProcessor ap = null;
        if (decls.isEmpty()) {
            ap = AnnotationProcessors.NO_OP;
        } else {
            ap = new StoreEntryAnnotationProcessor(decls, annEnv);
        }
        return ap;
    }
    
}

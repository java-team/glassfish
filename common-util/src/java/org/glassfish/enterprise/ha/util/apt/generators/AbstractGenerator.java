package org.glassfish.enterprise.ha.util.apt.generators;

import com.sun.mirror.declaration.ParameterDeclaration;
import com.sun.mirror.type.PrimitiveType;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

/**
 * @author Mahesh Kannan
 *         Date: Jun 12, 2009
 */
public class AbstractGenerator {

    private int indent;

    private String space = "";

    protected Map<String, ParameterDeclaration> params =
            new HashMap<String, ParameterDeclaration>();

    protected Set<String> attrNames = new HashSet<String>();

    protected void increaseIndent() {
        indent++;
        space += "\t";
    }

    protected void decreaseIndent() {
        indent--;
        space = space.substring(1);
    }

    protected void println(String msg) {
        System.out.println(space + msg);
    }

    protected void print(String msg) {
        System.out.print(space + msg);
    }

    protected void println() {
        System.out.println(space);
    }

    protected void addAttribute(String attrName, ParameterDeclaration decl) {
        attrNames.add(attrName);
        params.put(attrName, decl);
    }

    protected static String getWrapperType(ParameterDeclaration type) {
        String result = type.toString();
        int index = result.lastIndexOf(' ');
        result = result.substring(0, index);

        if (type.getType() instanceof PrimitiveType) {
            result = Character.toUpperCase(result.charAt(0)) + result.substring(1);
        }

        int ltIndex = result.indexOf('<');

        return (ltIndex == -1) ? result : result.substring(0, ltIndex);
    }

}

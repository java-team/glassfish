package org.glassfish.enterprise.ha.util.apt.processor;

import com.sun.mirror.declaration.MethodDeclaration;
import com.sun.mirror.declaration.ParameterDeclaration;
import org.glassfish.enterprise.ha.store.annotations.Attribute;
import org.glassfish.enterprise.ha.store.annotations.MappedAttribute;

/**
 * @author Mahesh Kannan
 *         Date: Jun 11, 2009
 */
public class MethodInfo {

    public enum MethodType {SETTER, VERSION, HASHKEY, MAPPED};

    String attrName;

    MethodDeclaration setter;

    ParameterDeclaration paramType;

    Attribute attrAnn;

    MethodType type;

}
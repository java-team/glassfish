package org.glassfish.enterprise.ha.util.apt.processor;

import com.sun.mirror.apt.AnnotationProcessor;
import com.sun.mirror.apt.AnnotationProcessorEnvironment;
import com.sun.mirror.declaration.*;
import com.sun.mirror.util.DeclarationFilter;
import org.glassfish.enterprise.ha.store.annotations.Attribute;
import org.glassfish.enterprise.ha.store.annotations.HashKey;
import org.glassfish.enterprise.ha.store.annotations.MappedAttribute;
import org.glassfish.enterprise.ha.store.annotations.Version;
import org.glassfish.enterprise.ha.util.apt.generators.StorableGenerator;
import org.glassfish.enterprise.ha.util.apt.generators.StoreEntryMetadataGenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * @author Mahesh Kannan
 *         Date: Jun 10, 2009
 */
public class StoreEntryAnnotationProcessor
        implements AnnotationProcessor {

    private Set<AnnotationTypeDeclaration> decls;

    private AnnotationProcessorEnvironment env;

    private String qName;

    private Collection<ClassInfo> classInfos
            = new ArrayList<ClassInfo>();

    StoreEntryAnnotationProcessor(Set<AnnotationTypeDeclaration> decls,
                                  AnnotationProcessorEnvironment env) {
        this.decls = decls;
        this.env = env;
    }

    public void process() {
        int counter = 0;
        AnnotationTypeDeclaration storeEntryAnnDecls = decls.iterator().next();
        DeclarationFilter classFilter = DeclarationFilter.getFilter(ClassDeclaration.class);
        for (Declaration decl : classFilter.filter(env.getDeclarationsAnnotatedWith(storeEntryAnnDecls))) {
            if (decl != null) {
                ClassDeclaration classDecl = (ClassDeclaration) decl;
                ClassInfo classInfo = new ClassInfo(classDecl);
                classInfo.setJavaDoc(classDecl.getDocComment());
                classInfos.add(classInfo);
                qName = classDecl.getQualifiedName();


                DeclarationFilter setterFilter = new DeclarationFilter() {
                    public boolean matches(Declaration d) {
                        return d.getSimpleName().startsWith("set");
                    }
                };

                Collection<? extends MethodDeclaration> methods = classDecl.getMethods();
                ParameterDeclaration paramType = null;
                for (MethodDeclaration m : setterFilter.filter(methods)) {
                    MethodInfo methodInfo = new MethodInfo();
                    String attributeName = null;
                    Attribute attrAnn = m.getAnnotation(Attribute.class);
                    if (attrAnn != null) {
                        attributeName = attrAnn.name();
                        methodInfo.type = MethodInfo.MethodType.SETTER;
                    } else {
                        Version versionAnn = m.getAnnotation(Version.class);
                        if (versionAnn != null) {
                            attributeName = versionAnn.name();
                            methodInfo.type = MethodInfo.MethodType.VERSION;
                        } else {
                            HashKey hashKeyAnn = m.getAnnotation(HashKey.class);
                            if (hashKeyAnn != null) {
                                attributeName = hashKeyAnn.name();
                                methodInfo.type = MethodInfo.MethodType.HASHKEY;
                            } else {
                                MappedAttribute mappedAttrAnn = m.getAnnotation(MappedAttribute.class);
                                if (mappedAttrAnn != null) {
                                    attributeName = mappedAttrAnn.mappedTo();
                                    methodInfo.type = MethodInfo.MethodType.MAPPED;
                                } else {
                                    //Some getter method
                                    continue;
                                }
                            }
                        }
                    }

                    if (attributeName == null || attributeName.length() == 0) {
                        attributeName = m.getSimpleName();
                        attributeName = Character.toLowerCase(attributeName.charAt(3)) + attributeName.substring(4);
                    }
                    methodInfo.attrName = attributeName;

                    Collection<ParameterDeclaration> paramTypes = m.getParameters();
                    if (paramTypes.size() == 1) {
                        paramType = paramTypes.iterator().next();
                    } else {
                        //Fixme error?
                    }

                    methodInfo.setter = m;
                    methodInfo.paramType = paramType;
                    classInfo.addMethodInfo(methodInfo);
                }
            }
        }

        this.accept(new StorableGenerator());
        this.accept(new StoreEntryMetadataGenerator());
    }

    public void accept(ClassVisitor cv) {
        for (ClassInfo classInfo : classInfos) {
            ClassDeclaration classDecl = classInfo.getClassDeclaration();
            cv.visit(classDecl.getPackage().toString(), classInfo.getJavaDoc(), classDecl.getSimpleName());
            for (MethodInfo methodInfo : classInfo.getMethodInfos()) {
                switch (methodInfo.type) {
                    case SETTER:
                        cv.visitSetter(methodInfo.setter.getSimpleName(), methodInfo.attrName, null, methodInfo.paramType);
                        break;
                    case MAPPED:
                        cv.visitMappedSetter(methodInfo.setter.getSimpleName(), methodInfo.attrName, null, methodInfo.paramType);
                        break;
                    case VERSION:
                        cv.visitVersionMethod(methodInfo.setter.getSimpleName(), methodInfo.attrName, null, methodInfo.paramType);
                        break;
                    case HASHKEY:
                        cv.visitHashKeyMethod(methodInfo.setter.getSimpleName(), methodInfo.attrName, null, methodInfo.paramType);
                        break;
                }
            }
            cv.visitEnd();
        }
    }
}

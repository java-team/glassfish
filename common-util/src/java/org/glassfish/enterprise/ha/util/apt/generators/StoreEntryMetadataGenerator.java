package org.glassfish.enterprise.ha.util.apt.generators;

import com.sun.mirror.declaration.ParameterDeclaration;
import org.glassfish.enterprise.ha.util.apt.processor.ClassVisitor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Mahesh Kannan
 *         Date: Jun 11, 2009
 */
public class StoreEntryMetadataGenerator
        extends AbstractGenerator
        implements ClassVisitor {

    private String className;

    private int index = 0;

    private String versionAttrName;

    private String hashKeyAttrName;

    public void visit(String packageName, String javaDoc, String className) {
        this.className = className;
        println("package " + packageName + ";");
        println();
        println("import java.util.ArrayList;");
        println("import java.util.Collection;");
        println("import java.util.HashMap;");
        println("import java.util.Map;");
        println();
        println("import org.glassfish.enterprise.ha.store.spi.AttributeMetadata;");
        println("import org.glassfish.enterprise.ha.store.util.AttributeMetadataImpl;");
        println();
        println("/**");
        println(" * Metadata for " + className);
        println(" *");
        println(" */");
        println();
        println("public class MetadataFor" + className + "__");
        println("\timplements StoreEntryMetadata {");
        increaseIndent();
        println();
    }

    public void visitSetter(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        attrNames.add(attrName);
        println("//@Attribute(name=\"" + attrName + "\")");
        println("public static AttributeMetadata<" + className + ", " + getWrapperType(paramType) + "> "
                + attrName + " = ");
        println("\tnew AttributeMetadataImpl<" + className + ", " + getWrapperType(paramType) + ">("
                + index++ + ", \"" + attrName + "\", " + className + ".class" + ", " + getWrapperType(paramType) + ".class"
                + ");");
        println();
    }

    public void visitMappedSetter(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        attrNames.add(attrName);

    }

    public void visitVersionMethod(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        attrNames.add(attrName);
        println("//@Version(name=\"" + attrName + "\")");
        println("public static AttributeMetadata<" + className + ", " + getWrapperType(paramType) + "> "
                + attrName + " = ");
        println("\tnew AttributeMetadataImpl<" + className + ", " + getWrapperType(paramType) + ">("
                + index++ + ", \"" + attrName + "\", " + className + ".class" + ", " + getWrapperType(paramType) + ".class"
                + ");");
        versionAttrName = attrName;
        println();
    }

    public void visitHashKeyMethod(String methodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        attrNames.add(attrName);
        println("//@HashKey(name=\"" + attrName + "\")");
        println("public static AttributeMetadata<" + className + ", " + getWrapperType(paramType) + "> "
                + attrName + " = ");
        println("\tnew AttributeMetadataImpl<" + className + ", " + getWrapperType(paramType) + ">("
                + index++ + ", \"" + attrName + "\", " + className + ".class" + ", " + getWrapperType(paramType) + ".class"
                + ");");
        hashKeyAttrName = attrName;
        println();
    }

    public void visitEnd() {
        generateStoreEntryMetadataMethods();
        println("private static Collection<AttributeMetadata<" + className + ", ?>> attributes__");
        println("\t= new ArrayList<AttributeMetadata<" + className + ", ?>>();");
        println();
        println("private static Map<String, AttributeMetadata<" + className + ", ?>> attrMap__");
        println("\t= new HashMap<String, AttributeMetadata<" + className + ", ?>>();");
        println();
        print("static {");
        increaseIndent();
        if (versionAttrName != null) {
            println();
            println(versionAttrName + ".markAsVersionAttribute();");
        }
        if (hashKeyAttrName != null) {
            println();
            println(hashKeyAttrName + ".markAsHashKeyAttribute();");
        }
        println();
        for (String attr : attrNames) {
            println("attributes__.add(" + attr + ");");
        }
        println();
        for (String attr : attrNames) {
            println("attrMap__.put(\"" + attr + "\", " + attr + ");");
        }
        decreaseIndent();
        println("}");
        decreaseIndent();
        println("}");
        println();
    }
    
    private void generateStoreEntryMetadataMethods() {
        println("public AttributeMetadata<" + className + ", ?> getAttributeMetadata("
            + "String attrName) {");
        println("\treturn attrMap__.get(attrName);");
        println("}");

        println();
        println("public Collection<AttributeMetadata<" + className + ", ?>> getAllAttributeMetadata() {");
        println("\t return attrMap__.values();");
        println("}");

        println();
        println("public Collection<String> getAllAttributeNames() {");
        println("\t return attributes__;");
        println("}");

        println();
    }

}
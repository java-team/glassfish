package org.glassfish.enterprise.ha.util.apt.generators;

import com.sun.mirror.declaration.ParameterDeclaration;
import org.glassfish.enterprise.ha.util.apt.processor.ClassVisitor;

import java.util.StringTokenizer;

/**
 * @author Mahesh Kannan
 *         Date: Jun 11, 2009
 */
public class StorableGenerator
        extends AbstractGenerator
        implements ClassVisitor {

    private static final String ATTR_NAMES = "_dirtyAttributeNames";

    private String versionGetterMethodName;

    public void visit(String packageName, String javaDoc, String className) {
        println("package " + packageName + ";");
        println();
        println("import java.util.Set;");
        println("import java.util.HashSet;");
        println();
        println("import org.glassfish.enterprise.ha.store.spi.Storable;");
        println("import org.glassfish.enterprise.ha.store.MutableStoreEntry;");
        println();
        println("/**");
        StringTokenizer st = new StringTokenizer(javaDoc, "\n");
        while (st.hasMoreTokens()) {
            println(" * " + st.nextToken() + "\n * ");
        }
        println(" */");
        println();
        println("public class Storable" + className + "__");
        increaseIndent();
        println("extends " + className);
        println("implements Storable, MutableStoreEntry {");
        println();
        println("private String _storeName;");
        println();
        println("private String _hashKey;");
        println();
        println("private Set<String> " + ATTR_NAMES + " = new HashSet<String>();");
        println();
    }

    private void handleDirtyAttribute(String setterMethodName, String attrName, ParameterDeclaration paramType) {
        super.addAttribute(attrName, paramType);
        increaseIndent();
        println("public void " + setterMethodName + "("
                + getWrapperType(paramType) + ") { ");
        println("_markAsDirty(\"" + attrName + "\");");
        println("super." + setterMethodName + "(value);");
        decreaseIndent();
    }

    public void visitSetter(String setterMethodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        println("//@Attribute(name=\"" + attrName + "\")");
        handleDirtyAttribute(setterMethodName, attrName, paramType);
        println("}");
        println();
    }

    public void visitMappedSetter(String setterMethodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        println("//@MappedAttribute(name=\"" + attrName + "\")");
        handleDirtyAttribute(setterMethodName, attrName, paramType);
        println("}");
        println();
    }

    public void visitVersionMethod(String setterMethodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        versionGetterMethodName = setterMethodName;
        println("//@Version(name=\"" + attrName + "\")");
        handleDirtyAttribute(setterMethodName, attrName, paramType);
        println("}");
        println();
    }

    public void visitHashKeyMethod(String setterMethodName, String attrName, String javaDoc, ParameterDeclaration paramType) {
        println("//@HashKey(name=\"" + attrName + "\")");
        handleDirtyAttribute(setterMethodName, attrName, paramType);
        println("}");
        println();
    }

    public void visitEnd() {
        println("//Storable method");
        println("public String _getStoreName() {");
        increaseIndent();
        println("return _storeName;");
        decreaseIndent();
        println("}");
        println();

        println("public String _getHashKey() {");
        increaseIndent();
        println("return _hashKey;");
        decreaseIndent();
        println("}");
        println();

        String getVersionName = (versionGetterMethodName == null)
                ? null : versionGetterMethodName;
        if (getVersionName != null) {
            getVersionName = "g" + getVersionName.substring(1);
        }
        println("public String _getVersion() {");
        increaseIndent();
        println("return " + getVersionName + "();");
        decreaseIndent();
        println("}");
        println();

        println("public Set<String> _getDirtyAttributeNames() {");
        increaseIndent();
        println("return " + ATTR_NAMES + ";");
        decreaseIndent();
        println("}");
        println();

        println("//MutableStoreEntry methods");
        println("public void _markAsDirty(String attrName) {");
        increaseIndent();
        println(ATTR_NAMES + ".add(attrName);");
        decreaseIndent();
        println("}");
        println();

        println("public void _markAsClean(String attrName) {");
        increaseIndent();
        println(ATTR_NAMES + ".remove(attrName);");
        decreaseIndent();
        println("}");
        println();

        println("public void _markAsClean() {");
        increaseIndent();
        println(ATTR_NAMES + " = new HashSet<String>();");
        decreaseIndent();
        println("}");
        println();

        decreaseIndent();
        println("}");
    }
}

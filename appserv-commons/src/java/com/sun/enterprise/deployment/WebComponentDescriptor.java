/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
 package com.sun.enterprise.deployment;

import java.util.*;
import com.sun.enterprise.deployment.*;
import com.sun.enterprise.deployment.web.InitializationParameter;
import com.sun.enterprise.deployment.web.SecurityRoleReference;

    /**
    * I am a superclass representing the common data nad behavior of the deployment
    * information about a JSP or JavaServlet in J2EE.
    *
    *@author Danny Coward
    */
public class WebComponentDescriptor extends Descriptor  {
    
    private WebBundleDescriptor webBundleDescriptor = null;
    /** Constant for Basic authentication.*/
    public static final String BASIC_AUTHENTICATION = "basic";
    /** Constant for Form authentication.*/
    public static final String FORM_AUTHENTICATION = "form";
    /** Constant for Secure authentication.*/
    public static final String SSL_AUTHENTICATION = "ssl";
    /** Constant for the htpp GET method. */
    public static final String GET = "GET";
    /** Constant for the http PUT method.*/
    public static final String PUT = "PUT";
    /** Constant for the http POST method.*/
    public static final String POST = "POST";
    /** Constant for the http DELET method.*/
    public static final String DELETE = "DELETE";
    
    private Set initializationParameters;
    private Set urlPatterns;
    private String canonicalName;
    private int loadOnStartUp = -1;
    private Set securityRoleReferences;
    
    private RunAsIdentityDescriptor runAs;
    
    /**
    * The default constructor.
    */

    public WebComponentDescriptor() {}
    
    // the copy constructor
    public WebComponentDescriptor(WebComponentDescriptor other) {
        setCanonicalName(other.getCanonicalName());
        setServlet(other.isServlet()); 
        setWebComponentImplementation(
            other.getWebComponentImplementation()); 
        getInitializationParameterSet().addAll(
            other.getInitializationParameterSet()); 
        getUrlPatternsSet().addAll(other.getUrlPatternsSet());
        setLoadOnStartUp(other.getLoadOnStartUp());
        getSecurityRoleReferenceSet().addAll(
            other.getSecurityRoleReferenceSet());
        setRunAsIdentity(other.getRunAsIdentity());
        setWebBundleDescriptor(other.getWebBundleDescriptor());
    }

    private Set getInitializationParameterSet() {
	if (this.initializationParameters == null) {
	    this.initializationParameters = new OrderedSet();
	}
	return this.initializationParameters = new OrderedSet(this.initializationParameters);
    }
    
	/**
	* Return the Set of servlet initialization parameters.
	*/
    public Enumeration getInitializationParameters() {
	return (new Vector(this.getInitializationParameterSet())).elements();
    }
    
	/**
	* Return a matching initialization parameter by its name if there is one.
	*/
    public InitializationParameter getInitializationParameterByName(String name) {
	for (Iterator itr = this.getInitializationParameterSet().iterator(); itr.hasNext();) {
	    InitializationParameter next = (InitializationParameter) itr.next();
	    if (next.getName().equals(name)) {
		return next;
	    }
	}
	return null;
    }
    
	/**
	* Adds a servlet initialization parameter to this component.
	*/
    public void addInitializationParameter(InitializationParameter initializationParameter) {
	this.getInitializationParameterSet().add(initializationParameter);
	this.changed();
    }
    /**
    * Removes the given servlet initialization parameter from this component.
    */
    public void removeInitializationParameter(InitializationParameter initializationParameter) {
	this.getInitializationParameterSet().remove(initializationParameter);
	this.changed();
    }

	/**
	* Return the set of URL pattern aliases for this component.
	*/
    public Set getUrlPatternsSet() {
	if (this.urlPatterns == null) {
	    this.urlPatterns = new OrderedSet();
	}
	return this.urlPatterns;
    }

	/**
	* Returns an enumeration of (String) URL pattern aliases for this component.
	*/
    public Enumeration getUrlPatterns() {
	return (new Vector(this.getUrlPatternsSet())).elements();
    }
    
	/**
	* Adds an alias to this web component.
	*/
    public void addUrlPattern(String urlPattern) {
	this.getUrlPatternsSet().add(urlPattern);
	this.changed();
    }
    
	/**
	* Removes a URL pattern from this web component.
	*/
    public void removeUrlPattern(String urlPattern) {
	this.getUrlPatternsSet().remove(urlPattern);
	this.changed();
    }
    
    void setWebBundleDescriptor(WebBundleDescriptor webBundleDescriptor) {
	this.webBundleDescriptor = webBundleDescriptor;
    }
    
	/**
	* Return the web app object to which I belong or null
	*/
    public WebBundleDescriptor getWebBundleDescriptor() {
	return this.webBundleDescriptor;
    }
    
	/** 
	* The canonical name for the web component. 
	*/
    public String getCanonicalName() {
	if (this.canonicalName == null) {
	    this.canonicalName = this.getName();
	}
	return this.canonicalName;
    }
    
	/**
	* Sets the canonical name of this web component.
	*/
    public void setCanonicalName(String canonicalName) {
	this.canonicalName = canonicalName;
	this.changed();
    }
    
    /**
    * Return the order on which this component will be loaded by the web server.
    */
    public int getLoadOnStartUp() {
	return this.loadOnStartUp;
    }
    
    /**
    * Sets the order on which this component will be loaded by the web server.
    */
    public void setLoadOnStartUp(int loadOnStartUp) {
	this.loadOnStartUp = loadOnStartUp;
	this.changed();
    }
    
    /**
    * Sets the order on which this component will be loaded by the web server.
    */
    public void setLoadOnStartUp(String loadOnStartUp) throws NumberFormatException {
        this.loadOnStartUp = Integer.decode(loadOnStartUp).intValue();
        this.changed();
    }    
    
    Set getSecurityRoleReferenceSet() {
	if (this.securityRoleReferences == null) {
	    this.securityRoleReferences = new OrderedSet();
	}
	return this.securityRoleReferences = new OrderedSet(this.securityRoleReferences);
    }
    
    /**
    * Return the Set of security role references that I have.
    */
    public Enumeration getSecurityRoleReferences() {
	return (new Vector(this.getSecurityRoleReferenceSet())).elements();
    }
    
    /** 
    * Return a matching role reference by name or null if there is none matching.
    */
    public SecurityRoleReference getSecurityRoleReferenceByName(String roleReferenceName) {
	for (Enumeration e = this.getSecurityRoleReferences(); e.hasMoreElements();) {
	    SecurityRoleReference nextRR = (SecurityRoleReference) e.nextElement();
	    if (nextRR.getRolename().equals( roleReferenceName )) {
		return nextRR; 
	    }
	}
	return null;   
    }
    
    /**
    * Adds a security role reference to this web component.
    */
    public void addSecurityRoleReference(SecurityRoleReference securityRoleReference) {
	this.getSecurityRoleReferenceSet().add(securityRoleReference);
	this.changed();
    }
    
    /** 
    * Removes the given security role reference from this web component.
    */
    public void removeSecurityRoleReference(SecurityRoleReference securityRoleReference) {
	this.getSecurityRoleReferenceSet().remove(securityRoleReference);
	this.changed();
    }
    
     /**
      * Gets the run-as of the referee EJB.
      * @return the value of run-as.
      */
     public void setRunAsIdentity(RunAsIdentityDescriptor runAs) {
         if (this.runAs == null) {
             this.runAs = runAs;
         }
     }
     
    /**
      * Sets the run-as of the referee EJB.
      * @param the value of run-as
      */ 
     public RunAsIdentityDescriptor getRunAsIdentity() {
         return this.runAs;
     }    

     public boolean getUsesCallerIdentity() {
	return (this.runAs==null);
     }

     public void setUsesCallerIdentity(boolean isCallerID) {
	if (isCallerID) { 
	    this.runAs = null;
	} else {
	    this.runAs = new RunAsIdentityDescriptor("");
        }
     }

     public Application getApplication() {
	if (this.getWebBundleDescriptor()!=null) {
	    return this.getWebBundleDescriptor().getApplication();
        }
	return null;
     }
     
     private String implFile="";
     private boolean isServlet=false;
     
     /**
      * sets the implementation file for this web component, the 
      * implementation file is either a servlet class name of a jsp
      * file name.
      * @param implFile the servlet class name or the jsp file
      */
     public void setWebComponentImplementation(String implFile) {
         if (!isServlet &&  !implFile.startsWith("/")) {
		implFile = "/" + implFile;
	 }             
         this.implFile = implFile;
         changed();
     }
     
     public String getWebComponentImplementation() {
         return implFile;         
     }
     
     public boolean isServlet() {
         return isServlet;
     }
     
     public void setServlet(boolean isServlet) {
         this.isServlet=isServlet;
     }
     
    /* -----------
    */    

    /**
    * A formatted string representing my state.
    */
    public void print(StringBuffer toStringBuffer) {
	super.print(toStringBuffer);
        toStringBuffer.append("WebComponentDescriptor\n");        
	toStringBuffer.append( "\n initializationParameters ").append(initializationParameters);
	toStringBuffer.append( "\n urlPatterns ").append(urlPatterns);
	toStringBuffer.append( "\n canonicalName ").append(canonicalName);
	toStringBuffer.append( "\n loadOnStartUp ").append(loadOnStartUp);
	toStringBuffer.append( "\n securityRoleReferences ").append(securityRoleReferences);
        if (isServlet()) {
            toStringBuffer.append( "\n servlet className ").append(getWebComponentImplementation());            
        } else {
            toStringBuffer.append( "\n jspFileName ").append(getWebComponentImplementation());            
        }        
    }

        
    public void changed() {
	if (this.getWebBundleDescriptor() != null) {
	    this.getWebBundleDescriptor().changed();
	} else {
	    super.changed();
	}
    }    

    public boolean equals(Object other) {
        if (other instanceof WebComponentDescriptor &&
            this.getCanonicalName().equals(((
            WebComponentDescriptor)other).getCanonicalName())) {
                return true;
        }
        return false;
    }

    public int hashCode() {
        int result = 17;
        result = 37*result + getCanonicalName().hashCode();
        return result;
    }


    // this method will combine the information from this "other" 
    // WebComponentDescriptor with current WebComponentDescriptor
    //
    // when there are conflicts between the contents of the two,
    // the value from current WebComponentDescriptor will override
    // the value in "other"
    //
    // Note: in the Set API, we only add value when such value 
    // is not existed in the Set already
    public void add(WebComponentDescriptor other) {
        // do not do anything if the canonical name of the two web 
        // components are different
        if (! getCanonicalName().equals(other.getCanonicalName())) {
            return;
        }
        // do not do anything if the type of the two web 
        // components are different
        if ( (isServlet() && !other.isServlet()) || 
             (!isServlet() && other.isServlet()) ) {
            return;
        }

        // for simple String types, we can rely on Set API
        getUrlPatternsSet().addAll(other.getUrlPatternsSet());

        // for complex types, only added it if the complex type with same 
        // name is not in the set yet 

        for (Iterator initParamIter = 
            other.getInitializationParameterSet().iterator();  
            initParamIter.hasNext();) {
            InitializationParameter initParam = 
                (InitializationParameter)initParamIter.next();
            if (getInitializationParameterByName(initParam.getName()) == null) {
                getInitializationParameterSet().add(initParam);
            } 
        }
        for (Iterator secRoleRefIter = 
            other.getSecurityRoleReferenceSet().iterator();
            secRoleRefIter.hasNext();) {
            SecurityRoleReference secRoleRef =
                (SecurityRoleReference) secRoleRefIter.next();
            if (getSecurityRoleReferenceByName(secRoleRef.getRolename()) 
                == null) {
                getSecurityRoleReferenceSet().add(secRoleRef);
            }
        }

        // only set these values if they are not set in the current
        // web component already

        if (getLoadOnStartUp() == -1) {
            setLoadOnStartUp(other.getLoadOnStartUp());
        }
        if (getRunAsIdentity() == null) {  
            setRunAsIdentity(other.getRunAsIdentity());
        }
        if (getWebComponentImplementation() == null) {
            setWebComponentImplementation(
                other.getWebComponentImplementation()); 
        }
    }
}

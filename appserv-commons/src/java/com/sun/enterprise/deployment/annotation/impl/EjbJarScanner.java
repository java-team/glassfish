/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.deployment.annotation.impl;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

import com.sun.enterprise.deployment.EjbBundleDescriptor;
import com.sun.enterprise.deployment.EjbDescriptor;
import com.sun.enterprise.deployment.EjbSessionDescriptor;
import com.sun.enterprise.deployment.EjbMessageBeanDescriptor;
import com.sun.enterprise.deployment.EjbInterceptor;

/**
 * Implementation of the Scanner interface for Ejb jar.
 *
 * @author Shing Wai Chan
 */
public class EjbJarScanner extends ModuleScanner {
    public EjbJarScanner(File archiveFile, EjbBundleDescriptor desc)
            throws IOException {
        this(archiveFile, desc, null);
    }

    /**
     * This scanner will scan the archiveFile for annotation processing.
     * @param archiveFile
     * @param classLoader
     */
    public EjbJarScanner(File archiveFile, EjbBundleDescriptor desc, 
        ClassLoader classLoader) throws IOException {
        if (AnnotationUtils.getLogger().isLoggable(Level.FINE)) {
            AnnotationUtils.getLogger().fine("archiveFile is " + archiveFile);
            AnnotationUtils.getLogger().fine("classLoader is " + classLoader);
        }
        this.archiveFile = archiveFile;
        this.classLoader = classLoader;
        if (archiveFile.isDirectory()) {
            addScanDirectory(archiveFile);

            // always add session beans, message driven beans,
            // interceptor classes that are defined in ejb-jar.xml r
            // regardless of they have annotation or not
            for (Iterator ejbs = desc.getEjbs().iterator(); ejbs.hasNext();) {
                EjbDescriptor ejbDesc = (EjbDescriptor)ejbs.next();
                if (ejbDesc instanceof EjbSessionDescriptor || 
                    ejbDesc instanceof EjbMessageBeanDescriptor) {
                    addScanClassName(ejbDesc.getEjbClassName());
                }
            }

            for (Iterator interceptors = desc.getInterceptors().iterator(); 
                interceptors.hasNext();) {
                EjbInterceptor interceptor = 
                    (EjbInterceptor)interceptors.next();
                addScanClassName(interceptor.getInterceptorClassName());
            }

        } // else in app client jar

    }
}

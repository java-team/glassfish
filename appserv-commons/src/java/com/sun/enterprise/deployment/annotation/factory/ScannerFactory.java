/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.annotation.factory;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.enterprise.deploy.shared.ModuleType;

import com.sun.enterprise.deployment.annotation.impl.AppClientScanner;
import com.sun.enterprise.deployment.annotation.impl.EjbJarScanner;
import com.sun.enterprise.deployment.annotation.impl.WarScanner;
import com.sun.enterprise.deployment.annotation.Scanner;
import com.sun.enterprise.deployment.ApplicationClientDescriptor;
import com.sun.enterprise.deployment.archivist.Archivist;
import com.sun.enterprise.deployment.BundleDescriptor;
import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.WebBundleDescriptor;
import com.sun.enterprise.deployment.EjbBundleDescriptor;

/**
 * The Factory is reponsible for creating Scanner.
 *
 * @author Shing Wai Chan
 */
public class ScannerFactory {
    private ScannerFactory() {
    }

    public static Scanner createScanner(BundleDescriptor bundleDesc,
            Archivist archivist, AbstractArchive abstractArchive,
            ClassLoader classLoader) throws IOException {
        Scanner scanner = null;
        File f = new File(abstractArchive.getArchiveUri());

        if (ModuleType.EJB.equals(archivist.getModuleType())) {
            scanner = new EjbJarScanner(f, (EjbBundleDescriptor)bundleDesc,
                   classLoader);
        } else if (ModuleType.WAR.equals(archivist.getModuleType())) {
            scanner = new WarScanner(f, (WebBundleDescriptor)bundleDesc,
                   classLoader);
        } else if (ModuleType.CAR.equals(archivist.getModuleType())) {
            ApplicationClientDescriptor appClientDesc =
                    (ApplicationClientDescriptor)bundleDesc;

            scanner = new AppClientScanner(f, appClientDesc, classLoader);
        }

        return scanner;
    }
}

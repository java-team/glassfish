/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.deployment.annotation.factory;

import java.util.HashSet;
import java.util.Set;

import com.sun.enterprise.deployment.annotation.AnnotationHandler;
import com.sun.enterprise.deployment.annotation.AnnotationProcessor;
import com.sun.enterprise.deployment.annotation.handlers.*;
import com.sun.enterprise.deployment.annotation.impl.AnnotationProcessorImpl;

/**
 * This factory is responsible for initializing a ready to use
 * AnnotationProcessor. 
 *
 * @author Shing Wai Chan
 */
public class SJSASFactory extends Factory {
    private static Set<String> annotationClassNames = new HashSet<String>();

    private static AnnotationProcessorImpl systemProcessor=null;

    private SJSASFactory() {
    }

    public static AnnotationProcessor getAnnotationProcessor() {
        init();
        AnnotationProcessorImpl processor = 
            Factory.getDefaultAnnotationProcessor();
        processor.setDelegate(systemProcessor);
        return processor;
    }

    public static Set<String> getAnnotations() {
        init();
        return (HashSet)((HashSet)annotationClassNames).clone();
    }

    public static AnnotationProcessor getSystemAnnotationProcessor() {
        return systemProcessor;
    }

    private static synchronized void init() {
        if (systemProcessor == null) {
            // initialize our system annotation processor...            
            systemProcessor = new AnnotationProcessorImpl();
            for (AnnotationHandler anHandler : getSystemAnnotationHandlers()) {
                systemProcessor.pushAnnotationHandler(anHandler); 
                annotationClassNames.add("L" +
                    anHandler.getAnnotationType().getName().
                    replace('.', '/') + ";");
            }
        }
    }

    private static AnnotationHandler[] getSystemAnnotationHandlers() {
        return new AnnotationHandler[] {
                new DenyAllHandler(),
                new ApplicationExceptionHandler(),
                new AroundInvokeHandler(),
                new DeclareRolesHandler(),
                new EntityManagerFactoryReferenceHandler(),
                new EntityManagerFactoryReferencesHandler(),
                new EntityManagerReferenceHandler(),
                new EntityManagerReferencesHandler(),
                new ExcludeClassInterceptorsHandler(),
                new ExcludeDefaultInterceptorsHandler(),
                new EJBHandler(),
                new EJBsHandler(),
                new HandlerChainHandler(),                        
                new InitHandler(),
                new InterceptorsHandler(),
                new MessageDrivenHandler(),
                new PermitAllHandler(),
                new PostActivateHandler(),
                new PostConstructHandler(),
                new PreDestroyHandler(),
                new PrePassivateHandler(),
                new ResourceHandler(),
                new RolesAllowedHandler(),
                new ResourcesHandler(),
                new RemoveHandler(),
                new RunAsHandler(),
                new StatefulHandler(),
                new StatelessHandler(),
                new TimeoutHandler(),    
                new TransactionAttributeHandler(),
                new TransactionManagementHandler(),
                new WebServiceHandler(),
                new WebServiceProviderHandler(),
		new WebServiceRefHandler(),
		new WebServiceRefsHandler()
        };
    }
}

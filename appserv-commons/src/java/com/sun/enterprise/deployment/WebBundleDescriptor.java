package com.sun.enterprise.deployment;
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
 import java.util.*;

import com.sun.enterprise.util.NotificationListener;
import com.sun.enterprise.util.NotificationEvent;

import com.sun.enterprise.deployment.PersistenceUnitsDescriptor;
import com.sun.enterprise.deployment.web.MimeMapping;
import com.sun.enterprise.deployment.web.ErrorPageDescriptor;
import com.sun.enterprise.deployment.web.AppListenerDescriptor;
import com.sun.enterprise.deployment.web.ResourceReference;
import com.sun.enterprise.deployment.web.ContextParameter;
import com.sun.enterprise.deployment.web.SecurityRole;
import com.sun.enterprise.deployment.web.SecurityRoleReference;
import com.sun.enterprise.deployment.web.SecurityConstraint;
import com.sun.enterprise.deployment.web.LocalizedContentDescriptor;
import com.sun.enterprise.deployment.web.LoginConfiguration;
import com.sun.enterprise.deployment.web.EnvironmentEntry;
import com.sun.enterprise.deployment.web.ServletFilter;
import com.sun.enterprise.deployment.web.ServletFilterMapping;
import com.sun.enterprise.deployment.web.WebResourceCollection;
import com.sun.enterprise.deployment.WebServiceEndpoint;
import com.sun.enterprise.deployment.node.web.WebBundleNode;

import com.sun.enterprise.deployment.util.WebBundleVisitor;
import com.sun.enterprise.deployment.util.DescriptorVisitor;

import com.sun.enterprise.deployment.types.EjbReference;
import com.sun.enterprise.deployment.types.ResourceReferenceContainer;
import com.sun.enterprise.deployment.types.ResourceEnvReferenceContainer;
import com.sun.enterprise.deployment.types.MessageDestinationReference;
import com.sun.enterprise.deployment.types.MessageDestinationReferenceContainer;
import com.sun.enterprise.deployment.types.EjbReferenceContainer;
import com.sun.enterprise.deployment.types.ServiceReferenceContainer;
import com.sun.enterprise.deployment.types.MessageDestinationReferencer;

import com.sun.enterprise.deployment.runtime.web.SunWebApp;

import javax.enterprise.deploy.shared.ModuleType;
import com.sun.enterprise.util.LocalStringManagerImpl;

    /** 
    * I am an object that represents all the deployment information about
    * a web app [{0}]lication.
    * @author Danny Coward
    */

public class WebBundleDescriptor extends BundleDescriptor 
    implements  WritableJndiNameEnvironment,
                ResourceReferenceContainer,
                ResourceEnvReferenceContainer,
                EjbReferenceContainer,
                MessageDestinationReferenceContainer,
                ServiceReferenceContainer,
                NotificationListener

{


    private Set webComponentDescriptors;
    private int sessionTimeout;
    private Set mimeMappings;
    private Set welcomeFiles;
    private Set errorPageDescriptors;
    private Vector appListenerDescriptors;
    private Set contextParameters;
    private Set ejbReferences;
    private Set resourceReferences;
    private Set jmsDestReferences;
    private Set messageDestReferences;
    private Set serviceReferences;

    private Set<LifecycleCallbackDescriptor> postConstructDescs =
        new HashSet<LifecycleCallbackDescriptor>();
    private Set<LifecycleCallbackDescriptor> preDestroyDescs =
        new HashSet<LifecycleCallbackDescriptor>();

    private Set<EntityManagerFactoryReferenceDescriptor> 
        entityManagerFactoryReferences = 
        new HashSet<EntityManagerFactoryReferenceDescriptor>();

    private Set<EntityManagerReferenceDescriptor> 
        entityManagerReferences = 
        new HashSet<EntityManagerReferenceDescriptor>();

    private boolean isDistributable;
    private Set securityRoles;
    private Set securityConstraints;
    private String contextRoot;
    private LoginConfiguration loginConfiguration;
    private Set environmentEntries;
    private LocaleEncodingMappingListDescriptor localeEncodingMappingDesc = null;
    private JspConfigDescriptor jspConfigDescriptor = null;    

    private Vector servletFilters = null;
    private Vector servletFilterMappings = null;

    public  static final int SESSION_TIMEOUT_DEFAULT = 30;
    private final static String DEPLOYMENT_DESCRIPTOR_DIR="WEB-INF";    
    
    private static LocalStringManagerImpl localStrings =
	    new LocalStringManagerImpl(WebBundleDescriptor.class);
   
   private SunWebApp sunWebApp=null;
   
    /** Constrct an empty web app [{0}].
    */
    public WebBundleDescriptor() {
        sessionTimeout = SESSION_TIMEOUT_DEFAULT;
    }   
    
    /**
     * @return the default version of the deployment descriptor
     * loaded by this descriptor
     */
    public String getDefaultSpecVersion() {
        return WebBundleNode.SPEC_VERSION;
    }

    /**
    * Return the set of named descriptors that I have.
    */
    public Collection getNamedDescriptors() {
	return super.getNamedDescriptorsFrom(this);
    }
    
   /** Return the saet of NamedReferencePairs that I have.
   */     
    public Vector getNamedReferencePairs() {
	return super.getNamedReferencePairsFrom(this);
    }
    
    /**
    * return the name of my context root
    */
    public String getContextRoot() {
        if (getModuleDescriptor()!=null && getModuleDescriptor().getContextRoot()!=null) {
            return getModuleDescriptor().getContextRoot();
        }        
	if (this.contextRoot == null) {
	    this.contextRoot = "";
	}
	return this.contextRoot;
    }
    
    /**
    * Set the name of my context root.
    */
    public void setContextRoot(String contextRoot) {
        if (getModuleDescriptor()!=null) {
            getModuleDescriptor().setContextRoot(contextRoot);
        }
	this.contextRoot = contextRoot;
	this.changed();
    }
    
    public void addWebBundleDescriptor(WebBundleDescriptor webBundleDescriptor) {
	super.addBundleDescriptor(webBundleDescriptor);

	// mdf - #4400074 ejb added to existing ejb-jar in wizard has wrong bundle
	// (this problem occured for WebComponents as well)
	//this.getWebComponentDescriptorsSet().addAll(webBundleDescriptor.getWebComponentDescriptorsSet());
	for (Iterator itr = webBundleDescriptor.getWebComponentDescriptorsSet().iterator(); itr.hasNext();) {
	    WebComponentDescriptor webComponentDescriptor = 
                new WebComponentDescriptor((WebComponentDescriptor)itr.next());
	    webComponentDescriptor.setWebBundleDescriptor(this);
	    this.getWebComponentDescriptorsSet().add(webComponentDescriptor);
	}

	this.getMimeMappingsSet().addAll(webBundleDescriptor.getMimeMappingsSet());
	this.getWelcomeFilesSet().addAll(webBundleDescriptor.getWelcomeFilesSet());
	this.getErrorPageDescriptorsSet().addAll(webBundleDescriptor.getErrorPageDescriptorsSet());
	this.getAppListeners().addAll(webBundleDescriptor.getAppListeners());
	this.getContextParametersSet().addAll(webBundleDescriptor.getContextParametersSet());
        Set ejbRefDescs =webBundleDescriptor.getEjbReferenceDescriptors();
        Iterator iter = ejbRefDescs.iterator();
        while(iter.hasNext()) {
            this.addEjbReferenceDescriptor((EjbReferenceDescriptor)(iter.next()));
        }
	this.getResourceReferenceDescriptors().addAll(webBundleDescriptor.getResourceReferenceDescriptors());
        this.getMessageDestinationReferenceDescriptors().addAll(webBundleDescriptor.getMessageDestinationReferenceDescriptors());
	this.getServiceReferenceDescriptors().addAll(webBundleDescriptor.getServiceReferenceDescriptors());
	this.getEnvironmentProperties().addAll(webBundleDescriptor.getEnvironmentProperties());
	this.getSecurityConstraintsSet().addAll(webBundleDescriptor.getSecurityConstraintsSet());

        this.setLoginConfiguration(webBundleDescriptor.getLoginConfiguration());
	// ServletFilters
	// mdf - #4399820 Servlet Filters and their mapping don't appear in inspector
	this.getServletFilters().addAll(webBundleDescriptor.getServletFilters());
	this.getServletFilterMappings().addAll(webBundleDescriptor.getServletFilterMappings());
	this.setLocaleEncodingMappingListDescriptor(webBundleDescriptor.getLocaleEncodingMappingListDescriptor());
	this.setJspConfigDescriptor(webBundleDescriptor.getJspConfigDescriptor());
        Enumeration enu = webBundleDescriptor.getSecurityRoles();
        while(enu.hasMoreElements()){
            SecurityRoleDescriptor srd = (SecurityRoleDescriptor) enu.nextElement();
            this.addSecurityRole(srd);
        }

	// WebServices
	WebServicesDescriptor thisWebServices = this.getWebServices();
	WebServicesDescriptor otherWebServices = webBundleDescriptor.getWebServices();
	for (Iterator i = otherWebServices.getWebServices().iterator(); i.hasNext();) {
	    WebService ws = (WebService)i.next();
	    thisWebServices.addWebService(new WebService(ws));
	}
	this.changed();
    }
    
    /** Return the Set of Web COmponent Descriptors (JSP or JavaServlets) in me.
    */
    public Set getWebDescriptors() {
	if (this.webComponentDescriptors == null) {
	    this.webComponentDescriptors = new OrderedSet();
	}
	return this.webComponentDescriptors;
    }
    
    public Set getWebComponentDescriptorsSet() {
	if (this.webComponentDescriptors == null) {
	    this.webComponentDescriptors = new OrderedSet();
	}
	return this.webComponentDescriptors;
    }

    /** Return an Enumeration of Web COmponent Descriptors (JSP or JavaServlets) in me.
    */
    public Enumeration getWebComponentDescriptors() {
	return (new Vector(this.getWebComponentDescriptorsSet())).elements();
    }
    
    /** Adds a new Web Component Descriptor to me.
    */
    
    public void addWebComponentDescriptor(WebComponentDescriptor webComponentDescriptor) {
	((WebComponentDescriptor) webComponentDescriptor).setWebBundleDescriptor(this);
        for (Iterator wcdIter = getWebComponentDescriptorsSet().iterator(); 
            wcdIter.hasNext();) {
            WebComponentDescriptor wbd = (WebComponentDescriptor)wcdIter.next();
            if (wbd.getCanonicalName().equals(
                webComponentDescriptor.getCanonicalName())) {
                // combine the contents of the two
                webComponentDescriptor.add(wbd);
                // remove the original one from the set
                // so we can add the new one
                getWebComponentDescriptorsSet().remove(wbd);
                break;
            }
        }
	this.getWebComponentDescriptorsSet().add(webComponentDescriptor);
	this.changed();
    }   
    
    /**
    * Remove the given web component from me.
    */
    
    public void removeWebComponentDescriptor(WebComponentDescriptor webComponentDescriptor) {
	((WebComponentDescriptor) webComponentDescriptor).setWebBundleDescriptor(null);
	this.getWebComponentDescriptorsSet().remove(webComponentDescriptor);
	this.changed();
    }   
    
    /**
     * WEB SERVICES REF APIS
     */
    public boolean hasServiceReferenceDescriptors() {
	if (serviceReferences==null) 
	    return false;
	return serviceReferences.size()!=0;
    }
    
    public Set getServiceReferenceDescriptors() {
        if( this.serviceReferences == null ) {
            this.serviceReferences = new OrderedSet();
        }
        return this.serviceReferences = new OrderedSet(this.serviceReferences);
    }

    public void addServiceReferenceDescriptor(ServiceReferenceDescriptor 
                                              serviceRef) {
        serviceRef.setBundleDescriptor(this);
        this.getServiceReferenceDescriptors().add(serviceRef);
        this.changed();
    }

    public void removeServiceReferenceDescriptor(ServiceReferenceDescriptor 
                                                 serviceRef) {
        this.getServiceReferenceDescriptors().remove(serviceRef);
        this.changed();
    }
    
    /**
     * Looks up an service reference with the given name. 
     * Throws an IllegalArgumentException if it is not found.
     */
    public ServiceReferenceDescriptor getServiceReferenceByName(String name) {
	for (Iterator itr = this.getServiceReferenceDescriptors().iterator(); 
             itr.hasNext();) {
	    ServiceReferenceDescriptor srd = (ServiceReferenceDescriptor) 
                itr.next();
	    if (srd.getName().equals(name)) {
		return srd;
	    }
	}
        throw new IllegalArgumentException(localStrings.getLocalString(
            "enterprise.deployment.exceptionwebapphasnoservicerefbyname",
            "This web app [{0}] has no service reference by the name of [{1}]",
            new Object[] {getName(), name}));
    }

/**
	* Return the set of JMS destination references this ejb declares.
	*/
    public Set getJmsDestinationReferenceDescriptors() {
	if (this.jmsDestReferences == null) {
	    this.jmsDestReferences = new OrderedSet();
	}
	return this.jmsDestReferences = new OrderedSet(this.jmsDestReferences);
    }

    public void addJmsDestinationReferenceDescriptor(JmsDestinationReferenceDescriptor jmsDestReference) {
	this.getJmsDestinationReferenceDescriptors().add(jmsDestReference);
	this.changed();
    }
    
    public void removeJmsDestinationReferenceDescriptor(JmsDestinationReferenceDescriptor jmsDestReference) {
	this.getJmsDestinationReferenceDescriptors().remove(jmsDestReference);
	this.changed();
    }

    /**
    * Return a JMS destination reference by the same name or throw an IllegalArgumentException.
    */
    public JmsDestinationReferenceDescriptor getJmsDestinationReferenceByName(String name) {
	for (Iterator itr = this.getJmsDestinationReferenceDescriptors().iterator(); itr.hasNext();) {
	    JmsDestinationReferenceDescriptor jdr = (JmsDestinationReferenceDescriptor) itr.next();
	    if (jdr.getName().equals(name)) {
		return jdr;   
	    }
	}
	throw new IllegalArgumentException(localStrings.getLocalString(
								       "enterprise.deployment.exceptionwebapphasnojmsdestrefbyname",
								       "This web app [{0}] has no resource environment reference by the name of [{1}]", new Object[] {getName(), name}));
    }

	/**
	* Return the value in seconds of when requests should time out.
	*/
    public int getSessionTimeout() {
	return this.sessionTimeout;
    }
    
	/** Sets thew value in seconds after sessions should timeout.
	*/
    public void setSessionTimeout(int sessionTimeout) {
	this.sessionTimeout = sessionTimeout;
	this.changed();
    }
    
    private Set getMimeMappingsSet() {
	if (this.mimeMappings == null) {
	    this.mimeMappings = new HashSet();
	}
	return this.mimeMappings;
    }
    
	/**
	* Sets the Set of Mime Mappings for this web app [{0}]lication.
	*/
    public void setMimeMappings(Set mimeMappings) {
	this.mimeMappings = mimeMappings;
	this.changed();
    }
    
	/**
	* Returns an enumeration of my mime mappings.
	*/
    public Enumeration getMimeMappings() {
	return (new Vector(this.getMimeMappingsSet())).elements();
    }
    
	/**
	* Adds the given mime mapping to my list.
	*/
    public void addMimeMapping(MimeMapping mimeMapping) {
        // always override
        // Since Set.add API doesn't replace
        // remove the element first if it's already contained
	for (Iterator itr = getMimeMappingsSet().iterator(); itr.hasNext();) {
	    MimeMapping mm = (MimeMapping) itr.next();
	    if (mm.getExtension().equals(mimeMapping.getExtension())) {
		getMimeMappingsSet().remove(mm);   
                break;
	    }
        }
	this.getMimeMappingsSet().add(mimeMapping);
	this.changed();
    }
    
	/**
	* Adds the given mime mapping to my list.
	*/
    public void addMimeMapping(MimeMappingDescriptor mimeMapping) {
        addMimeMapping((MimeMapping) mimeMapping);
    }
    
    public void addLocaleEncodingMappingListDescriptor(LocaleEncodingMappingListDescriptor lemDesc) {
	localeEncodingMappingDesc = lemDesc;
    }

    public LocaleEncodingMappingListDescriptor getLocaleEncodingMappingListDescriptor() {
	return localeEncodingMappingDesc;
    }
    
    public void setLocaleEncodingMappingListDescriptor(LocaleEncodingMappingListDescriptor lemDesc) {
	localeEncodingMappingDesc = lemDesc;
    }
	/**
	* Removes the given mime mapping from my list.
	*/
    public void removeMimeMapping(MimeMapping mimeMapping) {
	this.getMimeMappingsSet().remove(mimeMapping);
	this.changed();
    }
    
	/**
	* Return an enumeration of the welcome files I have..
	*/
    public Enumeration getWelcomeFiles() {
	return (new Vector(this.getWelcomeFilesSet())).elements();
    }
    
    public Set getWelcomeFilesSet() {
	if (this.welcomeFiles == null) {
	    this.welcomeFiles = new OrderedSet();
	}
	return welcomeFiles;
    }
    
	/** Adds a new welcome file to my list.
	*/
    public void addWelcomeFile(String fileUri) {
	this.getWelcomeFilesSet().add(fileUri);
	this.changed();
    }
    
	/** Removes a welcome file from my list.
	*/
    public void removeWelcomeFile(String fileUri) {
	this.getWelcomeFilesSet().remove(fileUri);
	this.changed();
    }
    
	/**
	* Sets the collection of my welcome files.
	*/
    public void setWelcomeFiles(Set welcomeFiles) {
	this.welcomeFiles = welcomeFiles;
	this.changed();
    }

    private Set getErrorPageDescriptorsSet() {
	if (this.errorPageDescriptors == null) {
	    this.errorPageDescriptors = new HashSet();
	}
	return this.errorPageDescriptors;
    }
	
	/** Returns an enumeration of the error pages I have.
	*/
    public Enumeration getErrorPageDescriptors() {
	return (new Vector(this.getErrorPageDescriptorsSet())).elements();
    }

	/** Adds a new error page to my list.
	*/
    public void addErrorPageDescriptor(ErrorPageDescriptor errorPageDescriptor) {
	this.getErrorPageDescriptorsSet().add(errorPageDescriptor);
	this.changed();
    }
    
    public void addErrorPageDescriptor(ErrorPageDescriptorImpl errorPageDescriptor) {
	addErrorPageDescriptor((ErrorPageDescriptor) errorPageDescriptor);
    }    
    
	/**
	* Removes the given error page from my list.
	*/
    public void removeErrorPageDescriptor(ErrorPageDescriptor errorPageDescriptor) {
	this.getErrorPageDescriptorsSet().remove(errorPageDescriptor);
	this.changed();
    }
    
	/**
	* Search my error pages for one with thei given signifier or null if there isn't one.
	*/
    public ErrorPageDescriptor getErrorPageDescriptorBySignifier(String signifier) {
	for (Iterator itr = this.getErrorPageDescriptorsSet().iterator(); itr.hasNext();) {
	    ErrorPageDescriptorImpl next = (ErrorPageDescriptorImpl) itr.next();
	    if (next.getErrorSignifierAsString().equals(signifier)) {
		return next;
	    }
	}
	return null;
    }
    
    /**
    * Returns the Set og my COntext Parameters.
    */
    public Set getContextParametersSet() {
	if (this.contextParameters == null) {
	    this.contextParameters = new OrderedSet();
	}
	return this.contextParameters = new OrderedSet(this.contextParameters);
    }
    
    /** Returns my COntext Parameters in an enumeration.
    */
    public Enumeration getContextParameters() {
	return (new Vector(this.getContextParametersSet())).elements();
    }
    
	/** Adds a new context parameter to my list.
	*/
    public void addContextParameter(ContextParameter contextParameter) {
	this.getContextParametersSet().add(contextParameter);
	this.changed();
    }
    
    /** Adds a new context parameter to my list.
    */
    public void addContextParameter(EnvironmentProperty contextParameter) {
        addContextParameter((ContextParameter) contextParameter);
    }    
    
	/**
	* Removes the given context parameter from my list.
	*/
    public void removeContextParameter(ContextParameter contextParameter) {
	this.getContextParametersSet().remove(contextParameter);
	this.changed();
    }
    
    /**
    * Return true if this web app [{0}] can be distributed across different processes.
    */
    
    public boolean isDistributable() {
	return isDistributable;
    }
    
     /**
    * Sets whether this web app [{0}] can be distributed across different processes.
    */
    public void setDistributable(boolean isDistributable) {
	this.isDistributable = isDistributable;
	this.changed();
    }
    
    /**
    * Returns the enumeration of my references to Enterprise Beans.
    */
	
    public Enumeration getEjbReferences() {
	return (new Vector(this.getEjbReferenceDescriptors())).elements();
    }
    /**
    * Returns the Set of my references to Enterprise Beans.
    */
    
    public Set getEjbReferenceDescriptors() {
	if (this.ejbReferences == null) {
	    this.ejbReferences = new OrderedSet();
	}
	return this.ejbReferences = new OrderedSet(this.ejbReferences);
    }
    
    /**
    * Returns an Enterprise Bean with the matching name or throw.
    */
    
    public EjbReferenceDescriptor getEjbReferenceByName(String name) {
        return (EjbReferenceDescriptor) getEjbReference(name);
    }
    
    public EjbReference getEjbReference(String name) {
	for (Iterator itr = this.getEjbReferenceDescriptors().iterator(); itr.hasNext();) {
	    EjbReference er = (EjbReference) itr.next();
	    if (er.getName().equals(name)) {
		return er;   
	    }
	}
	throw new IllegalArgumentException(localStrings.getLocalString(
							      "enterprise.deployment.exceptionwebapphasnoejbrefbyname",
							      "This web app [{0}] has no ejb reference by the name of [{1}] ", new Object[] {getName(), name}));
    }
    
     /**
    * Returns a reource reference with the matching name or throw.
    */
    
    public ResourceReferenceDescriptor getResourceReferenceByName(String name) {
	for (Iterator itr = this.getResourceReferenceDescriptors().iterator(); itr.hasNext();) {
	    ResourceReferenceDescriptor next = (ResourceReferenceDescriptor) itr.next();
	    if (next.getName().equals(name)) {
		return next;   
	    }
	}
	throw new IllegalArgumentException(localStrings.getLocalString(
							      "enterprise.deployment.exceptionwebapphasnoresourcerefbyname",
							      "This web app [{0}] has no resource reference by the name of [{1}]", new Object[] {getName(), name}));
    }
    
     /**
    * RReturns my Set of references to resources.
    */
    
    public Set getResourceReferenceDescriptors() {
	if (this.resourceReferences == null) {
	    this.resourceReferences = new OrderedSet();
	}
	return this.resourceReferences = new OrderedSet(this.resourceReferences);
    }
    
    public Set<EntityManagerFactoryReferenceDescriptor> 
        getEntityManagerFactoryReferenceDescriptors() {
        return entityManagerFactoryReferences;
    }

    /**
     * Return the entity manager factory reference descriptor corresponding to
     * the given name.
     */
    public EntityManagerFactoryReferenceDescriptor 
        getEntityManagerFactoryReferenceByName(String name) {
	for (EntityManagerFactoryReferenceDescriptor next :
             getEntityManagerFactoryReferenceDescriptors()) {

	    if (next.getName().equals(name)) {
		return next;
	    }
	}
        throw new IllegalArgumentException(localStrings.getLocalString(
            "exceptionwebapphasnoentitymgrfactoryrefbyname",
            "This web app [{0}] has no entity manager factory reference by the name of [{1}]",
            new Object[] {getName(), name}));
    }

    public void addEntityManagerFactoryReferenceDescriptor
        (EntityManagerFactoryReferenceDescriptor reference) { 
        reference.setReferringBundleDescriptor(this);
        this.getEntityManagerFactoryReferenceDescriptors().add(reference);
        this.changed();
    }

    public Set<EntityManagerReferenceDescriptor> 
        getEntityManagerReferenceDescriptors() {
        return entityManagerReferences;
    }

    /**
     * Return the entity manager factory reference descriptor corresponding to
     * the given name.
     */
    public EntityManagerReferenceDescriptor 
        getEntityManagerReferenceByName(String name) {
	for (EntityManagerReferenceDescriptor next :
             getEntityManagerReferenceDescriptors()) {

	    if (next.getName().equals(name)) {
		return next;
	    }
	}
        throw new IllegalArgumentException(localStrings.getLocalString(
            "exceptionwebapphasnoentitymgrrefbyname",
            "This web app [{0}] has no entity manager reference by the name of [{1}]",
            new Object[] {getName(), name}));
    }

    public void addEntityManagerReferenceDescriptor
        (EntityManagerReferenceDescriptor reference) { 
        reference.setReferringBundleDescriptor(this);
        this.getEntityManagerReferenceDescriptors().add(reference);
        this.changed();
    }

    /**
     * {@inheritDoc}
     */
    @Override public Collection<? extends PersistenceUnitDescriptor>
            findReferencedPUs() {
        Collection<PersistenceUnitDescriptor> pus =
                new HashSet<PersistenceUnitDescriptor>(
                        findReferencedPUsViaPURefs(this));
        pus.addAll(findReferencedPUsViaPCRefs(this));
        return pus;
    }

    /** Return my set of environment properties.
    */
    
    public Set getEnvironmentProperties() {
	return this.getEnvironmentEntrySet();
    }
    
    /** Adds a new reference to an ejb.*/
    
    public void addEjbReferenceDescriptor(EjbReference ejbReference) {
	this.getEjbReferenceDescriptors().add(ejbReference);        
        ejbReference.setReferringBundleDescriptor(this);        	
        this.changed();
    }
    
     /** Adds a new reference to an ejb.*/
    
    public void addEjbReferenceDescriptor(EjbReferenceDescriptor ejbReferenceDescriptor) {
        ejbReferenceDescriptor.setReferringBundleDescriptor(this);
	this.addEjbReferenceDescriptor((EjbReference) ejbReferenceDescriptor);

    }
    
     /** Removes a reference to an ejb.*/
    public void removeEjbReferenceDescriptor(EjbReferenceDescriptor ejbReferenceDescriptor) {
	this.removeEjbReferenceDescriptor((EjbReference) ejbReferenceDescriptor);
    }
    
    public void removeEjbReferenceDescriptor(EjbReference ejbReferenceDescriptor) {    
	this.getEjbReferenceDescriptors().remove(ejbReferenceDescriptor);
        ejbReferenceDescriptor.setReferringBundleDescriptor(null);
	this.changed();        
    }
	
	/** Return an enumeration of references to resources that I have.*/
    public Enumeration getResourceReferences() {
	return (new Vector(this.getResourceReferenceDescriptors())).elements();
    }
    
    /** adds a new reference to a resource.*/
    public void addResourceReference(ResourceReference resourceReference) {
	this.getResourceReferenceDescriptors().add(resourceReference);
	this.changed();
    }
    /** adds a new reference to a resource.*/
    public void addResourceReferenceDescriptor(ResourceReferenceDescriptor resourceReference) {
	this.addResourceReference((ResourceReference) resourceReference);
    }
    /** removes a reference to a resource.*/
    public void removeResourceReferenceDescriptor(ResourceReferenceDescriptor resourceReference) {
	this.getResourceReferenceDescriptors().remove(resourceReference);
	this.changed();
    }

    public Set getMessageDestinationReferenceDescriptors() {
        if( this.messageDestReferences == null ) {
            this.messageDestReferences = new OrderedSet();
        }
        return this.messageDestReferences = 
            new OrderedSet(this.messageDestReferences);
    }

    public void addMessageDestinationReferenceDescriptor
        (MessageDestinationReferenceDescriptor messageDestRef) { 
        messageDestRef.setReferringBundleDescriptor(this);
        this.getMessageDestinationReferenceDescriptors().add(messageDestRef);
        this.changed();
    }

    public void removeMessageDestinationReferenceDescriptor
        (MessageDestinationReferenceDescriptor msgDestRef) {
        this.getMessageDestinationReferenceDescriptors().remove(msgDestRef);
        this.changed();
    }
    
    /**
     * Looks up an message destination reference with the given name. 
     * Throws an IllegalArgumentException if it is not found.
     */
    public MessageDestinationReferenceDescriptor 
        getMessageDestinationReferenceByName(String name) {
	for (Iterator itr = 
                 this.getMessageDestinationReferenceDescriptors().iterator(); 
             itr.hasNext();) {
	    MessageDestinationReferenceDescriptor mdr = 
                (MessageDestinationReferenceDescriptor) itr.next();
	    if (mdr.getName().equals(name)) {
		return mdr;
	    }
	}
        throw new IllegalArgumentException(localStrings.getLocalString(
                "exceptionwebapphasnomsgdestrefbyname",
                "This web app [{0}] has no message destination reference by the name of [{1}]",
                new Object[] {getName(), name}));
    }

    public Set<LifecycleCallbackDescriptor>
        getPostConstructDescriptors() {
        return postConstructDescs;
    }

    public void addPostConstructDescriptor(LifecycleCallbackDescriptor
        postConstructDesc) {
        String className = postConstructDesc.getLifecycleCallbackClass();
        boolean found = false;
        for (LifecycleCallbackDescriptor next :
             getPostConstructDescriptors()) {
            if (next.getLifecycleCallbackClass().equals(className)) {
                found = true;
                break;
            }
        }
        if (!found) {
            getPostConstructDescriptors().add(postConstructDesc);
        }
    }

    public LifecycleCallbackDescriptor
        getPostConstructDescriptorByClass(String className) {
        return getPostConstructDescriptorByClass(className, this);
    }

    public Set<LifecycleCallbackDescriptor>
        getPreDestroyDescriptors() {
        return preDestroyDescs;
    }

    public void addPreDestroyDescriptor(LifecycleCallbackDescriptor
        preDestroyDesc) {
        String className = preDestroyDesc.getLifecycleCallbackClass();
        boolean found = false;
        for (LifecycleCallbackDescriptor next :
             getPreDestroyDescriptors()) {
            if (next.getLifecycleCallbackClass().equals(className)) {
                found = true;
                break;
            }
        }
        if (!found) {
            getPreDestroyDescriptors().add(preDestroyDesc);
        }
    }

    public LifecycleCallbackDescriptor
        getPreDestroyDescriptorByClass(String className) {
        return getPreDestroyDescriptorByClass(className, this);
    }

    protected List<InjectionCapable>
        getInjectableResourcesByClass(String className,
                                      JndiNameEnvironment jndiNameEnv) {
        List<InjectionCapable> injectables = 
            new LinkedList<InjectionCapable>();

        for(InjectionCapable next : getInjectableResources(jndiNameEnv) ) {
            if( next.isInjectable()) {
                for (InjectionTarget target : next.getInjectionTargets()) {
                    if (target.getClassName().equals(className) ) {
                        injectables.add(next);
                    }
                }
            }
        }
        
        if(((WebBundleDescriptor)jndiNameEnv).hasWebServices()) {
            // Add @Resource WebServiceContext present in endpoint impl class to the list of
            // injectable resources; We do this for servelt endpoint only because the actual 
            // endpoint impl class gets replaced by JAXWSServlet in web.xml and hence
            // will never be added as an injectable resource
            for(InjectionCapable next : getInjectableResources(this) ) {
                if( next.isInjectable()) {
                    for (InjectionTarget target : next.getInjectionTargets()) {
                        Iterator<WebServiceEndpoint> epIter = getWebServices().getEndpoints().iterator();
                        while(epIter.hasNext()) {
                            String servletImplClass = epIter.next().getServletImplClass();
                            if (target.getClassName().equals(servletImplClass) ) {
                                injectables.add(next);
                            }
                        }
                    }
                }
            }
        }
        return injectables;
    }
    
    public List<InjectionCapable> 
        getInjectableResourcesByClass(String className) {
        return(getInjectableResourcesByClass(className, this));
    }

    public InjectionInfo getInjectionInfoByClass(Class clazz) {
        return(getInjectionInfoByClass(clazz, this));
    }

	/** Returns an Enumeration of my SecurityRole objects. */
    public Enumeration getSecurityRoles() {
	Vector securityRoles = new Vector();
	for (Iterator itr = super.getRoles().iterator(); itr.hasNext();) {
	    Role r = (Role) itr.next();
	    SecurityRoleDescriptor srd = new SecurityRoleDescriptor(r);
	    securityRoles.add(srd);
	}
	return securityRoles.elements();
    }
    
    /** Add a new abstrct role to me. */
    public void addSecurityRole(SecurityRole securityRole) {
	Role r = new Role(securityRole.getName());
	r.setDescription(securityRole.getDescription());
	super.addRole(r);
    }
    /** Add a new abstrct role to me. */
    public void addSecurityRole(SecurityRoleDescriptor securityRole) {    
        addSecurityRole((SecurityRole) securityRole);
    }
    
    /** Return all the references by a given component (by name) to the given rolename. */
    public SecurityRoleReference getSecurityRoleReferenceByName(String compName, String roleName) {
	    for(Enumeration e = this.getWebComponentDescriptors(); e.hasMoreElements();){
		WebComponentDescriptor comp = (WebComponentDescriptor) e.nextElement();
		if(!comp.getCanonicalName().equals(compName))
		    continue;

		SecurityRoleReference r = comp.getSecurityRoleReferenceByName(roleName);
		if(r != null)
		    return r;
	    }

	    return null;
    }

    
    private Set getSecurityConstraintsSet() {
	if (this.securityConstraints == null) {
	    this.securityConstraints = new HashSet();
	}
	return this.securityConstraints;
    }
    
    /** My list of security constraints.*/
    public Enumeration getSecurityConstraints() {
	return (new Vector(this.getSecurityConstraintsSet())).elements();
    }

    public Collection getSecurityConstraintsForUrlPattern(String urlPattern) {
        Collection constraints = new HashSet();
        for(Iterator i = getSecurityConstraintsSet().iterator(); i.hasNext();) {
            SecurityConstraint next = (SecurityConstraint) i.next();
            boolean include = false;
            for(Enumeration wrc = next.getWebResourceCollections(); 
                wrc.hasMoreElements();) {
                WebResourceCollection nextCol = (WebResourceCollection) 
                    wrc.nextElement();
                for(Enumeration up = nextCol.getUrlPatterns();
                    up.hasMoreElements();) {
                    String nextPattern = (String) up.nextElement();
                    if((urlPattern != null) && urlPattern.equals(nextPattern)) {
                        include = true;
                        break;
                    }
                }
                if( include ) { break; }
            }
            if( include ) { constraints.add(next); }
        }
        return constraints;
    }

    /** Add a new security constraint.*/
    public void addSecurityConstraint(SecurityConstraint securityConstraint) {
	this.getSecurityConstraintsSet().add(securityConstraint);
	this.changed();
    }
    
    /** Add a new security constraint.*/
    public void addSecurityConstraint(SecurityConstraintImpl securityConstraint) {
	addSecurityConstraint((SecurityConstraint )securityConstraint);
    }
    
     /** Remove the given security constraint.*/
    public void removeSecurityConstraint(SecurityConstraint securityConstraint) {
	this.getSecurityConstraintsSet().remove(securityConstraint);
	this.changed();
    }
    
    
    
    /** Unused. */
    public Enumeration getLocalizedContentDescriptors() {
	return (new Vector()).elements();
    }

    public JspConfigDescriptor getJspConfigDescriptor() {
	return jspConfigDescriptor;
    }

    public void setJspConfigDescriptor(JspConfigDescriptor jspC) {
	if (jspConfigDescriptor != null) {
            jspConfigDescriptor.removeNotificationListener(this);
	}
	jspConfigDescriptor = jspC;
	if (jspC != null) {
            jspC.addNotificationListener(this);
	}
    }

    /* ----
    */

    public Set getServletDescriptors() {
	Set servletDescriptors = new HashSet();
	for (Iterator itr = this.getWebComponentDescriptorsSet().iterator(); itr.hasNext();) {
	    WebComponentDescriptor next = (WebComponentDescriptor) itr.next();
            if (next.isServlet()) {
		servletDescriptors.add(next);
	    }
	}
	return servletDescriptors;
    }
    
    /** Return my Set of jsps. */
    public Set getJspDescriptors() {
	Set jspDescriptors = new HashSet();
	for (Iterator itr = this.getWebComponentDescriptorsSet().iterator(); itr.hasNext();) {
	    WebComponentDescriptor next = (WebComponentDescriptor) itr.next();
            if (!next.isServlet()) {
		jspDescriptors.add(next);
	    }
	}
	return jspDescriptors;
    }
    
    private Set getEnvironmentEntrySet() {
	if (this.environmentEntries == null) {
	    this.environmentEntries = new OrderedSet();
	}
	return this.environmentEntries = new OrderedSet(environmentEntries);
    }
    
    /** Return my set of environment properties.*/
    public Enumeration getEnvironmentEntries() {
	return (new Vector(this.getEnvironmentEntrySet())).elements();
    }
    
    /** Adds this given environment property to my list.*/
    public void addEnvironmentEntry(EnvironmentEntry environmentEntry) {
	this.getEnvironmentEntrySet().add(environmentEntry);
	this.changed();
    }
    
    /** 
     * Returns the environment property object searching on the supplied key.
     * throws an illegal argument exception if no such environment property exists.
     */
    public EnvironmentProperty getEnvironmentPropertyByName(String name) {
	for (Iterator itr = this.getEnvironmentEntrySet().iterator(); 
             itr.hasNext();) {
	    EnvironmentProperty ev = (EnvironmentProperty) itr.next();
	    if (ev.getName().equals(name)) {
		return ev;   
	    }
	}
	throw new IllegalArgumentException(localStrings.getLocalString(
		"enterprise.deployment.exceptionwebapphasnoenvpropertybyname",
		"This web app [{0}] has no environment property by the name of [{1}]", 
                new Object[] {getName(), name}));
    }

    /** Removes this given environment property from my list.*/
    public void removeEnvironmentProperty(EnvironmentProperty environmentProperty) {
	this.getEnvironmentEntrySet().remove(environmentProperty);
	this.changed();
    }
    
    /** Adds this given environment property to my list.*/
    public void addEnvironmentProperty(EnvironmentProperty environmentProperty) {
	this.getEnvironmentEntrySet().add(environmentProperty);
	this.changed();
    }
    
    /** Removes this given environment property from my list.*/
    public void removeEnvironmentEntry(EnvironmentEntry environmentEntry) {
	this.getEnvironmentEntrySet().remove(environmentEntry);
	this.changed();
    }
    
    /** Return the information about how I should log in.*/
    public LoginConfiguration getLoginConfiguration() {
	return this.loginConfiguration;
    }
    
    /** Specifies the information about how I should log in.*/
    public void setLoginConfiguration(LoginConfiguration loginConfiguration) {
	this.loginConfiguration = loginConfiguration;
	this.changed();
    }

    public void setLoginConfiguration(LoginConfigurationImpl loginConfiguration) {
        setLoginConfiguration((LoginConfiguration) loginConfiguration);
    }

    /** Search for a web component that I have by name.*/
    public WebComponentDescriptor getWebComponentByName(String name) {
	for (Iterator itr = this.getWebComponentDescriptorsSet().iterator(); itr.hasNext();) {
	    Descriptor next = (Descriptor) itr.next();
	    if (next.getName().equals(name)) {
		return (WebComponentDescriptor) next;
	    }
	}
	return null;
    }

    /** Search for a web component that I have by name.*/
    public WebComponentDescriptor getWebComponentByCanonicalName(String name) {
	for (Iterator itr = this.getWebComponentDescriptorsSet().iterator(); itr.hasNext();) {
	    WebComponentDescriptor next = (WebComponentDescriptor) itr.next();
	    if (next.getCanonicalName().equals(name)) {
		return (WebComponentDescriptor) next;
	    }
	}
	return null;
    }

    /**
     * @return a set of web component descriptor of given impl name.
     */
    public WebComponentDescriptor[] getWebComponentByImplName(String name) {
        ArrayList<WebComponentDescriptor> webCompList =
                new ArrayList<WebComponentDescriptor>();
	for (Object webCompObj : this.getWebDescriptors()) {
            WebComponentDescriptor webComp = (WebComponentDescriptor)webCompObj;
            if (webComp.getWebComponentImplementation().equals(name)) {
                webCompList.add(webComp);
            }
        }
        return webCompList.toArray(new WebComponentDescriptor[webCompList.size()]);
    }
    
    /* ----
    */

    /**
    * Return a Vector of servlet filters that I have.
    */
    public Vector getServletFilters() { 
	if (this.servletFilters == null) {
	    this.servletFilters = new Vector();
	}
	return this.servletFilters;
    }

    /**
    * Return a Vector of servlet filters that I have.
    */
    public Vector getServletFilterDescriptors() { 
	return (Vector)this.getServletFilters().clone(); 
    }

    /**
    * Adds a servlet filter to this web component.
    */
    public void addServletFilter(ServletFilter ref) {
	if (!this.getServletFilters().contains(ref)) {
	    this.getServletFilters().addElement(ref);
	    this.changed();
	}
    }	
    
    public void addServletFilter(ServletFilterDescriptor ref) {
        addServletFilter((ServletFilter) ref);
    }

    /** 
    * Removes the given servlet filter from this web component.
    */
    public void removeServletFilter(ServletFilter ref) {
	this.removeVectorItem(this.getServletFilters(), ref);
	//this.getServletFilters().removeElement(ref); ** is based on 'equals()'
	this.changed();
    }

    /* ----
    */

    /**
    * Return a Vector of servlet filters that I have.
    */
    public Vector getServletFilterMappings() { 
	if (this.servletFilterMappings == null) {
	    this.servletFilterMappings = new Vector();
	}
	return this.servletFilterMappings;
    }

    /**
    * Return a Vector of servlet filter mappings that I have.
    */
    public Vector getServletFilterMappingDescriptors() { 
	return (Vector)this.getServletFilterMappings().clone(); 
    }

    /**
    * Adds a servlet filter mapping to this web component.
    */
    public void addServletFilterMapping(ServletFilterMapping ref) {
	if (!this.getServletFilterMappings().contains(ref)) {
	    this.getServletFilterMappings().addElement(ref);
	    this.changed();
	}
    }
    
    /**
    * Adds a servlet filter mapping to this web component.
    */
    public void addServletFilterMapping(ServletFilterMappingDescriptor ref) {
        addServletFilterMapping((ServletFilterMapping) ref);
    }    
	
    /** 
    * Removes the given servlet filter mapping from this web component.
    */
    public void removeServletFilterMapping(ServletFilterMapping ref) {
	this.removeVectorItem(this.getServletFilterMappings(), ref);
	//this.getServletFilterMappings().removeElement(ref);
	this.changed();
    }
    /** 
    ** Moves the given servlet filter mapping to a new relative location in 
    ** the list
    */
    public void moveServletFilterMapping(ServletFilterMapping ref, int relPos) {
	this.moveVectorItem(this.getServletFilterMappings(), ref, relPos);
	this.changed();
    }

    /* ----
    */
    
    private Vector getAppListeners() {
	if (this.appListenerDescriptors == null) {
	    this.appListenerDescriptors = new Vector();
	}
	return this.appListenerDescriptors;
    }
    
    public Vector getAppListenerDescriptors() {
	return (Vector)this.getAppListeners().clone(); 
    }

    public void setAppListeners(Collection c) { 
	this.getAppListeners().clear();
	this.getAppListeners().addAll(c);
	this.changed();
    }

    public void addAppListenerDescriptor(AppListenerDescriptor ref) {
	if (!this.getAppListeners().contains(ref)) {
	    this.getAppListeners().addElement(ref);
	    this.changed();
	}
    }	
    
    public void addAppListenerDescriptor(AppListenerDescriptorImpl ref) {    
        addAppListenerDescriptor((AppListenerDescriptor) ref);
    }

    public void removeAppListenerDescriptor(AppListenerDescriptor ref) {
	this.removeVectorItem(this.getAppListeners(), ref);
	//this.getAppListeners().removeElement(ref);
	this.changed();
    }

    public void moveAppListenerDescriptor(AppListenerDescriptor ref, 
	int relPos) {
	this.moveVectorItem(this.getAppListeners(), ref, relPos);
	this.changed();
    }

    /**
     * @return true if this bundle descriptor defines web service clients
     */
    public boolean hasWebServiceClients() {
        return !getServiceReferenceDescriptors().isEmpty();
    }         
    
    /**
     * End of Web-Services related API
     */
    
    /* ----
    */

    /** 
    * remove a specific object from the given list (does not rely on 'equals')
    */
    protected boolean removeVectorItem(Vector list, Object ref) {
	for (Iterator i = list.iterator(); i.hasNext();) {
	    if (ref == i.next()) {
		i.remove();
		return true;
	    }
	}
	return false;
    }

    /** 
    * Moves the given object to a new relative location in the specified list
    */
    protected void moveVectorItem(Vector list, Object ref, int rpos) {

	/* get current position of ref */
	// 'indexOf' is not used because it is base on 'equals()' which may
	// not be unique.
	int size = list.size(), old_pos = size - 1;
	for (;old_pos >= 0; old_pos--) {
	    if (ref == list.elementAt(old_pos)) {
		break;
	    }
	}
	if (old_pos < 0) {
	    return; // not found
	}

	/* limit up/down movement */
	int new_pos = old_pos + rpos;
	if (new_pos < 0) {
	    new_pos = 0; // limit movement
	} else
	if (new_pos >= size) {
	    new_pos = size - 1; // limit movement
	}

	/* is it really moving? */
	if (new_pos == old_pos) {
	    return; // it's not moving
	}	
	
	/* move it */
	list.removeElementAt(old_pos);
	list.insertElementAt(ref, new_pos);
	this.changed();

    }
    
    /**
     * visitor API implementation
     */
    public void visit(DescriptorVisitor aVisitor) {
        if (aVisitor instanceof WebBundleVisitor) {
            visit((WebBundleVisitor) aVisitor);
        } else {
            super.visit(aVisitor);
        }
    }
    
    /**
     * visitor API implementation
     */
    public void visit(WebBundleVisitor aVisitor) {
        super.visit(aVisitor);
        aVisitor.accept(this);

        // Visit all injectables first.  In some cases, basic type information
        // has to be derived from target inject method or inject field.
        for(InjectionCapable injectable : getInjectableResources(this)) {
            aVisitor.accept(injectable);
        }

        for (Iterator i=getWebComponentDescriptorsSet().iterator();i.hasNext();) {
            WebComponentDescriptor aWebComp = (WebComponentDescriptor) i.next();
            aVisitor.accept(aWebComp);
        }
        for (Iterator itr=getWebServices().getWebServices().iterator();
             itr.hasNext(); ) {
            WebService aWebService = (WebService) itr.next();
            aVisitor.accept(aWebService);
        }

        for (Iterator itr = getEjbReferenceDescriptors().iterator();itr.hasNext();) {
            EjbReference aRef = (EjbReference) itr.next();
            aVisitor.accept(aRef);
        }
        for (Iterator itr=getResourceReferenceDescriptors().iterator();
             itr.hasNext();) {
            ResourceReferenceDescriptor next = 
                (ResourceReferenceDescriptor) itr.next();
            aVisitor.accept(next);
        }
        for (Iterator itr=getJmsDestinationReferenceDescriptors().iterator();
             itr.hasNext();) {
            JmsDestinationReferenceDescriptor next = 
                (JmsDestinationReferenceDescriptor) itr.next();
            aVisitor.accept(next);
        }
        for (Iterator itr=getMessageDestinationReferenceDescriptors().iterator();
             itr.hasNext();) {
            MessageDestinationReferencer next = 
                (MessageDestinationReferencer) itr.next();
            aVisitor.accept(next);
        }
        for (Iterator itr = getMessageDestinations().iterator();
                itr.hasNext();) {
            MessageDestinationDescriptor msgDestDescriptor =
                (MessageDestinationDescriptor)itr.next();
            aVisitor.accept(msgDestDescriptor);
        }
        for (Iterator itr=getServiceReferenceDescriptors().iterator();
             itr.hasNext();) {
            aVisitor.accept((ServiceReferenceDescriptor) itr.next());
        }
    }

    /* ----
    */

    public void notification(NotificationEvent ne) {
	// currently, notifications come only from the WebServiceBundleDescriptor
	this.changed();
    }

    /* ----
    */

    /** Return a formatted version as a String. */
    public void print(StringBuffer toStringBuffer) {
	toStringBuffer.append("\nWeb Bundle descriptor");
	toStringBuffer.append("\n");
        super.print(toStringBuffer);
        toStringBuffer.append( "\n context root ").append(getContextRoot());
	toStringBuffer.append( "\n sessionTimeout ").append(sessionTimeout);
	toStringBuffer.append( "\n mimeMappings ").append(mimeMappings);
	toStringBuffer.append( "\n welcomeFiles ").append(welcomeFiles);
	toStringBuffer.append( "\n errorPageDescriptors ").append(errorPageDescriptors);
	toStringBuffer.append( "\n appListenerDescriptors ").append(appListenerDescriptors);
	toStringBuffer.append( "\n contextParameters ").append(contextParameters);
	toStringBuffer.append( "\n ejbReferences ");
        if(ejbReferences != null)
            printDescriptorSet(ejbReferences,toStringBuffer);
        toStringBuffer.append( "\n jmsDestReferences ");
        if(jmsDestReferences != null)
            printDescriptorSet(jmsDestReferences,toStringBuffer);
        toStringBuffer.append( "\n messageDestReferences ");
        if(messageDestReferences != null)
            printDescriptorSet(messageDestReferences,toStringBuffer);
	toStringBuffer.append( "\n resourceReferences ");
        if(resourceReferences != null)
            printDescriptorSet(resourceReferences,toStringBuffer);
	toStringBuffer.append( "\n serviceReferences ");
        if(serviceReferences != null)
            printDescriptorSet(serviceReferences,toStringBuffer);
	toStringBuffer.append( "\n isDistributable ").append(isDistributable);
	toStringBuffer.append( "\n securityRoles ").append(securityRoles);
	toStringBuffer.append( "\n securityConstraints ").append(securityConstraints);
	toStringBuffer.append( "\n contextRoot ").append(contextRoot);
	toStringBuffer.append( "\n loginConfiguration ").append(this.loginConfiguration);
	toStringBuffer.append( "\n webComponentDescriptors ");
        if(webComponentDescriptors != null)
            printDescriptorSet(webComponentDescriptors,toStringBuffer);
	toStringBuffer.append( "\n environmentEntries ");
        if(environmentEntries != null)
            printDescriptorSet(environmentEntries,toStringBuffer);       
	if (sunWebApp!=null) {
	    toStringBuffer.append( "\n ========== Runtime Descriptors =========");
	    toStringBuffer.append( "\n").append(sunWebApp.toString());
	}
    }
    private void printDescriptorSet(Set descSet, StringBuffer sbuf){
        if (descSet==null) 
            return;
        for(Iterator itr = descSet.iterator(); itr.hasNext();){
            Object obj = itr.next();
            if(obj instanceof Descriptor)
                ((Descriptor)obj).print(sbuf);
            else
                sbuf.append(obj);
        }
    }  
    
    /**
     * @return the module type for this bundle descriptor
     */
    public ModuleType getModuleType() {
        return ModuleType.WAR;
    }
    
    /**
     * @return the deployment descriptor directory location inside
     * the archive file
     */
    public String getDeploymentDescriptorDir() {
        return DEPLOYMENT_DESCRIPTOR_DIR;
    }    

        /***********************************************************************************************
     * START
     * Deployment Consolidation to Suppport Multiple Deployment API Clients
     * Methods: setSunDescriptor, getSunDescriptor
     ***********************************************************************************************/
    
    /** 
     * This returns the extra web sun specific info not in the RI DID.
     *
     * @return   object representation of web deployment descriptor 
     */
    public SunWebApp getSunDescriptor(){
	if (sunWebApp==null) {
	    sunWebApp = new SunWebApp();
	}
        return sunWebApp;
    }
    
    /** 
     * This sets the extra web sun specific info not in the RI DID.
     * 
     * @param    webApp  SunWebApp object representation of web deployment descriptor
     */
    public void setSunDescriptor(SunWebApp webApp){
        this.sunWebApp = webApp;
    }
   
    /*******************************************************************************************
     * END
     * Deployment Consolidation to Suppport Multiple Deployment API Clients
     *******************************************************************************************/ 
    
    public void copyWebBundleDescriptor(WebBundleDescriptor webBundleDescriptor) {
        addWebBundleDescriptor(webBundleDescriptor);
        // Added changes for Extension Deployment SPI
        // XXX : Need to confirm if this is the best way to achieve a combined
        // descriptor
        
        // add EntityManagerFactory and EntityManager references
        for(Iterator iter = webBundleDescriptor.
                getEntityManagerFactoryReferenceDescriptors().iterator(); iter.hasNext();) {
        this.addEntityManagerFactoryReferenceDescriptor(
                    (EntityManagerFactoryReferenceDescriptor)iter.next());
        }
        for(Iterator iter = webBundleDescriptor.
                getEntityManagerReferenceDescriptors().iterator(); iter.hasNext();) {
        this.addEntityManagerReferenceDescriptor(
                     (EntityManagerReferenceDescriptor)iter.next());
        }
        
        // add PU References
        for (PersistenceUnitsDescriptor pu : 
                   webBundleDescriptor.getPersistenceUnitsDescriptors()) {
            String puRoot = pu.getPuRoot();
            this.addPersistenceUnitsDescriptor(puRoot, pu);                             
        }
                  
        this.setModuleDescriptor(webBundleDescriptor.getModuleDescriptor());
        this.getJmsDestinationReferenceDescriptors().addAll(
                   webBundleDescriptor.getJmsDestinationReferenceDescriptors());
        this.getPreDestroyDescriptors().addAll(webBundleDescriptor.getPreDestroyDescriptors());
        this.getPostConstructDescriptors().addAll(webBundleDescriptor.getPostConstructDescriptors());
        
        this.setSunDescriptor(webBundleDescriptor.getSunDescriptor());
        this.setContextRoot(webBundleDescriptor.getContextRoot());

        this.isDistributable = webBundleDescriptor.isDistributable;

        this.changed();
    }
}
    

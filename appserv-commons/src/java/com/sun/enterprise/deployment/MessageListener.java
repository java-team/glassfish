/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
 package com.sun.enterprise.deployment;

import java.util.*;
import java.util.jar.*;
import java.io.*;
import java.util.zip.*;
import com.sun.enterprise.deployment.EnvironmentProperty;

/**
 * connector1.5
 * <!ELEMENT messagelistener (messagelistener-type, 
 *           activationspec)>
 *
 * @author Sheetal Vartak
 */
public class MessageListener extends Descriptor {

    
    private boolean isDirty = false;

    private String msgListenerType;
    private String activationSpecClass;
    private Set configProperties;

    //default constructor
    public MessageListener() {
	this.configProperties = new OrderedSet();
    }

   

    public String getMessageListenerType() {
	return msgListenerType;
    }

    public void setMessageListenerType(String msgListenerType) {
	this.msgListenerType = msgListenerType;
    }

    public String getActivationSpecClass() {
	return activationSpecClass;
    }

    public void setActivationSpecClass(String activationSpecClass) {
	this.activationSpecClass = activationSpecClass;
    }

     
    /** add a configProperty to the set
     */
    public void addConfigProperty(EnvironmentProperty configProperty) {
	this.configProperties.add(configProperty);
        this.setDirty();
        this.changed();
    }
    
    /** remove a configProperty from the set
     */ 
    public void removeConfigProperty(EnvironmentProperty configProperty) {
	this.configProperties.remove(configProperty);
        this.setDirty();
        this.changed();
    }

    /** Set of EnvironmentProperty 
     */
    public Set getConfigProperties() {
        return configProperties;
    }

    //return the msg listener name
    //FIXME.  No longer valid.  Use messagelistener-type instead of name
    public String getMessageListenerName() {
        throw new UnsupportedOperationException();
    }

    //set the msg listener name
    //FIXME.  No longer valid.  Use messagelistener-type instead of name
    public void setMessageListenerName(String msgListenerName) {
        throw new UnsupportedOperationException();
    }
  
    //////////////////////////////////////////
    //misc. methods for denoting change in data
    /////////////////////////////////////////
    public void changed() {
	super.changed();
    }    
        
    /**	
     * A flag to indicate that my data has changed since the last save.
     */
    public boolean isDirty() {
	return this.isDirty;
    }

    private void setDirty() {
        this.isDirty = true;
    }
    
    void doneOpening() {
	this.isDirty = false;
	this.changed();
    }

    void doneSaving() {
	this.isDirty = false;
	this.changed();
    }

    /** override 'setName' to set 'dirty' flag
    */
    public void setName(String name) {
	if (!this.getName().equals(name)) {
	    this.setDirty();
	    super.setName(name);
	}
    }
    
  }

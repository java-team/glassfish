/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * ArchiveLoader.java
 *
 * Created on April 5, 2007, 5:27 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sun.enterprise.deployment.interfaces.pluggable;

/**
 * This implementation of this interface would be used for loading/unloading 
 * of ExtensionModules.The user can decide to use the implementation of this 
 * interface to load/unload modules or decide to use a custom loader as
 * required by the target container.
 * During load on deployment or startup 
 * {@link #load(ArchiveDescriptor)} will then be called to load the
 *    module to the container.
 * 
 * During unload on undeployment or shutdown of the container
 * {@link #unload(ArchiveDescriptor)} will be called to unload the module
 * from the container
 * 
 * @author Prasad Subramanaian
 */

import java.util.Properties;

public interface ArchiveLoader {
    
   
    /**
     * This  method is responsible for loading all components of the
     * module to the container.
     * It will be invoked during deployment time and server start 
     * up time.
     *
     * @param descriptor the descriptor object which contains all the
     *                   metadata information and with the module
     *                   classloader set on it
     * @param isEnableAction describes if this is a module enable operation
     * @return whether the load operation is successful
     */
    boolean load(ArchiveDescriptor descriptor, boolean isEnableAction) 
                                                             throws Exception;

    /**
     * This  method is responsible for loading all components of the
     * module to the container.
     * It will be invoked during deployment time and server start
     * up time.
     *
     * @param descriptor the descriptor object which contains all the
     *                   metadata information and with the module
     *                   classloader set on it
     * @param isEnableAction describes if this is a module enable operation
     * @param props list of properties passed in while deployment
     * @return whether the load operation is successful
     */
    boolean load(ArchiveDescriptor descriptor, boolean isEnableAction,
                                               Properties props)
                                                             throws Exception;


    /**
     * This method is responsible for unloading all components of the 
     * module from the container.
     * It will be invoked during undeployment time.
     *
     * @param descriptor the descriptor object which contains all the
     *                   metadata information and with the module
     *                   classloader set on it
     * @param isDisableAction describes if this is a module disable operation
     *
     * @return whether the unload operation is successful
     */
    boolean unload(ArchiveDescriptor descriptor, boolean isDisableAction) 
                                                              throws Exception;
    
}

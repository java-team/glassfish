/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.interfaces.pluggable;

import java.io.File;
import java.io.IOException;
import org.xml.sax.SAXParseException;

import javax.enterprise.deploy.shared.ModuleType;
import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.Descriptor;
import com.sun.enterprise.deployment.BundleDescriptor;
import com.sun.enterprise.deployment.io.DeploymentDescriptorFile;


/**
 * The ArchiveDeployer manages the lifecycle of a module. 
 *
 * <p>For the deployment scenario, the following will be the order of which 
 * the methods will be invoked.<br>
 * 1. {@link #expand(File, File)} will be first called to expand the archive 
 *    to the specified module root directory if this is an archive deployment.
 *    <br>
 * 2. {@link #prepare(File, File, ClassLoader)} will be then called to 
 *    create a module classloader, process the metadata inside the archive, 
 *    and generate any necessary artifacts. It will return back an 
 *    ArchiveDescriptor object encapsulating all the processed
      metadata information and with the module classloader set on it.<br>
 *
 * <p>For the undeployment scenario, the following will be the order of which
 * the methods will be invoked.<br>
 * 1. {@link #cleanup(File)} will be then called to clean up the 
 *    module directory.
 * 
 * <p>For the server start up scenario, the following will be the order of 
 * which the methods will be invoked to load up the module. <br>
 * 1. {@link #prepare(File, File, ClassLoader)} will be first called to 
 *    create a module classloader, process the metadata inside the 
 *    archive and generate any necessary artifacts. It will return back an 
 *    ArchiveDescriptor object encapsulating all the processed
      metadata information and with the module classloader set on it.<br>
 *
 * <p>The loading and unloading of the modules to and from the container 
   shall be implemented by an implementation of the ArchiveLoader interface
   or a custom loader </p>
 */
public interface ArchiveDeployer {
    /**
     * This SPI is responsible for expanding the module.
     * It will be invoked during deployment time and only for archive
     * deployment.
     *
     * @param archivePath the original path of the module, point to the 
     *                    (uploaded) archive file
     * @param moduleRootDirectory the archive must be expanded to 
     */
    void expand(File archivePath, File moduleRootDirectory) throws Exception;

    /**
     * This SPI is responsible for preparing the module for the loading.
     * It will be invoked during deployment time and server start 
     * up time.<br>
     *
     * 1. It should first construct a module classloader using the passed-in 
     *    parent classloader.<br> 
     * 2. It should then process the metadata (deployment descriptors, 
     *    annotations etc).<br>
     * 3. It should then generate any necessary artifacts and store in
     *    the specified moduleScratchDirectory.<br>
     *
     * <p>The returned archive descriptor should contain all the processed
     * metadata information and with the module classloader set on it.
     *
     * @param moduleRootDirectory the module root directory 
     * @param moduleScratchDirectory the directory where the generated 
     *                               artificacts could be stored
     * @param parentClassLoader the parent classloader which contains
     *                          necessary glassfish server classpaths    
     * @return the descriptor which contains all the processed metadata 
     *         and with the module class loader set on it
     */
    ArchiveDescriptor prepare(File moduleRootDirectory, 
        File moduleScratchDirectory, ClassLoader parentClassLoader, 
            boolean isStartup) throws Exception; 

 
    void generatePolicy(ArchiveDescriptor desciptor)throws Exception; 
    void removePolicy(String appName)throws Exception; 

    /**
     * This SPI is responsible for the necessary clean up of undeployment.
     * For example, remove the expanded directory of previous deployment.
     * It will be invoked during undeployment time.
     *
     * @param  moduleRootDirectory the root directory of the module
     */
    void cleanup(File moduleRootDirectory) throws Exception;

    /**
     * This SPI is to determine whether this archive deployer can handle
     * the particular archive file.
     *
     * @param archiveFile path pointing to the archive file, could be a jar
     *                    or directory file
     * @return true if the archive deployer knows how to handle this archive
     */
    boolean handles(AbstractArchive archiveFile) throws IOException;

    /**
     * @return the description of the module that could be displayed by tool
     */
    String getModuleDescription();
    
    /**
     * @return  the standard deployment descriptor for the Archivist if any
     */
    DeploymentDescriptorFile getStandardDDFile();
    
    /**
     * @return the configuration deployment descriptor for the Archivist if any
     */
    DeploymentDescriptorFile getConfigurationDDFile();
    
    /**
     * @return the default Deployment Descriptor object
     */
    Descriptor getDefaultBundleDescriptor();
    
    /**
     *@return the module type that this ArchiveDeployer handles
     */
    ModuleType getModuleType();
    
    /*
     * Setter for the module descriptor
     */
    void setDescriptor(Descriptor descriptor);
    
    /**
     * Getter method for Module Descriptor
     */
    Descriptor getDescriptor();

    /**
     * Setter method for a flag that indicates if this application is a part
     *of a Java EE Application
     * @param isJ2ee flag which is set to true if the module is a module in the
     *               Java EE Application
     */
    public void setJ2eeApplication(boolean isJ2ee);
    
    /**
     * Getter method for a flag that indicates if this application is a part
     *of a Java EE Application
     * @return isJ2ee flag which is set to true if the module is a module in the
     *               Java EE Application
     */
    public boolean getJ2eeApplication();

}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.interfaces.pluggable;

import com.sun.enterprise.deployment.util.ComponentVisitor;

/**
 * The ArchiveDescriptor is the data model to hold the metadata of a module.
 * <p> It should also hold additional information such as the module 
 * classloader, module root directory and module scratch directory for
 * loading the module to the container. These values should be set on the 
 * ArchiveDescriptor in the 
 * {@link ArchiveDeployer#prepare(File, File, ClassLoader)} method.
 *
 */
public interface ArchiveDescriptor {
                                                                                
    /**
     * This SPI retrieves the module classloader from the archive descriptor.
     * <br>The module classloader should be set in the 
     * {@link ArchiveDeployer#prepare(File, File, ClassLoader)} method.
     *
     * @return the classloader which contains the module specific classpaths
     *         in addition to the glassfish server classpaths 
     */
    ClassLoader getModuleClassLoader();
    
    /**
     * This SPI retrieves the module name from the archive descriptor.
     *
     * @return the name of the module 
     */    
    String getModuleName();
    
    /**
     * This SPI retrieves the name of the descriptor
     * of this archive descriptor is a part. This is valid only in the
     * case that the ArchiveDescriptor is a part of a parent descriptor
     *
     * @return the descriptor object
     */ 
    Object getReferringDescriptor();
    
    /**
     * This SPI provides an entry point for resolving references
     * in a extension module when its a part of a Application
     *
     * @param the ComponentVisitor object
     */ 
    void visit(ComponentVisitor cVisitor);
}

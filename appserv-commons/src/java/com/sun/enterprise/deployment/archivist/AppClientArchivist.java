
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.archivist;

import java.io.InputStream;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.Vector;
import java.util.Iterator;
import javax.enterprise.deploy.shared.ModuleType;
import org.xml.sax.SAXParseException;

import com.sun.enterprise.deployment.ApplicationClientDescriptor;
import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.Descriptor;
import com.sun.enterprise.deployment.Application;
import com.sun.enterprise.deployment.io.AppClientDeploymentDescriptorFile;
import com.sun.enterprise.deployment.io.DeploymentDescriptorFile;
import com.sun.enterprise.deployment.io.runtime.AppClientRuntimeDDFile;
import com.sun.enterprise.deployment.node.appclient.AppClientNode;
import com.sun.enterprise.deployment.RootDeploymentDescriptor;
import com.sun.enterprise.deployment.util.AppClientVisitor;
import com.sun.enterprise.deployment.util.ApplicationValidator;
import com.sun.enterprise.deployment.util.DOLUtils;
import com.sun.enterprise.deployment.util.ModuleContentValidator;
import com.sun.enterprise.deployment.util.ModuleDescriptor;

/**
 * This class is responsible for handling J2EE app client files.
 *
 * @author Sheetal Vartak
 * @version 
 */
public class AppClientArchivist extends Archivist {

    ApplicationClientDescriptor descriptor = null;
    DeploymentDescriptorFile standardDD = new AppClientDeploymentDescriptorFile();
    
    /** Creates new ApplicationClientArchvisit */
    public AppClientArchivist() {
        handleRuntimeInfo = true;
    }
   
    /**
     * @return the  module type handled by this archivist 
     * as defined in the application DTD
     *
     */
    public ModuleType getModuleType() {
        return ModuleType.CAR;
    }        
            
    /** 
     * Set the DOL descriptor  for this Archivist, used by super classes 
     */    
    public void setDescriptor(Descriptor descriptor) {
        if (descriptor instanceof ApplicationClientDescriptor) {
            this.descriptor = (ApplicationClientDescriptor) descriptor;
        } else {
           if (descriptor instanceof Application) {
                // this is acceptable if the application actually represents
                // a standalone module
                java.util.Set appClientBundles = ((Application) descriptor).getApplicationClientDescriptors();
                if (appClientBundles.size()>0) {
                    this.descriptor = (ApplicationClientDescriptor) appClientBundles.iterator().next();
                    if (this.descriptor.getModuleDescriptor().isStandalone())
                        return;
                    else
                        this.descriptor=null;
                }
            }
            DOLUtils.getDefaultLogger().log(Level.SEVERE, "enterprise.deployment.backend.descriptorFailure", new Object[] {this});
            throw new RuntimeException("Error setting descriptor " + descriptor + " in " + this);
        }
    }
    
    /**
     * @return the DeploymentDescriptorFile responsible for handling
     * standard deployment descriptor
     */
    public DeploymentDescriptorFile getStandardDDFile() {
        return standardDD;
    }  
    
    /**
     * @return if exists the DeploymentDescriptorFile responsible for
     * handling the configuration deployment descriptors
     */
    public DeploymentDescriptorFile getConfigurationDDFile() {
        return new AppClientRuntimeDDFile();
    }     
    
    /**
     * @return the Descriptor for this archvist
     */ 
    public Descriptor getDescriptor() {
        return descriptor;
    }

    /**
     * @return a default BundleDescriptor for this archivist
     */
    public Descriptor getDefaultBundleDescriptor() {
        ApplicationClientDescriptor appClientDesc = 
            new ApplicationClientDescriptor();
        return appClientDesc;
    }

    /**
     * validates the DOL Objects associated with this archivist, usually
     * it requires that a class loader being set on this archivist or passed
     * as a parameter
     */
    public void validate(ClassLoader aClassLoader) {
        ClassLoader cl = aClassLoader;
        if (cl==null) {
            cl = classLoader;
        }
        if (cl==null) {
            return;
        }
        descriptor.setClassLoader(cl);
        descriptor.visit((AppClientVisitor) new ApplicationValidator());        
    }         
        
    /**
     * perform any action after all standard DDs is read
     * @param the deployment descriptor for the module
     * @param the module archive
     */
    protected void postStandardDDsRead(RootDeploymentDescriptor descriptor,
            AbstractArchive archive) throws IOException {
        super.postStandardDDsRead(descriptor, archive);
        // look for MAIN_CLASS
        ApplicationClientDescriptor appClient = (ApplicationClientDescriptor)descriptor;
        Manifest m = archive.getManifest();
        appClient.setMainClassName(getMainClassName(m));
    }
    
    /**
     * perform any post deployment descriptor reading action
     *
     * @param the deployment descriptor for the module
     * @param the module archive
     */
    protected void postOpen(RootDeploymentDescriptor descriptor, AbstractArchive archive)
        throws IOException 
    {
        
        super.postOpen(descriptor, archive);
        
        ApplicationClientDescriptor appClient = (ApplicationClientDescriptor) descriptor;
        ModuleContentValidator mdv = new ModuleContentValidator(archive);
        appClient.visit(mdv);
    }         

    /**
     * Add this archive to an application archivist 
     * 
     * @param application archive to add itself to
     * @param library jars for this archive
     * @param external deployment descriptor path
     */
    protected ModuleDescriptor addToArchive(ApplicationArchivist appArch, String externalDD) 
        throws IOException, SAXParseException {    
            
        ModuleDescriptor module = super.addToArchive(appArch, externalDD);
        if (module!=null) {
            ApplicationClientDescriptor acd = (ApplicationClientDescriptor) module.getDescriptor();
            AbstractArchive jarFile = abstractArchiveFactory.openArchive(getArchiveUri());
            Manifest jarManifest = jarFile.getManifest();
            if (jarManifest!=null) {
                String mainClassName = getMainClassName(jarManifest);
                if (mainClassName!=null) {
                    acd.setMainClassName(mainClassName);
                }
            }            
            jarFile.close();            
        }
        return module;
    }
    
    /**
     * writes the content of an archive to a JarFile
     * 
     * @param the input  archive
     * @param the archive output stream to write to
     * @param the files to not write from the original archive
     */
    protected void writeContents(AbstractArchive in, AbstractArchive out, Vector entriesToSkip) 
        throws IOException {   
        
        // prepare the manifest file to add the main class entry
        if (manifest==null) {
            manifest = new Manifest();
        }
	manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, MANIFEST_VERSION_VALUE);        
        manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, 
            ((ApplicationClientDescriptor) getDescriptor()).getMainClassName());
        
        super.writeContents(in, out, entriesToSkip);
    }
     
    /**
     * @return the manifest attribute Main-class
     */
    public String getMainClassName(Manifest m) {
        if (m!=null) {
            return m.getMainAttributes().getValue(Attributes.Name.MAIN_CLASS);
        }
        return null;
    }

    protected boolean postHandles(AbstractArchive abstractArchive) 
            throws IOException {
        //check the main-class attribute
        if (getMainClassName(abstractArchive.getManifest()) != null) {
            return true;
        }

        return false;
    }

    protected String getArchiveExtension() {
        return APPCLIENT_EXTENSION;
    }

    @Override public void readPersistenceDeploymentDescriptors(
            AbstractArchive archive,
            Descriptor descriptor)
            throws IOException, SAXParseException {
        if (logger.isLoggable(Level.FINE)) {
            logger.logp(Level.FINE, "AppClientArchivist",
                    "readPersistenceDeploymentDescriptors", "archive = {0}",
                    archive.getURI());
        }
        // note we pass "" as the PURootPath because META-INF/persistence.xml
        // can only be present on the top level in an client-jar, so the root of
        // persistence unit is always same as the root of an client-jar file.
        // hence relative distance between them is empty.
        readPersistenceDeploymentDescriptor(archive, "", descriptor);
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * PluggableArchivistsHelper.java
 *
 * Created on June 4, 2004, 10:48 AM
 */

package com.sun.enterprise.deployment.archivist;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import javax.enterprise.deploy.shared.ModuleType;

import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.deploy.shared.FileArchive;
import com.sun.enterprise.deployment.deploy.shared.InputJarArchive;
import com.sun.enterprise.deployment.util.DOLUtils;
import com.sun.enterprise.util.i18n.StringManager;


/**
 *
 * @author Jerome Dochez
 */
public class PluggableArchivistsHelper implements PluggableArchivists {
    
    // All archivist should be registered here with an empty instance
    // I could have used a prototype or some factory classes but it would
    // have been an overkill. Instances are registered so I can use the 
    // abstract methods defined at the Archivist level.
    private Archivist[] archivists = new Archivist[0];    

    /** string manager */
    private static StringManager localStrings =
        StringManager.getManager( PluggableArchivistsHelper.class );

    
    /** Creates a new instance of PluggableArchivistsHelper */
    public PluggableArchivistsHelper() {
    }
    
    /** 
     * @return a new Archivist implementation for the archive file type
     * Supported J2EE modules are defined in the J2EE platform spec
     */    
    public Archivist getArchivistForArchive(String path) throws IOException {
        File f = new File(path);
        if (!f.exists()) {
            throw new FileNotFoundException(path);
        }
        AbstractArchive archive;
        if (f.isDirectory()) {
            archive = new FileArchive();
            ((FileArchive) archive).open(path);
        } else {
            archive = new InputJarArchive();
            ((InputJarArchive) archive).open(path);
        }
        Archivist archivist=null;
        try {            
            archivist = getArchivistForArchive(archive);
        } finally {        
            archive.close();
        }
        return archivist;        
    }
    
    /** 
     * @return a new Archivist implementation for the archive file type
     * Supported J2EE modules are defined in the J2EE platform spec
     */    
    public Archivist getArchivistForArchive(File jarFileOrDirectory) throws IOException {
        return getArchivistForArchive(jarFileOrDirectory.getAbsolutePath());        
    }

    
    /** 
     * @return a new Archivist implementation for the archive file type
     * Supported J2EE modules are defined in the J2EE platform spec
     */    
    public Archivist getArchivistForArchive(AbstractArchive archive) throws IOException {

        Archivist a = handles(archive);
        if (a != null) {
            try {
                Archivist archivist = (Archivist) a.getClass().newInstance();
                if (archivist instanceof ExtensionModuleArchivist) {
                    ((ExtensionModuleArchivist)archivist).initialize(a);
                }
                archivist.setPluggableArchivists(this);
                return archivist;
            } catch (Exception e) {
                DOLUtils.getDefaultLogger().log(
                Level.SEVERE, 
                "enterprise.deployment.backend.archivistInstantiationFailure",
                new Object[] {a.getClass(), archive});
                e.printStackTrace();
            }
        } else {
            String msg = localStrings.getString(
                "enterprise.deployment.unknown.application.type",
                archive.getArchiveUri());
            throw new IOException(msg);
        }
        return null;        
    }

    private synchronized Archivist handles(AbstractArchive archive) throws IOException {

        //first, check the existence of any deployment descriptors
        for (Archivist a : archivists) {
            if (a.handles(archive)) {
                return a;
            }
        }

        // Java EE 5 Specification: Section EE.8.4.2.1

        //second, check file extension if any, excluding .jar as it needs
        //additional processing
        String uri = archive.getArchiveUri();
        File file = new File(uri);
        if (!file.isDirectory() && !uri.endsWith(Archivist.EJB_EXTENSION)) {
            for (Archivist a : archivists) {
                if (uri.endsWith(a.getArchiveExtension())) {
                    return a;
                }
            }
        }

        //finally, still not returned here, call for additional processing
        for (Archivist a : archivists) {
            if (a.postHandles(archive)) {
                return a;
            }
        }

        return null;
    }

    /** 
     * @return a new Archivist implementation for the type passed. 
     * Supported types are defined in the application.xml DTD 
     */    
    public synchronized Archivist getArchivistForType(ModuleType type) {
        
        for (int i=0;i<archivists.length;i++) {
            if (archivists[i].getModuleType().equals(type)) {
                try {
                    Archivist archivist = (Archivist) archivists[i].getClass().newInstance();                    
                    if (archivist instanceof ExtensionModuleArchivist) {
                        ((ExtensionModuleArchivist)archivist).initialize(
                            archivists[i]);
                    }
                    archivist.setPluggableArchivists(this);
                    return archivist;
                } catch (Exception  e) {
                    DOLUtils.getDefaultLogger().log(Level.SEVERE, "enterprise.deployment.backend.archivistInstantiationFailure",
                                new Object[] {archivists[i].getClass(), type});                    
                    e.printStackTrace();
                }
            }
        }
        DOLUtils.getDefaultLogger().log(Level.SEVERE, "enterprise.deployment.backend.archivistInstantiationFailure",
                                new Object[] {null, type});                    
        return null;                
    }
    
    /**
     * register a new type of archivist
     * @param Archivist to register...
     */
    public synchronized void registerArchivist(Archivist archivist) {
        for (int i=0;i<archivists.length;i++) {
            if (archivists[i].getModuleType().equals(archivist.getModuleType())) {        
                // we are replacing an archivist
                archivists[i]=archivist;
                return;
            }
        }
        // if we end up here, it's a new archvist for a new type of archive...
        Archivist[] newArchivists = new Archivist[archivists.length+1];
     
        // insert the latest registered archivist to the beginning
        System.arraycopy(archivists, 0, newArchivists, 1, archivists.length);
        newArchivists[0]=archivist;
        archivists = newArchivists;
    }    
    
    /**
     * @return the array of registered archivists
     */
    public Archivist[] getRegisteredArchivists() {
        Archivist[] newArchivists = new Archivist[archivists.length+1];
        System.arraycopy(archivists, 0, newArchivists, 0, archivists.length);
        return newArchivists;        
    }
     
}

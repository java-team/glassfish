/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.archivist;

import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import org.xml.sax.SAXParseException;
import java.util.Map;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.enterprise.deploy.shared.ModuleType;

import com.sun.logging.LogDomains;
import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveDeployer;
import com.sun.enterprise.deployment.interfaces.pluggable.ArchiveDescriptor;
import com.sun.enterprise.deployment.util.XModuleType;
import com.sun.enterprise.deployment.Descriptor;
import com.sun.enterprise.deployment.io.DeploymentDescriptorFile;
import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.deploy.shared.Archive;
import com.sun.enterprise.deployment.io.DescriptorConstants;

/**
 * This archivist is responsible for reading and write external module
 * archive files.
 *
 */
public class ExtensionModuleArchivist extends Archivist {

    private ModuleType moduleType = null;
    private ArchiveDeployer archiveDeployer = null;

    protected static final Logger logger =
            LogDomains.getLogger(LogDomains.DPL_LOGGER);

    private boolean isj2ee = false;

    public ExtensionModuleArchivist() {
    }

    public ExtensionModuleArchivist(ArchiveDeployer pDeployer) {
        archiveDeployer = pDeployer;
        // use the fully qualified class name of archive deployer
        // as the unique module type
        String mType = archiveDeployer.getClass().getName();
        moduleType = XModuleType.getModuleType(mType);
    }

    /**
     * @return the  module type handled by this archivist
     * as defined in the application DTD
     *
     */
    public ModuleType getModuleType() {
        return archiveDeployer.getModuleType();
    }
    
    public void setModuleType(String mType) {
        moduleType = XModuleType.getModuleType(mType);
    }

    /**
     * @return the archive deployer of this archivist
     *
     */
    public ArchiveDeployer getArchiveDeployer() {
        return archiveDeployer;
    }

   /**
     * initialize the archivist from another one
     * @param otherArchivist
     *
     */
    public void initialize(Archivist otherArchivist) {
        if (otherArchivist instanceof ExtensionModuleArchivist) {
            ExtensionModuleArchivist oArchivist = 
                (ExtensionModuleArchivist)otherArchivist;
            moduleType = oArchivist.getModuleType();
            archiveDeployer = oArchivist.getArchiveDeployer();
        }
    }

    /**
     * The extension module archivist is retrieved using the 
     * ArchiveDeployer.handles.
     *
     * @return empty string for extension modules
     */
    protected String getArchiveExtension() {
        return "dummy";
    }
    
    public void setJ2eeApplication(boolean isJ2ee) {
        this.isj2ee = isJ2ee;
    }
    
    public boolean getJ2eeApplication() {
        return isj2ee;
    }

    public boolean handles(AbstractArchive archive) throws IOException { 
        return archiveDeployer.handles(archive);
    }

    public ArchiveDescriptor openArchive(String moduleRoot, 
        File moduleScratchDir, ClassLoader parentClassLoader, boolean isStartup) 
        throws Exception {
            archiveDeployer.setJ2eeApplication(getJ2eeApplication());
            return archiveDeployer.prepare(new File(moduleRoot), moduleScratchDir, 
                parentClassLoader, isStartup);    
    }

    // following are no-op implementation of the rest abstact 
    // methods of super class

    /**
     * Archivist read XML deployment descriptors and keep the
     * parsed result in the DOL descriptor instances. Sets the descriptor
     * for a particular Archivist type
     */
    public void setDescriptor(Descriptor descriptor) {
        archiveDeployer.setDescriptor(descriptor);
    }
                                                                                
    /**
     * @return the Descriptor for this archvist
     */
    public Descriptor getDescriptor() {
        return archiveDeployer.getDescriptor();
    }
                                                                                
    /**
     * @return the DeploymentDescriptorFile responsible for handling
     * standard deployment descriptor
     */
    public DeploymentDescriptorFile getStandardDDFile() {
        return archiveDeployer.getStandardDDFile();
    }
                                                                                      
    /**
     * @return if exists the DeploymentDescriptorFile responsible for
     * handling the configuration deployment descriptors
     */
    public DeploymentDescriptorFile getConfigurationDDFile() {
        return archiveDeployer.getConfigurationDDFile();
    }
                                                                                      
    /**
     * @return a default BundleDescriptor for this archivist
     */
    public Descriptor getDefaultBundleDescriptor() {
        return archiveDeployer.getDefaultBundleDescriptor();
    }

    /**
     * @return true if the archivist is handling the provided archive
     */
    protected boolean postHandles(AbstractArchive abstractArchive) 
        throws IOException {
        return false;
    }
   
    /**
     * Overrided method to read the Persistence Deployment Descriptors
     * This is useful in case we wish to deploy modules with persistence DDs
     * @param archive the Archive being deployed
     * @param descriptor the descriptor object that holds the persistence DD
     *                   meta data
     * @throws IOException 
     * @throws SAXParseException
     */ 
    @Override public void readPersistenceDeploymentDescriptors(
                                   AbstractArchive archive,
                                        Descriptor descriptor)
                                           throws IOException, 
                                                SAXParseException {
        if(logger.isLoggable(Level.FINE)) {
            logger.logp(Level.FINE, "ExtensionModuleArchivist",
                    "readPersistenceDeploymentDescriptors", "archive = {0}",
                    archive.getURI());
        } 
        Map<String, Archive> subArchives = new HashMap<String, Archive>();
        Enumeration entries = archive.entries();
        final String CLASSES_DIR = "WEB-INF/classes/";
        final String LIB_DIR = "WEB-INF/lib/";
        final String JAR_EXT = ".jar";
        try {
            final String pathOfPersistenceXMLInsideClassesDir =
                    CLASSES_DIR+DescriptorConstants.PERSISTENCE_DD_ENTRY;
            while(entries.hasMoreElements()){
                final String nextEntry =
                           String.class.cast(entries.nextElement());
                if(pathOfPersistenceXMLInsideClassesDir.equals(nextEntry)) {
                    subArchives.put(CLASSES_DIR,
                               archive.getSubArchive(CLASSES_DIR));
                } else if (nextEntry.startsWith(LIB_DIR) &&
                                nextEntry.endsWith(JAR_EXT)) {
                    String jarFile = 
                           nextEntry.substring(LIB_DIR.length(),
                                 nextEntry.length()-JAR_EXT.length());
                    if(jarFile.indexOf('/') == -1) { 
                        // to avoid WEB-INF/lib/foo/bar.jar
                        // this jarFile is directly inside WEB-INF/lib directory
                        subArchives.put(nextEntry, 
                                     archive.getSubArchive(nextEntry));
                    } else {
                       if(logger.isLoggable(Level.FINE)) {
                            logger.logp(Level.FINE, "ExtensionModuleArchivist",
                                    "readPersistenceDeploymentDescriptors",
                                    "skipping {0} as it exists inside a directory in {1}.",
                                    new Object[]{nextEntry, LIB_DIR});
                        } 
                        continue;
                    }
                }
            }
            for(Map.Entry<String, Archive> pathToArchiveEntry :
                                               subArchives.entrySet()) {
                readPersistenceDeploymentDescriptor(
                       pathToArchiveEntry.getValue(),
                           pathToArchiveEntry.getKey(), descriptor);
            }
        } finally {
            for(Archive subArchive : subArchives.values()) {
                subArchive.close();
            }
        }
    }
}

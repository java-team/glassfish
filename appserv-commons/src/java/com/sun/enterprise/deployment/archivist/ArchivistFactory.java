/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.deployment.archivist;

import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import javax.enterprise.deploy.shared.ModuleType;

import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.util.DOLUtils;

/**
 * This factory class is responsible for creating Archivists
 *
 * @author  Jerome Dochez
 * @version 
 */
public class ArchivistFactory {
    
    private static final PluggableArchivistsHelper defaultArchivists = initDefaultArchivists();
    
    /** no need to create ArchivistFactory */
    private ArchivistFactory() {
    }

    
    private static PluggableArchivistsHelper initDefaultArchivists() {
        
        // initialize our default Archivists helper singleton 
        // instance.
        PluggableArchivistsHelper result = new PluggableArchivistsHelper();
        result.registerArchivist(new WebArchivist());
        result.registerArchivist(new ConnectorArchivist());
        result.registerArchivist(new AppClientArchivist());        
        result.registerArchivist(new EjbArchivist());
        
        // register the application archivist last so it will 
        // appear in the archivist array before other JavaEE 
        // archivists
        result.registerArchivist(new ApplicationArchivist());
        return result;
    }

    public static PluggableArchivists getPluggableArchivists() {
        return defaultArchivists;
    }
    
    /** 
     * @return a new Archivist implementation for the type passed. 
     * Supported types are defined in the application.xml DTD 
     */
    public static Archivist getArchivistForType(ModuleType type) {
        return defaultArchivists.getArchivistForType(type);
    }
    
    /** 
     * @return a new Archivist implementation for the archive file type
     * Supported J2EE modules are defined in the J2EE platform spec
     */
    public static Archivist getArchivistForArchive(File jarFileOrDirectory) throws IOException {    
        return defaultArchivists.getArchivistForArchive(jarFileOrDirectory);
    }        
    
    /** 
     * @return a new Archivist implementation for the archive file type
     * Supported J2EE modules are defined in the J2EE platform spec
     */
    public static Archivist getArchivistForArchive(String path) throws IOException {    
        return defaultArchivists.getArchivistForArchive(path);
    }
        
    /** 
     * @return a new Archivist implementation for the archive file type
     * Supported J2EE modules are defined in the J2EE platform spec
     */
    public static Archivist getArchivistForArchive(AbstractArchive archive) throws IOException {   
        return defaultArchivists.getArchivistForArchive(archive);
    }
    
    /**
     * register a new type of archivist
     * @param archivist to register...
     */
    public static void registerArchivist(Archivist archivist) {
        defaultArchivists.registerArchivist(archivist);
    }
}

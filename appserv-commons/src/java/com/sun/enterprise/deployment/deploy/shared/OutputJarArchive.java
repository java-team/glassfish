/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.*;
import java.util.*;
import java.util.jar.Manifest;
import java.util.zip.*;
import java.net.URI;

import com.sun.enterprise.deployment.util.DOLUtils;

/**
 * Provides an implementation of the AbstractArchive that maps to 
 * a Jar file @see java.util.jar.JarFile
*
 * @author  Jerome Dochez
 */
public class OutputJarArchive extends AbstractArchive {
    
    // the archiveUri
    private String archiveUri;
    
    // the file we are currently mapped to (if open for writing)
    protected ZipOutputStream jos=null;
    
    private Manifest manifest=null;
    
    // list of entries already written to this ouput
    private Vector entries = new Vector();
    
    
    /** 
     * @return the archive uri
     */ 
    public String getArchiveUri() {
        return archiveUri;
    }
        
    /**
     * Get the size of the archive
     * @return -1 because this is getting created
     */
    public long getArchiveSize() throws NullPointerException, SecurityException {
        return -1;
    }
    
    /** close the abstract archive
     */
    public void close() throws IOException {
        if (jos!=null) {
            jos.flush();
            jos.finish();
            jos.close();
            jos=null;
        }
    }
        
    /** creates a new abstract archive with the given path
     * @param the path to create the archive
     */
    public void create(String path) throws IOException {
        archiveUri = path;
        File file = new File(path);
        // if teh file exists, we delete it first
        if (file.exists()) {
            file.delete();
        }
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        jos = new ZipOutputStream(bos);
    }
        
    /** @return an @see java.util.Enumeration of entries in this abstract
     * archive
     */
    public Enumeration entries() {
        return entries.elements();
    }
    
    /**
     *  @return an @see java.util.Enumeration of entries in this abstract
     * archive, providing the list of embedded archive to not count their 
     * entries as part of this archive
     */
     public Enumeration entries(Enumeration embeddedArchives) {
  	return entries();
     }    
    
    /** @return a @see java.io.InputStream for an existing entry in
     * the current abstract archive
     * @param the entry name
     */
    public InputStream getEntry(String name) throws IOException {
        throw new UnsupportedOperationException("Cannot read from a JAR archive open for writing");        
    }
    
    /** Open an abstract archive
     * @param the path to the archive
     */
    public void open(String path) throws IOException {
        throw new UnsupportedOperationException("Cannot read jar files");        
    }        
    
    /** @return the manifest information for this abstract archive
     */
    public Manifest getManifest() throws IOException {
        if (manifest!=null) {
            manifest = new Manifest();
        }
        return manifest;
    }
    
    public boolean exists() {
        throw new UnsupportedOperationException("Cannot read from a JAR archive open for writing");        
    }
    
    public boolean delete() {
        throw new UnsupportedOperationException("Cannot read from a JAR archive open for writing");        
    }
    
    public boolean renameTo(String name) {
        throw new UnsupportedOperationException("Cannot read from a JAR archive open for writing");        
    }
    
    public AbstractArchive getEmbeddedArchive(String name) throws IOException {
        OutputStream os = putNextEntry(name);
        ZipOutputStream jos = new ZipOutputStream(os);
        OutputJarArchive ja = new OutputJarArchive();
        ja.archiveUri = name;
        ja.jos = jos;
        return ja;
    }
    
    /** close a previously returned @see java.io.OutputStream returned
     * by an addEntry call
     * @param the output stream to close
     */
    public void closeEntry(AbstractArchive os) throws IOException {
        if (os instanceof OutputJarArchive) {
            ((OutputJarArchive) os).jos.flush();
            ((OutputJarArchive) os).jos.finish();
        }
        jos.closeEntry();
    }
    
       
    /** @returns an @see java.io.OutputStream for a new entry in this
     * current abstract archive.
     * @param the entry name
     */
    public OutputStream putNextEntry(String name) throws java.io.IOException {
        if (jos!=null) {
            ZipEntry ze = new ZipEntry(name);
            jos.putNextEntry(ze);
            entries.add(name);
        } 
        return jos;      
    }

    
    /**
     * closes the current entry
     */
    public void closeEntry() throws IOException {
        if (jos!=null) {
            jos.flush();
            jos.closeEntry();
        } 
    }    
    
    public java.net.URI getURI() {
        try {
            return ArchiveFactory.prepareArchiveURI(getArchiveUri());
        } catch(java.net.URISyntaxException e) {
            return null;
        } catch (UnsupportedEncodingException uee) {
            return null;
        } catch (IOException ioe) {
            return null;
        }
    }
    
    
}

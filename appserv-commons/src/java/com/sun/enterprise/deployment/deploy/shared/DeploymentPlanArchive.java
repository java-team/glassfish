/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Vector;
import java.util.Enumeration;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import com.sun.enterprise.util.io.FileUtils;

/**
 * This AbstractArchive offers an abstraction for jsr88 
 * deployment plan as defined for the SJES Application 
 * Server. 
 *
 * @author Jerome Dochez
 */
public class DeploymentPlanArchive extends AbstractArchive {

    // the deployment plan jar file...
    JarFile jarFile = null;
    
    // original archive uri
    String archiveUri = null;
    
    // cached list of elements
    Vector elements=null;
    
    String subArchiveUri=null;
    
    /** Creates a new instance of DeploymentPlanArchive 
     * package private
     */
    public DeploymentPlanArchive() {
    }
    
    /** Open an existing DeploymentPlan archive and return 
     * a abstraction for reading from it.
     * @param the path to the archive
     */
    public void open(String path) throws IOException {
        archiveUri = path;
        File f = new File(path);
        if (f.exists()) {
            jarFile = new JarFile(f);
        }
    }
    
    /**
     * Get the size of the archive
     * @return tje the size of this archive or -1 on error
     */
    public long getArchiveSize() throws NullPointerException, SecurityException {
        if(getArchiveUri() == null) {
            return -1;
        }
        File tmpFile = new File(getArchiveUri());
        return(tmpFile.length());
    }
    
    /**
     * Closes the current jar file
     */
    public void close() throws java.io.IOException {
        if (jarFile!=null) {
            jarFile.close();
            jarFile=null;
        }
    }
    
    /**
     * Closes the output jar file entry
     */
    public void closeEntry() throws java.io.IOException {
        // nothing to do
    }
    
    /**
     * Closes the output sub archive entry
     */
    public void closeEntry(AbstractArchive sub) throws java.io.IOException {       
        // nothing to do...
    }
    
    /**
     * Deletes the underlying jar file
     */
    public boolean delete() {
        File f = new File(archiveUri);
        if (f.exists()) {
            return FileUtils.deleteFile(f);
        }
        return false;
    }
    
    /**
     * @return an Enumeration of entries for this archive
     */
    public Enumeration entries() {
        // Deployment Plan are organized flatly, 
        
        if (elements==null) {
            synchronized(this) {
                elements = new Vector();
                for (Enumeration e = jarFile.entries();e.hasMoreElements();) {
                    ZipEntry ze = (ZipEntry) e.nextElement();
                    if (!ze.isDirectory() && !ze.getName().equals(
                            JarFile.MANIFEST_NAME)) {
                        elements.add(ze.getName());
                    }
                }
            }
        }

        Vector entries = new Vector();
        for (Enumeration e = elements.elements();e.hasMoreElements();) {
            
            String entryName = (String) e.nextElement();
            
            String mangledName = entryName;
            String prefix = "META-INF/";
            if (entryName.indexOf("sun-web.xml")!=-1) {
                prefix = "WEB-INF/";
            }  
            if (subArchiveUri != null && entryName.startsWith(subArchiveUri)) {
                mangledName = mangledName.substring(subArchiveUri.length()+1);
            }
            if (entryName.endsWith(".dbschema")) {
                mangledName = mangledName.replaceAll("#", "/");
            } else {
                mangledName = prefix + mangledName;
            }
            
            if (subArchiveUri==null) {
                // top level archive
                if ((entryName.indexOf(".jar.")!=-1) || 
                    (entryName.indexOf(".war.")!=-1) || 
                    (entryName.indexOf(".rar."))!=-1) {
                    
                    // this element is in a sub archive
                    continue;
                }
                entries.add(mangledName);            
            } else {
                // this is a sub archive
                if (entryName.startsWith(subArchiveUri)) {
                    entries.add(mangledName);
                }
            }             
        } 
        return entries.elements();
    }
    
    /**
     * @return an Enumeration of entries not including entries 
     * from the subarchives
     */
    public Enumeration entries(java.util.Enumeration embeddedArchives) {
        return entries();
    }
    
    /**
     * @return true if the underlying archive exists
     */
    public boolean exists() {
        File f = new File(archiveUri);
        return f.exists();
    }
    
    /**
     * @return the archive URI 
     */
    public String getArchiveUri() {
        return archiveUri;
    }
    
    /**
     * @return a sub archive giving the name 
     */
    public AbstractArchive getEmbeddedArchive(String name) throws java.io.IOException {
        if (jarFile==null) {
            return null;
        }
        DeploymentPlanArchive dpArchive = new DeploymentPlanArchive();
        dpArchive.jarFile = new JarFile(archiveUri);
        dpArchive.archiveUri = archiveUri + File.separator + name;
        dpArchive.subArchiveUri = name;
        dpArchive.elements = elements;
        return dpArchive;        
    }
    
    /**
     * @return an input stream giving its entry name
     */
    public InputStream getEntry(String name) throws IOException {

        // we are just interested in the file name, not the 
        // relative path
        if (name.endsWith(".dbschema")) {
            name = name.replaceAll("/", "#");
        } else {
            name = name.substring(name.lastIndexOf('/')+1);
        }
        
        if (subArchiveUri==null) {
            // we are at the "top level"
            
            return getElement(name);
        } else {
            // we are in a sub archive...
            // now let's mangle the name...
            String mangledName = subArchiveUri + "." + 
                name;
            return getElement(mangledName);
            
        }        
    }
    
    private InputStream getElement(String name) throws IOException {
        if (elements.contains(name)) {
            return jarFile.getInputStream(jarFile.getEntry(name));
        } else {
            return null;
        }
    }
    
    /** 
     * @return the manifest
     */
    public java.util.jar.Manifest getManifest() throws java.io.IOException {
        // no manifest in DeploymentPlan
        return new Manifest();
    }
    
    /**
     * @return the URI for this archive
     */
    public java.net.URI getURI() {
        File f = new File(archiveUri);
        try {
            return new URI("jar://" + f.toURI().getSchemeSpecificPart());
        } catch(java.net.URISyntaxException e) {
            return null;
        }
    }
    
    /**
     * Add an entry to the archive
     */
    public java.io.OutputStream putNextEntry(String name) throws java.io.IOException {
        // not supported for now.
        throw new UnsupportedOperationException();
    }
    
    /**
     * rename the underlying archive
     */
    public boolean renameTo(String name) {
        File f = new File(archiveUri);
        boolean result = FileUtils.renameFile(f, new File(name));
        if (result) {
            archiveUri = name;
        }
        return result;
        
    }
    
}

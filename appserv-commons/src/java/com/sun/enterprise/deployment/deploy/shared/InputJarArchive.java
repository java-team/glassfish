/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.*;
import java.util.*;
import java.util.jar.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.net.URI;

import com.sun.enterprise.deployment.util.DOLUtils;
import com.sun.enterprise.deployment.deploy.shared.Archive;
import com.sun.enterprise.util.io.FileUtils;
import com.sun.enterprise.util.i18n.StringManager;

/**
 * This implementation of the AbstractArchive deal with reading 
 * jar files either from a JarFile or from a JarInputStream
 *
 * @author Jerome Dochez
 */
public class InputJarArchive extends AbstractArchive {
    
    
    // the file we are currently mapped to 
    protected JarFile jarFile=null;
    
    // in case this abstraction is dealing with a jar file
    // within a jar file, the jarFile will be null and this
    // JarInputStream will contain the 
    protected JarInputStream jarIS=null; 
    
    // the archive Uri
    private String archiveUri;

    // parent jar file for embedded jar
    private InputJarArchive parentArchive=null;

    private StringManager localStrings = StringManager.getManager(getClass());


    /** 
     * @return the archive uri
     */ 
    public String getArchiveUri() {
        return archiveUri;
    }       
    
    /**
     * Get the size of the archive
     * @return tje the size of this archive or -1 on error
     */
    public long getArchiveSize() throws NullPointerException, SecurityException {
        if(getArchiveUri() == null) {
            return -1;
        }
        File tmpFile = new File(getArchiveUri());
        return(tmpFile.length());
    }
    
    /** @returns an @see java.io.OutputStream for a new entry in this
     * current abstract archive.
     * @param the entry name
     */
    public OutputStream addEntry(String name) throws IOException {
        throw new UnsupportedOperationException("Cannot write to an JAR archive open for reading");        
    }
    
    /** 
     * close the abstract archive
     */
    public void close() throws IOException {
        if (jarFile!=null) {
            jarFile.close();
            jarFile=null;
        }
        if (jarIS!=null) {
            jarIS.close();
            jarIS=null;
        }
    }
        
    /** 
     * creates a new abstract archive with the given path
     *
     * @param the path to create the archive
     */
    public void create(String path) throws IOException {
        throw new UnsupportedOperationException("Cannot write to an JAR archive open for reading");        
    }
        
    /** 
     * @return an @see java.util.Enumeration of entries in this abstract
     * archive
     */
    public Enumeration entries() {
        Vector entries = new Vector();
 
        if (parentArchive!=null) {
            try {
                // reopen the embedded archive and position the input stream
                // at the beginning of the desired element
                jarIS = new JarInputStream(parentArchive.jarFile.getInputStream(parentArchive.jarFile.getJarEntry(archiveUri)));
                JarEntry ze;
                do {
                    ze = jarIS.getNextJarEntry();
                    if (ze!=null && !ze.isDirectory()) {
                        entries.add(ze.getName());
                    }                
                } while (ze!=null);
                jarIS.close();
                jarIS = null;
            } catch(IOException ioe) {
                return null;
            }
        } else {
            try {
                if (jarFile==null) {
                    getJarFile(archiveUri);
                }
            } catch(IOException ioe) {
                return entries.elements();
            }
            if (jarFile==null) {
                return entries.elements();
            }
            for (Enumeration e = jarFile.entries();e.hasMoreElements();) {
                ZipEntry ze = (ZipEntry) e.nextElement();
                if (!ze.isDirectory() && !ze.getName().equals(JarFile.MANIFEST_NAME)) {
                    entries.add(ze.getName());
                }
            }
        }
        return entries.elements();        
    }
    
    /**
     *  @return an @see java.util.Enumeration of entries in this abstract
     * archive, providing the list of embedded archive to not count their 
     * entries as part of this archive
     */
     public Enumeration entries(Enumeration embeddedArchives) {
	// jar file are not recursive    
  	return entries();
    }
    
    /**
     * @return a @see java.io.InputStream for an existing entry in
     * the current abstract archive
     * @param the entry name
     */
    public InputStream getEntry(String entryName) throws IOException {
        if (jarFile!=null) {
            ZipEntry ze = jarFile.getEntry(entryName);
            if (ze!=null) {
                return new BufferedInputStream(jarFile.getInputStream(ze));
            } else {
                return null;
            }            
        } else
	if ((parentArchive != null) && (parentArchive.jarFile != null)) {
            JarEntry je;
            // close the current input stream
            if (jarIS!=null) {
                jarIS.close();
            }
            
            // reopen the embedded archive and position the input stream
            // at the beginning of the desired element
	    JarEntry archiveJarEntry = (archiveUri != null)? parentArchive.jarFile.getJarEntry(archiveUri) : null;
	    if (archiveJarEntry == null) {
		return null;
	    }
            jarIS = new JarInputStream(parentArchive.jarFile.getInputStream(archiveJarEntry));
            do {
                je = jarIS.getNextJarEntry();
            } while (je!=null && !je.getName().equals(entryName));
            if (je!=null) {
                return new BufferedInputStream(jarIS);
            } else {
                return null;
            }
        } else {
	    return null;
	}
    }
    
    /** Open an abstract archive
     * @param the path to the archive
     */
    public void open(String path) throws IOException {
        archiveUri = path;
        jarFile = getJarFile(path);
    }
    
    /**
     * @return a JarFile instance for a file path
     */
    protected JarFile getJarFile(String path) throws IOException {
        jarFile = null;
        try {
            File file = new File(path);
            if (file.exists()) {
                jarFile = new JarFile(file);
            }
        } catch(IOException e) {
            DOLUtils.getDefaultLogger().log(Level.SEVERE, "enterprise.deployment.backend.fileOpenFailure", 
                    new Object[]{path});
            // add the additional information about the path
            // since the IOException from jdk doesn't include that info
            String additionalInfo = localStrings.getString(
                "enterprise.deployment.invalid_zip_file", path); 
            IOException ioe = new IOException(e.getLocalizedMessage() + " --  " + additionalInfo);
            ioe.initCause(e);
            throw ioe;
        }
        return jarFile;
    }       
    
    
    /** 
     * @return the manifest information for this abstract archive
     */
    public Manifest getManifest() throws IOException {
        if (jarFile!=null) {
            return jarFile.getManifest();
        } 
        if (parentArchive!=null) {    
            // close the current input stream
            if (jarIS!=null) {
                jarIS.close();
            }
            // reopen the embedded archive and position the input stream
            // at the beginning of the desired element
            if (jarIS==null) {
                jarIS = new JarInputStream(parentArchive.jarFile.getInputStream(parentArchive.jarFile.getJarEntry(archiveUri)));
            }
            Manifest m = jarIS.getManifest();
            if (m==null) {
               java.io.InputStream is = getEntry(java.util.jar.JarFile.MANIFEST_NAME);
               if (is!=null) {
                    m = new Manifest();
                    m.read(is);
                    is.close();
               }
            }
            return m;
        }                        
        return null;
    }
    
    /**
     * @return true if this abstract archive maps to an existing 
     * jar file
     */
    public boolean exists() {
        return jarFile!=null;
    }
    
    /**
     * deletes the underlying jar file
     */
    public boolean delete() {
        if (jarFile==null) {
            return false;
        }
        try {
            jarFile.close();
            jarFile = null;
        } catch (IOException ioe) {
            return false;
        }
        return FileUtils.deleteFile(new File(archiveUri));
    }
    
    /**
     * rename the underlying jar file
     */
    public boolean renameTo(String name) {
        if (jarFile==null) {
            return false;
        }
        try {
            jarFile.close();
            jarFile = null;
        } catch (IOException ioe) {
            return false;
        }        
        return FileUtils.renameFile(new File(archiveUri), new File(name));
    }
    
    /**
     * @return an AbstractArchive for an embedded archive indentified with 
     * the name parameter
     */
    public AbstractArchive getEmbeddedArchive(String name) throws IOException {
        if (jarFile!=null) {
            // for now, I only support one level down embedded archives
            InputJarArchive ija = new InputJarArchive();
            JarEntry je = jarFile.getJarEntry(name);
            if (je!=null) {
                JarInputStream jis = new JarInputStream(new BufferedInputStream(jarFile.getInputStream(je)));
                ija.archiveUri = name;
                ija.jarIS = jis;
                ija.parentArchive = this;
                return ija;
            }
        }
        return null;
    }
    
    /** close a previously returned @see java.io.OutputStream returned
     * by an addEntry call
     * @param the output stream to close
     */
    public void closeEntry(AbstractArchive os) throws IOException {
        throw new UnsupportedOperationException("Cannot write to an JAR archive open for reading");        
    }
            
    public void closeEntry() {
        throw new UnsupportedOperationException("Cannot write to an JAR archive open for reading");                
    }    
    
    public URI getURI() {
        try {
            return ArchiveFactory.prepareArchiveURI(getArchiveUri());
        } catch(java.net.URISyntaxException e) {
            return  null;
        } catch (UnsupportedEncodingException uee) {
            return null;
        } catch (IOException ioe) {
            return null;
        }
    }
    
    public OutputStream putNextEntry(String name) throws java.io.IOException {
        throw new UnsupportedOperationException("Cannot write to an JAR archive open for reading");        
    }
    
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.jar.Manifest;
import java.util.Vector;

import com.sun.enterprise.deployment.deploy.shared.WritableArchive;
import com.sun.enterprise.deployment.deploy.shared.Archive;

/**
 * This abstract class contains all common implementation of the 
 * Archive/WritableArchive interfaces for all types of archives.
 *
 * @author Jerome Dochez
 */
public abstract class AbstractArchive implements WritableArchive {

    /** 
     * @return the archive uri
     */ 
    public abstract String getArchiveUri();

    /**
     * @return the size of the archive
     */
     public abstract long getArchiveSize() throws NullPointerException, SecurityException;
                
    /**
     * @return an @see java.util.Enumeration of entries in this abstract
     * archive, providing the list of embedded archive to not count their 
     * entries as part of this archive
     */
    public abstract Enumeration entries(Enumeration embeddedArchives);    
    
    /**
     * @return true if this archive exists
     */
    public abstract boolean exists();
    
    /**
     * deletes the archive
     */
    public abstract boolean delete();
    
    /**
     * rename the archive
     *
     * @param name the archive name
     */
    public abstract boolean renameTo(String name);
    
    /**
     * @return true if this archive abstraction supports overwriting of elements
     */
    public boolean supportsElementsOverwriting() {
        return false;
    }
    
    /**
     * @return the sub archive
     */
    public Archive getSubArchive(String name) throws IOException {
        return getEmbeddedArchive(name);
    }
    
    /**
     * close a sub archive
     */
    public abstract void closeEntry(AbstractArchive sub) throws IOException;
    
    public abstract AbstractArchive getEmbeddedArchive(String name) 
        throws IOException;

    /** 
     * Returns an enumeration of the module file entries with the
     * specified prefix.  All elements in the enumeration are of 
     * type String.  Each String represents a file name relative 
     * to the root of the module. 
     * 
     * @param prefix the prefix of entries to be included
     * @return an enumeration of the archive file entries. 
     */ 
    public Enumeration entries(String prefix) {
        Enumeration allEntries = entries();
        Vector entries = new Vector();
        while (allEntries.hasMoreElements()) {
            String name = (String) allEntries.nextElement();
            if (name != null && name.startsWith(prefix)) {
                entries.add(name);
            }
        }
        return entries.elements();
    }
}

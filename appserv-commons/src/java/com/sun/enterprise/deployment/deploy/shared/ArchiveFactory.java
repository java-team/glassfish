/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.IOException;
import java.net.URI;
import java.net.MalformedURLException;
import java.io.File;

/**
 * This implementation of the AbstractArchiveFactory interface
 * is capable of creating the right abstraction of the Archive 
 * interface depending on the protocol used in the URL.
 *
 * @author Jerome Dochez
 */
public class ArchiveFactory implements AbstractArchiveFactory {
    
    /** Creates a new instance of ArchiveFactory */
    public ArchiveFactory() {
    }
    
            
    public AbstractArchive createArchive(String path) throws java.io.IOException {
        try {
            /*
             *Use the expanded constructor so illegal characters (such as embedded blanks) in the path 
             *will be encoded.
             */
            return createArchive(prepareArchiveURI(path));
        } catch(java.net.URISyntaxException e) {
            return null;
        }
    }
    
    public AbstractArchive openArchive(String path) throws java.io.IOException {
        try {
            return openArchive(prepareArchiveURI(path));
        } catch(java.net.URISyntaxException e) {
            return null;
        }
    }
    
    /**
     * Creates a new archivist using the URL as the path. The URL 
     * protocol will define the type of desired archive (jar, file, etc)
     * @param url to the archive
     * @return the apropriate archive
     */
    public AbstractArchive createArchive(URI path) throws IOException {
        
        String protocol = path.getScheme();
        if (protocol.equals("file")) {
            FileArchive output = new FileArchive();
            output.create(path.getPath());        
            return output;             
        } else 
        if (protocol.equals("jar")) {
            OutputJarArchive ja =  new OutputJarArchive();
            ja.create(path.getPath());
            return ja;            
        } else 
        throw new MalformedURLException("Protocol not supported : " + protocol);        
    }
    
    /**
     * Opens an existing archivist using the URL as the path. 
     * The URL protocol will defines the type of desired archive 
     * (jar, file, memory, etc...) 
     * @param url to the existing archive
     * @return the appropriate archive 
     */
    public AbstractArchive openArchive(URI path) throws IOException {
        
        String protocol = path.getScheme();
        if (protocol.equals("file")) {
            FileArchive input = new FileArchive();
            input.open(path.getPath());        
            return input;            
        } else 
        if (protocol.equals("jar")) {
            InputJarArchive ja = new InputJarArchive();
            ja.open(path.getPath());
            return ja;           
        } else
/*        if (protocol.equals("mem")) {
            MemoryMappedArchive mapped = new MemoryMappedArchive();
            mapped.open(path.getPath());
            return mapped;            
        }*/
        throw new MalformedURLException("Protocol not supported : " + protocol);    
    }
    
    /**
     *Create a URI for the jar specified by the path string.
     *<p>
     *The steps used here correctly encode "illegal" characters - such as embedded blanks - in 
     *the path string that otherwise would render the URI unusable.  The URI constructor that
     *accepts just the path string does not perform this encoding.
     *@param path string for the archive
     *@return URI with any necessary encoding of special characters
     */
    static java.net.URI prepareArchiveURI(String path) throws java.net.URISyntaxException, java.io.UnsupportedEncodingException, java.io.IOException {
       
        File archiveFile = new File(path);
        URI archiveURI = archiveFile.toURI();
        String scheme = (archiveFile.isDirectory() ? "file" : "jar");
        URI answer = new URI(scheme, null /* authority */, archiveURI.getPath(), null /* query */, null /* fragment */);
        return answer;
    }
}

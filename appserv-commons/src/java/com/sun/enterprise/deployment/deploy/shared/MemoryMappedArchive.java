/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * MemoryMappedArchive.java
 *
 * Created on September 6, 2002, 2:58 PM
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URI;

import java.util.Vector;
import java.util.Enumeration;

import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.util.shared.ArchivistUtils;

/**
 *
 * @author  Jerome Dochez
 */
public class MemoryMappedArchive extends AbstractArchive {
    
    byte[] file;
    
    /** Creates a new instance of MemoryMappedArchive */
    protected MemoryMappedArchive() {
	// for use by subclasses
    }

    /** Creates a new instance of MemoryMappedArchive */
    public MemoryMappedArchive(InputStream is) throws IOException {
        read(is);
    }

    public byte[] getByteArray() {
        return file;
    }
    
    private void read(InputStream is) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ArchivistUtils.copy(new BufferedInputStream(is), new BufferedOutputStream(baos));
        file = baos.toByteArray();
        
    }
    
    public void open(String path) throws IOException {
        File in = new File(path);
        if (!in.exists()) {
            throw new FileNotFoundException(path);            
        }
        FileInputStream is = new FileInputStream(in);
        read(is);
    }
    
    // copy constructor
    public MemoryMappedArchive(AbstractArchive source) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JarOutputStream jos = new JarOutputStream(new BufferedOutputStream(baos));
        for (Enumeration elements = source.entries();elements.hasMoreElements();) {
            String elementName = (String) elements.nextElement();
            InputStream is = source.getEntry(elementName); 
            jos.putNextEntry(new ZipEntry(elementName));
            ArchivistUtils.copyWithoutClose(is, jos);            
            is.close();
            jos.flush();
            jos.closeEntry();
        }
        jos.close();
        file = baos.toByteArray();            
    }
    
    /**
     * @returns an @see java.io.OutputStream for a new entry in this
     * current abstract archive.
     * @param the entry name
     */
    public OutputStream putNextEntry(String name) throws IOException {
        return null;
    }
    
    /**
     * close the abstract archive
     */
    public void close() throws IOException {
    }
    
    /**
     * close a previously returned @see java.io.OutputStream returned
     * by an addEntry call
     *
     * @param the output stream to close
     */
    public void closeEntry(AbstractArchive os) throws IOException {
    }
    
    /**
     * close a previously returned @see java.io.OutputStream returned
     * by an addEntry call
     */
    public void closeEntry() throws IOException {
    }
    
    /**
     * delete the archive
     */
    public boolean delete() {
        return false;
    }
    
    /**
     * @return an @see java.util.Enumeration of entries in this abstract
     * archive
     */
    public Enumeration entries() {
        Vector entries = new Vector();
        try {
            JarInputStream jis = new JarInputStream(new ByteArrayInputStream(file));
            ZipEntry ze;
            while ((ze=jis.getNextEntry())!=null) {
                entries.add(ze.getName());
            }
            jis.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        return entries.elements();        
    }
    
	/**
	 *  @return an @see java.util.Enumeration of entries in this abstract
	 * archive, providing the list of embedded archive to not count their 
	 * entries as part of this archive
	 */
	 public Enumeration entries(Enumeration embeddedArchives) {
	// jar file are not recursive    
	return entries();
	}
    
    /**
     * @return true if this archive exists
     */
    public boolean exists() {
        return false;
    }
    
    /**
     * @return the archive uri
     */
    public String getArchiveUri() {
        return null;
    }
    
    /**
     * Get the size of the archive
     * @return tje the size of this archive or -1 on error
     */
    public long getArchiveSize() throws NullPointerException, SecurityException {
        return(file.length);
    }
    
    public URI getURI() {
        return null;
    }
    
    /**
     * create or obtain an embedded archive within this abstraction.
     *
     * @param the name of the embedded archive.
     */
    public AbstractArchive getEmbeddedArchive(String name) throws IOException {
        InputStream is = getEntry(name);
        if (is!=null) {
            AbstractArchive archive = new MemoryMappedArchive(is);
            is.close();
            return archive;
        }
        return null;
    }
    
    /**
     * @return a @see java.io.InputStream for an existing entry in
     * the current abstract archive
     * @param the entry name
     */
    public InputStream getEntry(String name) throws IOException {
        JarInputStream jis = new JarInputStream(new ByteArrayInputStream(file));
        ZipEntry ze;
        while ((ze=jis.getNextEntry())!=null) {
            if (ze.getName().equals(name)) 
                return new BufferedInputStream(jis);
        }
        return null;        
    }
    
    /**
     * @return the manifest information for this abstract archive
     */
    public Manifest getManifest() throws IOException {
        JarInputStream jis = new JarInputStream(new ByteArrayInputStream(file));
        Manifest m = jis.getManifest();
        jis.close();
        return m;
    }
    
    /**
     * rename the archive
     *
     * @param name the archive name
     */
    public boolean renameTo(String name) {
        return false;
    }        
    
}

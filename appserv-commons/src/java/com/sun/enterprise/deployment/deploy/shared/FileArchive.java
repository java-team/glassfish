/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.deploy.shared;

import java.io.*;
import java.util.Vector;
import java.util.Enumeration;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.jar.Manifest;
import java.util.jar.JarFile;
import java.net.URI;

import com.sun.enterprise.util.io.FileUtils;
import com.sun.enterprise.deployment.deploy.shared.Archive;

/**
 * This implementation of the AbstractArchive interface maps to a directory/file
 * structure.
 *
 * @author Jerome Dochez
 */
public class FileArchive extends AbstractArchive {
  
    // the archive abstraction directory.
    File archive;
    String path;
    
    // the currently opened entry
    OutputStream os=null;
    
    /** Creates a new instance of FileArchive */
    public FileArchive() {
    }
    
    /** 
     * Open an abstract archive
     * @param the path to the archive
     */
    public void open(String path) throws IOException {    
        this.path = path.replace('/', File.separatorChar);
        archive = new File(path);
        if (!archive.exists()) {
            throw new FileNotFoundException(path);
        }
    }
    
    /**
     * Get the size of the archive
     * @return tje the size of this archive or -1 on error
     */
    public long getArchiveSize() throws NullPointerException, SecurityException {
        if(getArchiveUri() == null) {
            return -1;
        }
        File tmpFile = new File(getArchiveUri());
        return(tmpFile.length());
    }
    
    /** 
     * creates a new abstract archive with the given path
     * @param the path to create the archive
     */
    public void create(String path) throws IOException {    
        this.path = path.replace('/', File.separatorChar);
        archive = new File(path);
        archive.mkdirs();
    }
        
    /**
     * close the abstract archive
     */
    public void close() throws IOException {
        // nothing to do 
    }
    
    /**
     * close a previously returned @see java.io.OutputStream returned
     * by an addEntry call
     *
     * @param the output stream to close
     */
    public void closeEntry(AbstractArchive os) throws IOException {
        os.close();
    }
        
    /**
     * delete the archive
     */
    public boolean delete() {
        // delete the directory structure...
        try {
            return deleteDir(archive);
        } catch (IOException e) {
            return false;
        }
    }
    
    /**
     * @return an @see java.util.Enumeration of entries in this abstract
     * archive
     */
    public Enumeration entries() {
        Vector namesList = new Vector();
        getListOfFiles(archive, namesList, null);
        return namesList.elements();
    }
    
    /**
     *  @return an @see java.util.Enumeration of entries in this abstract
     * archive, providing the list of embedded archive to not count their 
     * entries as part of this archive
     */
     public Enumeration entries(Enumeration embeddedArchives) {
     	Vector nameList = new Vector();
        List massagedNames = new ArrayList();
	while (embeddedArchives.hasMoreElements()) {
		String subArchiveName  = (String) embeddedArchives.nextElement();
		massagedNames.add(FileUtils.makeFriendlyFileName(subArchiveName));
	}        
     	getListOfFiles(archive, nameList, massagedNames);
     	return nameList.elements();
     }

    /** 
     * Returns an enumeration of the module file entries with the
     * specified prefix.  All elements in the enumeration are of 
     * type String.  Each String represents a file name relative 
     * to the root of the module. 
     * 
     * @param prefix the prefix of entries to be included
     * @return an enumeration of the archive file entries. 
     */ 
    public Enumeration entries(String prefix) {
        prefix = prefix.replace('/', File.separatorChar);
        File file = new File(archive, prefix);
        Vector namesList = new Vector();
        getListOfFiles(file, namesList, null);
        return namesList.elements();
    }
    
    /**
     * @return true if this archive exists
     */
    public boolean exists() {
        return archive.exists();
    }
    
    /**
     * @return the archive uri
     */
    public String getArchiveUri() {
        return path;
    }
    
    /**
     * create or obtain an embedded archive within this abstraction.
     *
     * @param the name of the embedded archive.
     */
    public AbstractArchive getEmbeddedArchive(String name) throws IOException {
       // Convert name to native form. See bug #6345029 for more details.
       name = name.replace('/', File.separatorChar);
       File file = new File(name);
       File subDir;
       if (file.isAbsolute()) {
           subDir = file;
       } else {
           // first we try to see if a sub directory with the right file
           // name exist
           subDir = new File(archive, FileUtils.makeFriendlyFileName(name));
       	   if (!subDir.exists()) {       	  
               // now we try to open a sub jar file...
               subDir = new File(archive, name);
               if (!subDir.exists()) {
                   // ok, nothing worked, reassing the name to the 
                   // sub directory one
                  subDir = new File(archive, FileUtils.makeFriendlyFileName(name));
              }                  
       	   }
       }
       String subName = subDir.getPath();
       if (!subDir.exists()) {
           // time to create a new sub directory
           File newDir = new File(subName);
           newDir.mkdirs();           
       }
       AbstractArchive sub;
       if (subDir.isDirectory()) {
       	    sub = new FileArchive();
            ((FileArchive) sub).open(subName);
       } else {
       	    sub = new InputJarArchive();
       	    ((InputJarArchive) sub).open(subName);
       }
       return sub;
    }
    
    /**
     * @return a @see java.io.InputStream for an existing entry in
     * the current abstract archive
     * @param the entry name
     */
    public InputStream getEntry(String name) throws IOException {
            
        name = name.replace('/', File.separatorChar);
        File input = new File(archive, name);
        if (!input.exists() || input.isDirectory()) {
            return null;
        }
        FileInputStream fis = new FileInputStream(input);
        try {
            BufferedInputStream bis = new BufferedInputStream(fis);
            return bis;
        } catch (Throwable tx) {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Throwable thr) {
                    IOException ioe = new IOException("Error closing FileInputStream after error opening BufferedInputStream for entry " + name);
                    ioe.initCause(thr);
                    throw ioe;
                }
            }
            IOException ioe = new IOException("Error opening BufferedInputStream for entry " + name);
            ioe.initCause(tx);
            throw ioe;
        }
    }

    /**
     * @return the manifest information for this abstract archive
     */
    public Manifest getManifest() throws IOException {
        InputStream is = null;
        try {
            is = getEntry(JarFile.MANIFEST_NAME);
            if (is!=null) {
                Manifest m = new Manifest(is);
                return m;
            }
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return null;
    }
    
    /**
     * rename the archive
     *
     * @param name the archive name
     */
    public boolean renameTo(String name) {
        return FileUtils.renameFile(archive, new File(name));
    }
    
    
    /**
     * utility method for deleting a directory and all its content
     */
    private boolean deleteDir(File directory) throws IOException {
        if (!directory.isDirectory()) {
            throw new FileNotFoundException(directory.getPath());
        }
        
        // delete contents
        File[] entries = directory.listFiles();
        for (int i=0;i<entries.length;i++) {
            if (entries[i].isDirectory()) {
                deleteDir(entries[i]);
            } else {
                FileUtils.deleteFile(entries[i]);
            }
        }
        // delete self
        return FileUtils.deleteFile(directory);
    } 
    
    /**
     * utility method for getting contents of directory and 
     * sub directories
     */
    private void getListOfFiles(File directory, Vector files, List embeddedArchives) {
        File[] list = directory.listFiles();
        if (list == null) {
            return;
        }
        for (int i=0;i<list.length;i++) {
	    String fileName = list[i].getAbsolutePath().substring(archive.getAbsolutePath().length()+1);
            if (!list[i].isDirectory()) {                
                fileName = fileName.replace(File.separatorChar, '/');
                if (!fileName.equals(JarFile.MANIFEST_NAME)) {
                    files.add(fileName);
                }
            } else {
		if (embeddedArchives!=null) {
			if (!embeddedArchives.contains(fileName)) {
				getListOfFiles(list[i], files, null);
			}            		
		} else {
	                getListOfFiles(list[i], files, null);
		}
            }
        }
    }          
    
    /** @return true if this archive abstraction supports overwriting of elements
     *
     */
    public boolean supportsElementsOverwriting() {
        return true;
    }
    
    /** delete an entry in the archive
     * @param the entry name
     * @return true if the entry was successfully deleted
     *
     */
    public boolean deleteEntry(String name) {
        name = name.replace('/', File.separatorChar);
        File input = new File(archive, name);
        if (!input.exists()) {
            return false;
        }
        return input.delete();
    }

    /**
     * Closes the current entry
     */
    public void closeEntry() throws IOException {
        if (os!=null) {
            os.flush();
            os.close();
            os = null;
        }
    }
    
    public URI getURI() {
        return archive.toURI();
    }
    
    /**
     * @returns an @see java.io.OutputStream for a new entry in this
     * current abstract archive.
     * @param the entry name
     */    
    public OutputStream putNextEntry(String name) throws java.io.IOException {
        name = name.replace('/', File.separatorChar);
        
        File newFile = new File(archive, name);
        if (newFile.exists()) {
            if (!deleteEntry(name))
                throw new IOException(name + " already exists and cannot be deleted");
        }
        // if the entry name contains directory structure, we need
        // to create those directories first.
        if (name.lastIndexOf(File.separatorChar)!=-1) {            
            String dirs = name.substring(0, name.lastIndexOf(File.separatorChar));            
            (new File(archive, dirs)).mkdirs();
        }
        os = new BufferedOutputStream(new FileOutputStream(newFile));
        return os;   
    }
    
}

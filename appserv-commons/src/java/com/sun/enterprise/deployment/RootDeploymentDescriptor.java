/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package com.sun.enterprise.deployment;

import java.util.*;
import javax.enterprise.deploy.shared.ModuleType;
import java.util.logging.*;
import com.sun.enterprise.deployment.util.DOLUtils;

/**
 * This descriptor contains all common information amongst root element 
 * of the J2EE Deployment Descriptors (application, ejb-jar, web-app, 
 * connector...). 
 *
 * @author Jerome Dochez
 */
public abstract class RootDeploymentDescriptor extends Descriptor {
    
    /**
     * each module is uniquely identified with a moduleID
     */
    protected String moduleID;
    
    /**
     * version of the specification loaded by this descriptor
     */
    private String specVersion;
    
    /**
     * class loader associated to this module to load classes 
     * contained in the archive file
     */
    protected transient ClassLoader classLoader = null;

    /**
     * key is the URI representing PURoot.
     */
    protected Map<String, PersistenceUnitsDescriptor> persistenceUnitsDescriptors
            = new HashMap<String, PersistenceUnitsDescriptor>();

    /**
     * Construct a new RootDeploymentDescriptor 
     */
    public RootDeploymentDescriptor() {
        super();
    }
    
    /**
     * Construct a new RootDeploymentDescriptor with a name and description
     */
    public RootDeploymentDescriptor(String name, String description) {
        super(name, description);
    }
    
    /**
     * each module is uniquely identified with a moduleID
     * @param moduleID for this module
     */
    public void setModuleID(String moduleID) {
        this.moduleID = moduleID;
    }
    
    /**
     * @return the module ID for this module descriptor
     */
    public abstract String getModuleID();

    /**
     * @return the default version of the deployment descriptor
     * loaded by this descriptor
     */
    public abstract String getDefaultSpecVersion();

        
    /**
     * @return the specification version of the deployment descriptor
     * loaded by this descriptor
     */
    public String getSpecVersion() {
        if (specVersion == null) {
            specVersion = getDefaultSpecVersion();
        } 
        try {
            Double.parseDouble(specVersion); 
        } catch (NumberFormatException nfe) {
            DOLUtils.getDefaultLogger().log(Level.WARNING, "invalidSpecVersion",
                new Object[] {specVersion, getDefaultSpecVersion()});
            specVersion = getDefaultSpecVersion();
        }

        return specVersion;
    }
    
    /**
     * Sets the specification version of the deployment descriptor
     * @param specVersion version number
     */
    public void setSpecVersion(String specVersion) {
        this.specVersion = specVersion;
    }      
    
    /**
     * @return the module type for this bundle descriptor
     */
    public abstract ModuleType getModuleType();

    /**
     * Sets the class loader for this application
     */
    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
    
    /**
     * @return the class loader associated with this module
     */
    public abstract ClassLoader getClassLoader();

    /**
     * sets the display name for this bundle
     */
    public void setDisplayName(String name) {
        super.setName(name);
    }
    
    /** 
     * @return the display name
     */
    public String getDisplayName() {
        return super.getName();
    }
    
    /**
     * as of J2EE1.4, get/setName are deprecated, 
     * people should use the set/getDisplayName or 
     * the set/getModuleID.
     */
    public void setName(String name) {
        setModuleID(name);
    }
    
    /**
     * as of J2EE1.4, get/setName are deprecated, 
     * people should use the set/getDisplayName or 
     * the set/getModuleID.
     * note : backward compatibility
     */     
    public String getName() {
        if (getModuleID()!=null) {
            return getModuleID();
        } else {
            return getDisplayName();
        }
    }
        
    public void setSchemaLocation(String schemaLocation) {
        addExtraAttribute("schema-location", schemaLocation);
    }
    
    public String getSchemaLocation() {
        return (String) getExtraAttribute("schema-location");
    }
    
    /**
     * @return true if this module is an application object 
     */
    public abstract boolean isApplication();
    
    /**
     * print a meaningful string for this object
     */
    public void print(StringBuffer toStringBuffer) {
        super.print(toStringBuffer);
        toStringBuffer.append("\n Module Type = ").append(getModuleType());
        toStringBuffer.append("\n Module spec version = ").append(getSpecVersion());
        if (moduleID!=null) 
            toStringBuffer.append("\n Module ID = ").append(moduleID);
        if (getSchemaLocation()!=null)
            toStringBuffer.append("\n Client SchemaLocation = ").append(getSchemaLocation());
    }

    /**
     * This method returns PersistenceUnitDescriptor objects in the
     * scope of this RootDeploymentDescriptor.
     * @return it returns an unmodifiable collection.
     * returns an empty collection if there is no PersistenceUnitDescriptor.
     */
    public Collection<PersistenceUnitsDescriptor> getPersistenceUnitsDescriptors() {
        return Collections.unmodifiableCollection(persistenceUnitsDescriptors.values());
    }

    /**
     * This method returns PersistenceUnitDescriptor object with matching
     * PURoot in the scope of this RootDeploymentDescriptor.
     * @param puRoot used for lookup
     * @return return the PersisteneUnitsDescriptor for the given PURoot.
     * It returns null if not found.
     */
    public PersistenceUnitsDescriptor getPersistenceUnitsDescriptor(
            String puRoot) {
        return persistenceUnitsDescriptors.get(puRoot);
    }

    /**
     * Add deplyoment information about all the persistence units
     * defined in a persistence.xml to this RootDeploymentDescriptor.
     * All the persistence units  defined inside the same persistence.xml
     * will be added with same PURoot. This method also sets the parent
     * reference in PersistenceUnitsDescriptor.
     * @param puRoot root of the persistence unit (its a relative path)
     * @param persistenceUnitsDescriptor is the descriptor object
     * representing the persistence unit.
     */
    public void addPersistenceUnitsDescriptor(String puRoot, PersistenceUnitsDescriptor persistenceUnitsDescriptor) {
        // We don't expect the parent to be already set
        // because that indicates transfer of ownership.
        assert(persistenceUnitsDescriptor.getParent() == null);
        persistenceUnitsDescriptor.setParent(this);
        persistenceUnitsDescriptor.setPuRoot(puRoot);
        persistenceUnitsDescriptors.put(puRoot, persistenceUnitsDescriptor);
    }

}


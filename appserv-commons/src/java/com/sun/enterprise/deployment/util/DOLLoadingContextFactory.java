/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.reflect.Method;

import com.sun.enterprise.deployment.WebBundleDescriptor;
import com.sun.logging.LogDomains;

public class DOLLoadingContextFactory {
    private static WebBundleDescriptor defaultWebXMLWbd = null;
    private static boolean isParsingDefaultWebXML = false;
    private static boolean isDefaultWebXMLInitialized = false;
    
    private static Class dolLoadingContext = null;
    private static final String DLC_CLASS_NAME = 
        "com.sun.enterprise.deployment.backend.DOLLoadingContext";
    private static final String DLC_METHOD_NAME = 
        "initDefaultWebBundleDescriptor";

    private static Logger _logger = 
        LogDomains.getLogger(LogDomains.DPL_LOGGER);

    private static Class getDOLLoadingContext () {
        try {
            if (dolLoadingContext == null) {
                dolLoadingContext = Class.forName(DLC_CLASS_NAME, 
                    true, Thread.currentThread().getContextClassLoader());
            }
        } catch (ClassNotFoundException cnfe) {
            _logger.log(Level.WARNING, "enterprise.deployment.class.not.found", 
                 new Object[] {DLC_CLASS_NAME});
        }

        return dolLoadingContext;
    }   

    /**
     * @return a copy of default WebBundleDescriptor populated from
     * default-web.xml
     */
    public static WebBundleDescriptor getDefaultWebBundleDescriptor() {
        initDefaultWebBundleDescriptor();

        // when default-web.xml exists, add the default bundle descriptor
        // as the base web bundle descriptor
        WebBundleDescriptor defaultWebBundleDesc = 
            new WebBundleDescriptor();
        if (defaultWebXMLWbd != null) {
            defaultWebBundleDesc.addWebBundleDescriptor(defaultWebXMLWbd);
        }
        return defaultWebBundleDesc;
    }

    /**
     * initialize the default WebBundleDescriptor from
     * default-web.xml
     */
    private static synchronized void initDefaultWebBundleDescriptor() {
        // it's possible after we attempt to initialize defaultXMLWbd, it 
        // is still null, for instance, when default-web.xml does not exist
        // for these cases, we don't want to call initialize again
        if (defaultWebXMLWbd == null && !isDefaultWebXMLInitialized) { 
            // delegate to DOLLoadingContext 
            try {
                Class dlcClass = getDOLLoadingContext(); 
                Method initWbdMethod = 
                    dlcClass.getMethod(DLC_METHOD_NAME, new Class[0]); 
                if (initWbdMethod != null) {
                    defaultWebXMLWbd = 
                        (WebBundleDescriptor)initWbdMethod.invoke(dlcClass, 
                        new Object[0]);
                } else {
                    _logger.log(Level.WARNING, 
                         "enterprise.deployment.method.not.found", 
                      new Object[] {DLC_METHOD_NAME});
                }
            } catch (Exception e) { 
                _logger.log(Level.WARNING, e.getMessage());
                defaultWebXMLWbd = null;
            } finally {
                isDefaultWebXMLInitialized = true;
            }
        }
    }

    /**
     * @return the flag to indicate whether we are parsing the
     * default-web.xml
     */
    public static boolean isParsingDefaultWebXML() {
        return isParsingDefaultWebXML;
    }

    /**
     *  sets the flag to indicate whether we are parsing the
     *  default-web.xml
     *  @param isDefault
     */
    public static void setParsingDefaultWebXML(boolean isDefault) {
        isParsingDefaultWebXML = isDefault;
    }
}

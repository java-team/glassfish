/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.deployment.util;

import com.sun.enterprise.deployment.annotation.introspection.ClassFile;
import com.sun.enterprise.deployment.annotation.introspection.ConstantPoolInfo;
import com.sun.enterprise.deployment.annotation.introspection.CustomAnnotationScanner;
import com.sun.enterprise.deployment.annotation.introspection.EjbComponentAnnotationScanner;
import com.sun.enterprise.deployment.deploy.shared.AbstractArchive;
import com.sun.enterprise.deployment.deploy.shared.FileArchive;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Abstract superclass for specific types of annotation detectors.
 *
 * @author Qingqing Ouyang
 * @author tjquinn
 */
public abstract class AnnotationDetector {
    
    private ClassFile classFile = new ClassFile();

    public AnnotationDetector() {
        CustomAnnotationScanner scanner = createAnnotationScanner();
        ConstantPoolInfo poolInfo = new ConstantPoolInfo(scanner);
        classFile.setConstantPoolInfo(poolInfo);
    }

    /**
     * Provides a new annotation scanner, tailored to the particular type of
     * annotation detector the concrete subclass implements.
     * @return an implementation of CustomAnnotationScanner to be used by the detector
     */
    protected abstract CustomAnnotationScanner createAnnotationScanner();
    
    public boolean hasAnnotationInArchive(AbstractArchive archive) throws IOException {
        File file = new File(archive.getArchiveUri());
        if (!file.exists()) {
            throw new FileNotFoundException(archive.getArchiveUri());
        }

        //@@@ note that the two methods could probably be combined into one.
        //The challenge is to find the size of each entry without having to
        //read the entry twice, once for the inputstream, the other for size.
        if (file.isDirectory()) {
            return hasAnnotationInDirectory(archive);
        } else {
            return hasAnnotationInJar(archive);
        }
    }

    /**
     *@return true if the jar file contains any of the appropriate type of annotations
     */
    private boolean hasAnnotationInJar(AbstractArchive archive) throws IOException {
        JarFile jf = null;
        try {
            jf = new JarFile(new File(archive.getArchiveUri()));
            Enumeration<JarEntry> entriesEnum = jf.entries();
            while(entriesEnum.hasMoreElements()) {
                JarEntry je = entriesEnum.nextElement();
                if (je.getName().endsWith(".class")) {
                    if (containsAnnotation(jf, je)) {
                        return true;
                    }
                }
            }
        } finally {
            if (jf != null) {
                jf.close();
            }
        }
        return false;
    }

    /**
     *@return true if the directory contains any of the appropriate type of annotations
     */
    private boolean hasAnnotationInDirectory(AbstractArchive archive) throws IOException {
        Enumeration entriesEnum = archive.entries();
        while(entriesEnum.hasMoreElements()) {
            String entry = (String) entriesEnum.nextElement();
            if (entry.endsWith(".class")) {
                File file = null;
                int ind = entry.lastIndexOf("/");
                if (ind != -1) {
                    String entryName = entry.substring(ind + 1);
                    String parent = archive.getArchiveUri() + File.separatorChar + 
                                    entry.substring(0, ind);
                    file  = new File(parent.replace('/', File.separatorChar), entryName);
                } else {
                    file = new File(archive.getArchiveUri(), entry);
                }
                if (containsAnnotation(file)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean containsAnnotation(JarFile jarFile, JarEntry entry) throws IOException {
        boolean result = false;
        // check if it contains top level annotations...
        ReadableByteChannel channel = null;
        try {
            channel = Channels.newChannel(jarFile.getInputStream(entry));
            if (channel!=null) {
                result = classFile.containsAnnotation(channel, entry.getSize());
             }
             return result;
        } finally {
            if (channel != null) {
                channel.close();
            }
        }
    }
    
    public boolean containsAnnotation(File file) throws FileNotFoundException, IOException {
        boolean result = false;
        // check if it contains top level annotations...
        InputStream is = null;
        try {
            is = new BufferedInputStream(new FileInputStream(file));
            ReadableByteChannel channel = Channels.newChannel(is);
            if (channel!=null) {
                result = classFile.containsAnnotation(channel, file.length());
            }
            return result;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
}

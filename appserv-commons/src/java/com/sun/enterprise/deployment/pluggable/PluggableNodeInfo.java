/*
 * PluggableNodeInfo.java
 * 
 * Created on Oct 12, 2007, 7:58:35 PM
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.enterprise.deployment.pluggable;

import java.util.HashMap;

/**
 *
 * @author Prasad Subramanian
 */
public class PluggableNodeInfo {
    
    private static HashMap<String, String> nodeMap = null;
   
    public synchronized static void registerBundleNode(String tag, String bundleNodeClassName){
        if(nodeMap == null) {
            nodeMap = new HashMap<String, String>();
        }
        nodeMap.put(tag, bundleNodeClassName);
        //FIX ME : Add a logger call
        //System.out.println("Added to nodeMap"+nodeMap.toString());
    }
    
    public static HashMap<String, String> getRegisteredBundleNodes() {
        return nodeMap;
    }

}

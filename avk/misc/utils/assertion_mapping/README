Author:
-------
Sanjeeb.Sahoo@Sun.COM/27 May 2004

What is it?
-----------
A simple helper class that generates test mapping information in XML format from LocalStrings.properties.
It also has an option to convert the XML file into an HTML document using a style sheet. 
An example style sheet by name assertion_doc_style.xml is located in this dir.
This class and the stylesheet is used in the doc build process. 

Requirements:
-------------
Requires J2SE 1.4.2 or above at both compile time and runtime.

How To Use?
-----------
To compile, just run the following command...
javac LocalStringsHelper.java

To generate mapping information for each component separately, run the following command in C shell.
foreach comp (app appclient connector ejb web webservices wsclients)
java LocalStringsHelper -map LocalStrings.properties com.sun.enterprise.tools.verifier.tests.${comp}. ${comp}_assertions.xml
end

To generate mapping information for all components, run the following command
java LocalStringsHelper -map LocalStrings.properties com.sun.enterprise.tools.verifier.tests. assertions.xml

To convert the XML to HTML, run
java LocalStringsHelper -transform assertions.xml assertion_doc_style.xml assertions.html

Combining both, if you want to generate mapping and html for each component, run
foreach comp (app appclient connector ejb web webservices wsclients)
java LocalStringsHelper -map LocalStrings.properties com.sun.enterprise.tools.verifier.tests.${comp}. ${comp}_assertions.xml
java LocalStringsHelper -transform ${comp}_assertions.xml assertion_doc_style.xml ${comp}_assertions.html
end

In the above command, see the '.' at the end of the package name. Thatis important for the XSl transformation
to take place correctly.

Feel free to use it and enhace. Don't forget to update the doc.

Thank you,
Sahoo


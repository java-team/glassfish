/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * Copyright 2004-2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
 
/*
 */

package com.sun.enterprise.management.support;

import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.management.ObjectName;

import com.sun.appserv.management.util.misc.ExceptionUtil;
import com.sun.appserv.management.util.jmx.JMXUtil;
	
/**
    MBean registration and unregistration thread.  Maintains a queue of items
    to be processed sequentially by the thread.
    <p>
    A deceptively simple class, it is very straightforward for ordinary processing. However
    the case of thread-exit is much more complicated to do right, but it is not a necessary case
    to support.
    <p>
    A more complex implementation could be used, including a ShutdownHook which gracefully
    shuts down the thread.  The complexity is significantly higher (it was coded and tested),
    but  it appears to be of no value in the context of the overall shutdown process,
    which seems to end up killing the JVM in the end anyway.  So the simpler appraoch is used:
    a daemon thread which never exits is used to guarantee that the queue will never block
    indefinitely.  This fixes Esc#1-23761179, CR 6702146.
 */
final class LoaderRegThread extends Thread
{
    /**
    Please leave debug code in place; debug() statements are handy in the code below.
    */
    private static void debug(final Object... args)
    {
        //final String s = com.sun.appserv.management.util.misc.StringUtil.toString( ", ", args);
        //System.out.println( ">>>>> " + s);
    }
    
	private final Logger mLogger;
    
    ///** items to be be processed of size MAX_QUEUE_SIZE */
	//private final ArrayBlockingQueue<QueueItem>	mQueue;
	private final LinkedBlockingQueue<QueueItem>	mQueue;
    
    /** callback for handling register/unregister */
	private final LoaderRegHandler          mRegHandler;
        
		private
	LoaderRegThread(
		final LoaderRegHandler	regHandler,
		final Logger	logger )
	{
		mRegHandler         = regHandler;
		mLogger             = logger;

		// mQueue		= new ArrayBlockingQueue<QueueItem>( MAX_QUEUE_SIZE );
        // no size limit to avoid shutdown deadlock due to recursion
		mQueue		= new LinkedBlockingQueue<QueueItem>();
        
        // allows the JVM to "drop it on the floor" -- just fine for this thread
        // INVARIANT: thread never dies, see processForever().
        setDaemon(true);
	}
    
		public static LoaderRegThread
	createInstance(
		final LoaderRegHandler	regHandler,
		final Logger	logger )
	{
        final LoaderRegThread t = new LoaderRegThread( regHandler, logger);
        //Runtime.getRuntime().addShutdownHook( new LoaderRegThreadShutdownHook(t) );
        
        return t;
    }
	
	private final Logger getLogger( ) { return mLogger; }

		public void
	enqueueAll(
		final boolean           register,
		final List<ObjectName>	candidates )
        throws InterruptedException
	{
        for( final ObjectName objectName : candidates )
		{
			enqueue( register, objectName );
		}
	}
	
    /**
        Queuing an item would be trivial if it weren't for the fact that at system shutdown
        clients continue to enqueue items before during and after the thread exits. 
        This causes two problems:<br>
        <ul>
        <li>The queue could become full which blocks the caller;
            it will never be unblocked if the thread has exited</li>
        <li>Callers could block in waitAll() and never be unblocked</li>
        </ul>
     */
		public void
	enqueue(
		final boolean		register,
		final ObjectName	theObject )
        throws InterruptedException
	{
        final MBeanItem item = new MBeanItem( theObject, register );
        //debug( "MBeanItem", item.mObjectName, register, mQueue.size() );
        mQueue.put( item );
	}
	
    /**
        Wait for all outstanding requests to be finished.
        <p>
        Some callers need to wait until items are processed (eg creating config has synchronous
        semantics). The caller might "pick up" some
        additional items between submitting its last item and waiting, this is fine.
     */
		public void
	waitAll()
        throws InterruptedException
	{
        // insert an item into the queue.  When done, all previous items have been handled also.
        final WaitDoneItem waiter = new WaitDoneItem();
        mQueue.put(waiter);
        waiter.await();
	}

		private void
	processRegistration( final ObjectName objectName )
	{
		try
		{
			mRegHandler.handleMBeanRegistered( objectName );
			getLogger().finer( "LoaderRegThread.processRegistration: processed mbean: " + objectName );
		}
		catch( Throwable t )
		{
			getLogger().warning( "LoaderRegThread.processRegistration: " +
				"registration of MBean failed for: " + 
				objectName + " = " + t.toString() + ", " + t.getMessage() + "\n" +
				ExceptionUtil.getStackTrace( t ) );
		}
	}
	
		private void
	processUnregistration( final ObjectName objectName )
	{
		try
		{
			mRegHandler.handleMBeanUnregistered( objectName );
		}
		catch( Throwable t )
		{
			getLogger().warning( "LoaderRegThread.processUnregistration: " +
				"unregistration of MBean failed for: " + 
				objectName + " = " + t.toString() );
		}
	}
        
        public boolean
    isQueueEmpty()
    {
        return mQueue.size() == 0;
    }
	
        private void
    processItem( final QueueItem item )
    {
        try
        {
            if ( item instanceof MBeanItem )
            {
                final MBeanItem mb = (MBeanItem)item;
                if ( mb.mRegister )
                {
                    processRegistration( mb.mObjectName );
                }
                else
                {
                    processUnregistration( mb.mObjectName );
                }
            }
            else
            {
                // nothing to do for MarkerItems
            }
        }
        finally
        {
            // it is critical to unblock threads, this must be in the 'finally' block
            item.countDown();
        }
    }

    private void logExceptionAsWarning( final Throwable t )
    {
        getLogger().warning( ExceptionUtil.toString(t) );
    }
    
    /**
        Forever!!!  The thread MUST NOT EXIT, or at shutdown a full queue could end up blocking
        other threads (at shutdown large numbers of MBeans can be unregistered, naming Servlets).
     */
		private void
	processFOREVER()
	{
		while ( true )
		{
            // block waiting for items to appear
            try
            {
                final QueueItem item = mQueue.take();
                // process this item, then grab the remaining ones below
                processItem(item);
            }
            catch( final Throwable ignore )
            {
                logExceptionAsWarning(ignore);
                // do NOT exit; cause unclear, could be caused by bad MBean, etc
            }
            
            // Efficiently suck out remaining queue items in one fell swoop if there are at least
            // two items (why allocate an array for one item).
            // Note that this effectively doubles the size of the queue
            if ( mQueue.size() >= 2 )
            {
                try
                {
                    // pre-allocate enough space for all possible items
                    final List<QueueItem> items = new ArrayList<QueueItem>(MAX_QUEUE_SIZE);
                    mQueue.drainTo(items, MAX_QUEUE_SIZE);
                    //debug( "SUCKED OUT ITEMS: " + items.size() );
                    for(final QueueItem item : items)
                    {
                        try
                        {
                            processItem(item);
                        }
                        catch( final Throwable ignore )
                        {
                            logExceptionAsWarning(ignore);
                            // KEEP GOING
                        }
                    }
                }
                catch( final Throwable ignore )
                {
                    logExceptionAsWarning(ignore);
                    // KEEP GOING
                }
            }
		}
	}

		public void
	run()
	{
        try
        {
            processFOREVER();
        }
        catch( Throwable t )
        {
            getLogger().severe( "LoaderRegThread.run(): impossible" );
        }
    }
    
    //---------------------------------  QueueItems below ------------------------------------------
        
    /**
        Maximum size of the queue.  Since the queue is a BlockingQueue, this size should be
        large so as to avoid blocking callers with large domains; large
        numbers of items can be queued in a clustered environment very rapidly.  Testing shows that
        starting up a default domain with 10 empty clusters (no servers) can fill up a 512-item
        queue during startup, and instances can be far larger than that.  Fortunately, that
        is mostly an issue only for the AMX loader, which bulk-loads MBeans upon initialization.
     */
    private static final int MAX_QUEUE_SIZE = 512;
    
    /**
        Any item to be processed. 
        <p>
        Note that CountDownLatch functionality could potentially be used for any subclass
        of QueueItem, so it is included in the base class.
     */
    private static class QueueItem
    {
        /** Optional latch to indicate completion of an item */
        final CountDownLatch    mLatch;
        
        /** The thread that is waiting on the latch */
        final Thread            mWaitingThread;
                
        QueueItem( final CountDownLatch latch, final Thread callingThread)
        {
            mLatch         = latch;
            mWaitingThread = callingThread;
        }
        
        public final void await()
            throws InterruptedException
        {
            if ( mLatch != null )
            {
                mLatch.await();
            }
        }
        
        public final void countDown()
        {
            if ( mLatch != null )
            {
                mLatch.countDown();
            }
        }
    }
    
    /**
      QueueItem for registering/unregistering an MBean.  Does not support latch functionality,
      but could do so if callers had a need.
     */
    private static final class MBeanItem extends QueueItem
    {
        /** for registration or unregistration */
        public final ObjectName mObjectName;
        
        /* true to register the MBean, false to unregister */
        public final boolean    mRegister;
        
        MBeanItem( final ObjectName item, final boolean register )
        {
            super(null,null);
            mObjectName = item;
            mRegister   = register;
        }
    }
    
    /**
        Base class for any item used for coordinating with the worker thread.
     */
    private static class WaitDoneItem extends QueueItem
    {
        WaitDoneItem()
        {
            super(new CountDownLatch(1), Thread.currentThread());
            //debug( "WaitDoneItem");
        }
    }
}













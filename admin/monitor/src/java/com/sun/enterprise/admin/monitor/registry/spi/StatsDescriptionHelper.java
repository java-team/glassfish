/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * StatsDescriptionHelper.java
 *
 * Created on September 17, 2003, 3:12 PM
 */

package com.sun.enterprise.admin.monitor.registry.spi;

import java.util.ResourceBundle;
import java.util.Vector;
import java.util.Iterator;
import java.util.MissingResourceException;

/**
 * Provides a localized String that is the Description of a
 * given statistic.
 * A Vector that holds a list of resource bundles is being used 
 * for its thread-safe properties.
 * @author  Shreedhar Ganapathy<mailto:shreedhar.ganapathy@sun.com>
 */
public class StatsDescriptionHelper {
    
    private final Vector<ResourceBundle> resourceBundles = new Vector<ResourceBundle>();
    public static final String NO_DESCRIPTION_AVAILABLE = "No Description was available";
    private static final String DEFAULT_PACKAGE = StatsDescriptionHelper.class.getPackage().getName();
    private static final String DEFAULT_FILE = "LocalStrings";
    private static final StatsDescriptionHelper sdh = new StatsDescriptionHelper();

    public StatsDescriptionHelper() 
    {
        ResourceBundle resourceBundle = 
                ResourceBundle.getBundle(DEFAULT_PACKAGE + "." + DEFAULT_FILE);
        resourceBundles.add(resourceBundle);
    }

    public static StatsDescriptionHelper getInstance() {
        return sdh;
    }

    /**
     * Adds a resource bundle
     */
    public void addResourceBundle(ResourceBundle rb)
    {
        resourceBundles.add(rb);
    }
    
    /**
     * Returns the localized description for the statistic.
     * If {@link #addResourceBundle} is called while getting a description,
     * it will result in ConcurrentModificationException.
     */
    public String getDescription(String key)
    {
        String value = NO_DESCRIPTION_AVAILABLE;

        for (ResourceBundle resourceBundle: resourceBundles) {
            try {
                value = resourceBundle.getString(key);
                break;
            }
            catch (MissingResourceException mre) {
                // continue search in next resource bundle
            }
        }
        return value;
    }
}

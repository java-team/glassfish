#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
TAR=../glassfish_$2.orig.tar.xz
DIR=glassfish-$2.orig

# clean up the upstream tarball
unzip $3
mv glassfish $DIR
GZIP=--best tar -c -J -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $3 $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
